import settings

def _default_test(user):
    return user.groups.filter(name='crans_adherent')

def app_list(request):
    apps = []
    if request.user.is_authenticated():
        for app in settings.INTRANET_APPS + settings.REDIRECT_SERVICES:
            if app.get('test', _default_test)(request.user):
                apps.append(app)
        apps.sort(key = lambda x: (x['category'], 'label' in x and x['label'] or x['name'] ))
    return {'apps': apps}
