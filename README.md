# Crans Intranet2

L'intranet du Crans regroupe en un site web les services de l'association,
sois sous la forme d'applications, soit sous la forme de pointeurs vers
les services. L'intranet actuel (version 2) est basé sur django.

Actuellement, le code est compatible python 2.7 et django 1.7 (jessie). Il
utilise une base `pgsql` pour son fonctionnement interne, mais la plupart
des applications se base sur les données `ldap` et nécessitent `lc_ldap`.

## Dépendances Debian
 * python-django
 * python-django-reversion
 * texlive
 * texlive-latex-extra
 * texlive-lang-french

## Dépendances Crans
L'intranet est donc intrinsèquement lié à `/usr/scripts`. Celui-ci est
d'ailleurs mentionné dans le `sys.path`.

 * `/usr/scripts`
 * `lc_ldap`
 * `wifi_new` (pour la wifimap)

En cas d'utilisation du CAS crans pour le login, le module `django_cas_ng`
doit être installé via `pip`.

## Configuration d'une copie de test sur vo

Pour faire marcher l'intranet :

 * copier settings_local.py.example en settings_local.py
 * Dans settings_local.py :
   * passer DEBUG à True
   * changer LOCATION (par exemple localhome)
   * passer DEV à True (si on veut avoir les fichiers statiques servis
     par le serveur de développement de django)
   * changer la ligne ROOT_PATH correspondante
   * changer BASE_LDAP_TEST à True
   * changer le ROOT_URL (par exemple "https://vo.crans.org:8080")

 * exécuter si besoin (quand on utilise la base de test ça ne sert à rien) : `./manage.py syncdb`
 * Pour le lancer : `./manage.py runserver <ROOT_URL>`
    Ex : `./manage.py runserver vo.crans.org:8080`

## Fonctionnement en prod

Le serveur habituel de production est `o2.crans.org`, qui sert les pages webs
via `nginx`, en rajoutant le https.

 * `nginx` sert directement les fichiers statiques, dont l'uri commence par
   `/static/` depuis le répertoire local `var/static/`. Ce repertoire est
   peuplé via la commande `umask 002; ./manage.py collectstatic`.
 * Les autres requêtes https sont traités en mode proxy, c'est-à-dire qu'elles
   sont redirigés vers une socket locale `/tmp/gunicorn-intranet.sock` servie
   par `gunicorn`.

La base de donnée pgsql n'est _pas_ sur `o2` : une connexion distante doit
être établie avec les paramètres suivants :

 * Serveur : `pgsql.adm.crans.org` (actuellement `thot`)
 * Nom de base : `django`
 * Utilisateur : `crans`

L'authentification se fait à l'aide d'`ident` ce qui permet de s'affranchir du
mot de passe. Certains services ont également besoin d'accéder aux données
des modèles django, et lorsque cela est possible, on leur autorise un accès
en lecture seule à l'aide de l'utilisateur `crans_ro`.

## Fonctionnement en dev sur vo

Une copie du dépôt est présente sur vo avec une configuration très proche de
celle de la prod afin de pouvoir réaliser des tests réalistes. La seule
différence réside dans la base pgsql qui est locale.

La modification du `settings.py` assure également que l'on utilise la base
de test.

## Développer pour l'intranet
Si vous souhaitez contribuer à l'intranet veuillez lire le [guide de développeur](doc/dev_guide.md)

## Todo-List
 * drop column type (reversion_version)
 * fin de l'appli d'impression
 * comment dumper la base de prod ici
 * crans/crans_ro information
 * env de test plus propre
