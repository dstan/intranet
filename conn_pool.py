# -*- coding: utf-8 -*-
#
# CONN_POOL.PY-- 
#
# Copyright (C) 2010 Antoine Durand-Gasselin
# Author: Antoine Durand-Gasselin <adg@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
import time
import sys
import ldap
import settings
from ldap import SERVER_DOWN

import lc_ldap.shortcuts as shortcuts

CONNS = {}
OBJECTS = {}
LDAP_USERS = {}
LDAP_USER_LAST_UPDATE = {}

LDAP_USER_TIMEOUT = 10

def get_conn(user, force_new=False):
    if settings.BASE_LDAP_TEST:
        make_admin_conn = shortcuts.lc_ldap_test
    else:
        make_admin_conn = shortcuts.lc_ldap_admin
    if force_new or not user.username in CONNS.keys():
        CONNS[user.username]= make_admin_conn(user=user.username)
    return CONNS[user.username]

def get_user(user, mode='ro', refresh=False):
    if not refresh and user.username in LDAP_USER_LAST_UPDATE.keys() and \
    time.time() - LDAP_USER_LAST_UPDATE[user.username] < LDAP_USER_TIMEOUT and \
    user.username in LDAP_USERS.keys() and \
    (mode == 'ro' or LDAP_USERS[user.username].mode == mode):
        return LDAP_USERS[user.username]
    elif user.username:
        filter = u'uid=%s' % user.username.split('@', 1)[0]
        try:
            [luser] = get_conn(user).search(filter, mode=mode, scope=ldap.SCOPE_ONELEVEL)
        except SERVER_DOWN:
            [luser] = get_conn(user, force_new=True).search(filter, mode=mode, scope=ldap.SCOPE_ONELEVEL)
        LDAP_USERS[user.username] = luser
        LDAP_USER_LAST_UPDATE[user.username] = time.time()
        return luser
    else:
        return None

def get_machine(user, mid, mode='ro', refresh=False):
    # Si on est connecté, on cherche dans les machines de l'user ldap
    if user.username:
        luser =  get_user(user, mode=mode, refresh=False)
        machine = [ m for m in get_user(user).machines() if m['mid'][0].value == int(mid) ]

    if mode == 'rw' and machine and machine[0].mode == 'ro':
        machine = []

    # Sinon ou si on n'a rien trouvé, on fait une requête ldap
    if not user.username or not machine:
        try:
            machine = get_conn(user).search(u'mid=%s' % mid, mode=mode)
        except SERVER_DOWN:
            machine = get_conn(user, force_new=True).search(u'mid=%s' % mid, mode=mode)

    if machine:
        return machine[0]
    else:
        return None
