# -*- coding: utf-8 -*-
# Django settings for intranet project.

import os
# Imports des secrets (plus tard, mais pas en debug)
import sys
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')
if '/usr/local/django/intranet' not in sys.path:
    sys.path.append('/usr/local/django/intranet')

import django
from django.utils.importlib import import_module
import gestion.secrets_new as secrets
conn_pool = import_module('conn_pool', 'intranet')

""" Liste d'applications désactivées """
DISABLED_APPS = ['impression', 'locaux', 'dummy', 'voip']

try:
    # On récupère les settings du dépot dans lequel on est…
    from settings_local import *
except ImportError:
    # …ou on fallback sur une conf par défaut
    # Si on teste l'intranet, il vaut mieux changer les valeurs dans settings_local.py
    DEBUG = False
    LOCATION = "o2"
    # Explication :
    #  Les paramètres ne sont pas les mêmes si :
    #   - "o2" : on est en prod sur o2
    #   - "vo" : on est en test sur vo
    #   - "localhome" : on est en dev dans son localhome perso (penser à changer le ROOT_PATH)

    # Est-ce qu'on doit servir les fichiers statiques
    DEV = False

    if LOCATION == "localhome":
        ROOT_PATH = '/localhome/toto/Crans/intranet/'
    elif LOCATION == "vo":
        ROOT_PATH = '/localhome/django/intranet/'
    elif LOCATION == 'apprentis':
        ROOT_PATH = '/usr/local/intranet/'
    elif LOCATION == "o2":
        SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
        ROOT_PATH = '/usr/local/django/intranet/'
    else:
        raise ValueError(u"Location unknown")

    # Utiliser la base LDAP de test ?
    BASE_LDAP_TEST = False

    # L'url de l'intranet
    ROOT_URL = "https://intranet.crans.org/"

    # À qui faut-il envoyer les mails de câblage
    CABLAGE_MAIL_DEST = ['respbats@crans.org', ]

    # Faut-il utiliser le CAS pour s'authentifier
    CAS_ENABLED = True

# Où stocker les fichiers static collectés
STATIC_ROOT = os.path.abspath(os.path.join(ROOT_PATH, 'var/static'))

SESSION_COOKIE_AGE = 300

SESSION_SAVE_EVERY_REQUEST = True

TEMPLATE_DEBUG = DEBUG

EMAIL_SUBJECT_PREFIX = "[Intranet2 Cr@ns] "

# Nécessaire pour timezone.now() renvoie un datetime timezoné
USE_TZ = True

ADMINS = (
    ('Intranet', 'root@crans.org'),
)

ALLOWED_HOSTS = [
    'intranet2.crans.org',
    'intranet.crans.org',
    'intranet-dev.crans.org',
    'intranet-apprentis.crans.org',
]

ATOMIC_REQUESTS = True

MANAGERS = ADMINS

FIXTURE_DIRS=(ROOT_PATH + 'fixtures/',)

DB_HOST = os.getenv('DBG_DJANGO_DB', None)
if not DB_HOST:
    if LOCATION == 'o2':
        DB_HOST = 'pgsql.v4.adm.crans.org'
    elif LOCATION in ['vo', 'localhome']:
        DB_HOST = 'localhost'
if DB_HOST:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'django',
            'HOST': DB_HOST,
            'USER': 'crans',
        },
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': ROOT_PATH + "default.sqlite",
        },
    }


STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(ROOT_PATH, 'static'),)

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Formate les dates en fonction de la localisation
USE_L10N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = ROOT_PATH

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = ROOT_URL + 'media/'

# Make this unique, and don't share it with anybody.
if LOCATION != 'o2':
    SECRET_KEY = "sYjlDBZUBSMnk"
else: # Ne marchera que sur o2
    SECRET_KEY = secrets.get('django_secret_key')

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
#    'django.middleware.transaction.TransactionMiddleware', Est retiré dans Django 1.8
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
)


if CAS_ENABLED:
    MIDDLEWARE_CLASSES += (
      'django.middleware.common.CommonMiddleware',
      'django.contrib.sessions.middleware.SessionMiddleware',
      'django.contrib.auth.middleware.AuthenticationMiddleware',
    )

MIDDLEWARE_CLASSES += (
    'django.contrib.messages.middleware.MessageMiddleware',
    'reversion.middleware.RevisionMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
)

TEMPLATE_CONTEXT_PROCESSORS += (
    'intranet.custom_context_processor.app_list',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    MEDIA_ROOT + 'templates/',
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

LOGIN_URL = "/login"
LOGIN_REDIRECT_URL = "/"

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]

CAS_SERVER_URL = 'https://cas.crans.org/cas/'
CAS_VERSION = '3'

# En dehors d'un usage personnel, on se permet d'activer le backend LDAP
# (reste à voir si on se connecte à la vraie ou à la base de test)
if CAS_ENABLED:
    AUTHENTICATION_BACKENDS.append('django_cas_ng.backends.CASBackend')
else:
    AUTHENTICATION_BACKENDS.append('login.LDAPUserBackend')

# Le nom de l'appli prises est utilisé pour les reverse URL.
#  Si jamais on a envie de le changer, pour que ce soit plus simple,
#  on le place dans une variable ici.
APP_PRISES_NAME = "prises"

CABLAGE_MAIL_FROM = u'"L\'intranet du Cr@ns" <intranet-bugreport@lists.crans.org>'

INTRANET_APPS = (
    {
        'name': 'locaux',
        'category': 'Beta',
        'label': u'Accès aux locaux',
        'test': lambda u: u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_apprenti'),
    },
    {
        'name':'dummy',
        'title':'Application factice vide pour tests',
        'category':'Beta',
        'test': lambda u: u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_apprenti'),
    },
    {
        'name': APP_PRISES_NAME,
        'category':'Administration',
        'label': u'Prises réseau',
        'test': lambda u: u.has_perm('prises.can_view') or u.groups.filter(name='crans_adherent'),
    },
    {
        'name':'impressions',
        'category': 'Services',
        'label': u'Impressions',
        'help':'https://wiki.crans.org/VieCrans/ImpressionReseau',
    },
    {
        'name':'digicode',
        'category': 'Services',
        'label': u'Gestion des digicodes',
        'title':"Les codes pour accéder au local d'impression du 4J",
    },
    {
        'name':'wifimap',
        'title':'Carte des positions et état des bornes wifi Cr@ns',
        'category':'Services',
        'label': u'Carte WiFi',
    },
    {
        'name': 'tv',
        'title':'Télévison et radio',
        'category':'Services',
        'label': u'TV',
        'help':'https://wiki.crans.org/TvReseau',
    },
    {
        'name':'voip',
        'title':'Service de téléphonie via internet',
        'category': 'Services',
        'label': u'Téléphonie VoIP',
        'help':'https://wiki.crans.org/VieCrans/UtiliserVoIP',
    },
    {
        'name': 'wiki',
        'category': 'Administration',
        'label': 'Compte WiKi',
        'title':'Associer son compte crans avec le wiki du crans',
        'help':'https://wiki.crans.org'
    },
    {
        'name':'compte',
        'title':'Gestion de compte',
        'category':'Administration',
        'label': u'Mon Compte',
        'help': 'https://wiki.crans.org/VieCrans/GestionCompte/'
    },
    {
        'name':'pageperso',
        'title':'Gestion page perso',
        'category': 'Administration',
        'label': u'Page Perso',
        'help':'https://wiki.crans.org/VieCrans/GestionCompte/#Comment_cr.2BAOk-er_sa_page_personnelle_.3F',
    },
    {
        'name' : 'solde',
        'title' : 'Consulter son solde et recharger son compte Cr@ns',
        'category' : 'Administration',
        'label' : u'Mon Solde',
        'help': 'https://wiki.crans.org/CransPratique/SoldeImpression',
    },
    {
        'name':'quota',
        'title':'Gestion des Quotas',
        'category':'Administration',
        'label': u'Quotas',
        'help':'https://wiki.crans.org/VieCrans/GestionCompte/GestionQuota',
    },
    {
        'name': 'machines',
        'category': 'Administration',
        'label': 'Gestion des machines',
        'test': lambda u: (u.groups.filter(name='crans_paiement_ok') or conn_pool.get_user(u).machines()),
        'title': 'Créer, modifier ou supprimer des machines',
    },
    {
        'name': 'factures',
        'category': 'Administration',
        'label': 'Mes factures',
    },
    {
        'name': 'validation',
        'category': 'Administration',
        'label': 'Validation',
        'test': lambda u: u.groups.filter(name='crans_nounou') or u.groups.filter(name='crans_cableur'),
    },
    {
        'name': 'autoconf',
        'category': 'Administration',
        'label': 'autoconf',
        'test': lambda _: False, # Personne n'a besoin de voir cette appli
    },
    {
        'name': 'club',
        'category': 'Administration',
        'label': 'Gestion des clubs',
        'test': lambda u: u.groups.filter(name='crans_respo_club'),
    },
)

INTRANET_APPS = tuple([ i for i in INTRANET_APPS if i['name'] not in
    DISABLED_APPS])

REDIRECT_SERVICES = (
    {
        'url': 'https://gitlab.crans.org/',
        'category': 'Divers',
        'label': 'GitLab',
        'pic' : 'icone_gitlab',
        'test': lambda _: True,
        'title': 'Interface web pour gérer des dépôts Git',
        'help':'https://wiki.crans.org/CransTechnique/GitLab',
    },
    {
        'url': 'https://roundcube.crans.org',
        'category': 'Services',
        'label': 'Webmail',
        'pic' : 'icone_roundcube',
        'test': lambda u: u.groups.filter(name='crans_adherent') or u.groups.filter(name='crans_club'),
        'title': 'Interface web pour vos mails crans',
        'help':'https://wiki.crans.org/VieCrans/LesMails',
    },
    {
        'url': 'https://news.crans.org/newsgroups.php?cas=crans.org',
        'category': 'Services',
        'label': 'Webnews',
        'pic' : 'icone_webnews',
        'title': 'Interface web pour les newsgroups (les forums) crans',
        'help':'https://wiki.crans.org/VieCrans/ForumNews',
    },
    {
        'url': 'http://irc.crans.org/web/',
        'category': 'Services',
        'label': 'Webirc',
        'pic' : 'icone_irc',
        'test': lambda _: True,
        'title': 'Interface web pour les canaux de discussion du crans',
        'help':'https://wiki.crans.org/VieCrans/UtiliserIrc',
    },
    {
        'url': 'https://owncloud.crans.org/index.php?app=user_cas',
        'category': 'Services',
        'label': 'ownCloud',
        'pic' : 'icone_owncloud',
        'test': lambda u: u.groups.filter(name='crans_adherent') or u.groups.filter(name='crans_club'),
        'title':'Interface web pour stocker des fichiers à la dropbox',
        'help':'https://wiki.crans.org/VieCrans/Owncloud',
    },
    {
        'url': 'https://pad.crans.org/',
        'category': 'Divers',
        'label': 'Etherpad',
        'test': lambda _: True,
        'pic' : 'icone_etherpad',
        'title' : "Une interface web pour faire de l'édition collaborative de texte",
    },
    {
        'url': 'https://zero.crans.org/',
        'category': 'Divers',
        'label': 'ZeroBin',
        'test': lambda _: True,
        'pic' : 'icone_zerobin',
        'title' : "Un outil minimaliste pour partager du texte",
    },

)

#REDIRECT_SERVICES = tuple([i.update({'name' : 'redirect/%s' % i['url']}) or i for i in REDIRECT_SERVICES ])
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    #'south',
    'reversion',
) + tuple( 'apps.%s' % app['name'] for app in INTRANET_APPS )

# Config de l'app d'impressions
MAX_PRINTFILE_SIZE=25 * 1024 * 1024 # 25Mio

###############################################################################
# Django >= 1.6
###############################################################################
CSRF_HTTP_ONLY = True


###############################################################################
# Django >= 1.7
###############################################################################
SESSION_COOKIE_SECURE = not DEBUG

SILENCED_SYSTEM_CHECKS = ["1_6.W001"]
