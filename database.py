# -*- coding: utf-8 -*-
#
# DATABASE.PY -- Gestion des bases de données multiples dans l'intranet
#
# Copyright (C) 2010 Nicolas Dandrimont
# Authors: Nicolas Dandrimont <olasd@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import settings

class Router(object):
    """Effectue le routage entre les différentes bases de données

    Actuellement, redirige toutes les requetes de l'application
    `prises' vers la base de données idoine"""

    def db_for_read(self, model, **hints):
        """Base de données à utiliser pour la lecture"""
        #if model._meta.app_label == "prises":
        #    return "switchs"

        return None

    def db_for_write(self, model, **hints):
        """Base de données à utiliser pour l'écriture"""
        #if model._meta.app_label == "prises":
        #    return "switchs"

        return None

    def allow_syncdb(self, db, model):
        """Autoriser l'écriture du modèle donné dans la base de données ?"""

        #if db == "switchs":
        #    if model._meta.app_label == "prises":
        #        return True
        #    return False
        #else:
        #    if model._meta.app_label == "prises":
        #        return False
        return None

