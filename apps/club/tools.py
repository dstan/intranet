# -*- coding: utf-8 -*-
from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')

def get_clubs_for_user(user):
    '''Renvoie la liste des noms des clubs de l'utilisateur et leur objet ldap associé'''
    ldap_user = conn_pool.get_user(user, refresh=True)
    clubs = ldap_user.clubs()
    clubs_name = [unicode(club['nom'][0]) for club in clubs]
    return zip(clubs_name, clubs)

def get_imprimeurs_for_club(club):
    '''Retourne le nom des imprimeurs pour l'objet club'''
    adhs_name = []
    adhs_object = []
    for adh in club['imprimeurClub']:
        adh = adh.get_imprimeur()
        adhs_name.append(unicode(adh['prenom'][0]) + u' ' + unicode(adh['nom'][0]))
        adhs_object.append(adh)
    return zip(adhs_name, adhs_object)

