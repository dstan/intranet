# -*- coding: utf-8 -*-
from django import forms
from tools import get_imprimeurs_for_club

class ImprimeurDelForm(forms.Form):
    old_imprimeurs = forms.MultipleChoiceField(required=True, label=u'Imprimeurs à supprimer', choices=())

    def __init__(self, club, *args, **kwargs):
        super(ImprimeurDelForm, self).__init__(*args, **kwargs)
        choices = [
            (
            imprimeur_obj['aid'][0],
            imprimeur_name
            )
            for imprimeur_name, imprimeur_obj in get_imprimeurs_for_club(club)]
        self.fields['old_imprimeurs'].choices = tuple(choices)

class ImprimeurAddForm(forms.Form):
    new_imprimeur = forms.CharField(
        required=True,
        label='Ajouter un imprimeur',
        help_text=('Entrer le login crans de la personne.\n'
            'Usuellement le nom (sans accents) '
            'ou la première lettre du prénom suivi du nom'
            )
        )

    def clean_new_imprimeur(self):
        """Passe le login de l'imprimeur en minuscule et retire les espaces"""
        # Apparement cette méthode est appelé par Django au moment du clean
        new_imprimeur = self.cleaned_data['new_imprimeur']
        login = new_imprimeur.lower().strip(' ')
        return login

