# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.utils.importlib import import_module
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.forms.models import formset_factory
from django.contrib import messages

from forms import ImprimeurAddForm, ImprimeurDelForm
from tools import get_clubs_for_user, get_imprimeurs_for_club

conn_pool = import_module('conn_pool', 'intranet')


class IndexView(TemplateView):
    template_name = 'club/index.html'

    def get_context_data(self, *args, **kwargs):
        context = super(IndexView, self).get_context_data(*args, **kwargs)
        clubs = []
        for (club_name, club) in get_clubs_for_user(self.request.user):
            imprimeurs = get_imprimeurs_for_club(club)
            club_dict = {
                'club_name': club_name,
                'cid': int(club['cid'][0]),
                }
            if imprimeurs:
                club_dict.update({
                    'imprimeurs': zip(*get_imprimeurs_for_club(club))[0],
                })
            else:
                club_dict.update({
                    'imprimeurs': [],
                })
            clubs.append(club_dict)
        context.update({'clubs': clubs})
        return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(IndexView, self).dispatch(*args, **kwargs)

class ImprimeurView(FormView):
    success_url = reverse_lazy('club:index')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        self.cid = int(self.kwargs['cid'])
        for club_name, club in get_clubs_for_user(self.request.user):
            if int(club['cid'][0]) == self.cid:
                break
        else: # Ce else est pour le for et non pour le if
            messages.error(self.request, "Vous n'avez pas le droit de modifier ce club !")
            return redirect('club:index')
        return super(ImprimeurView, self).dispatch(*args, **kwargs)

class ImprimeurAddView(ImprimeurView):
    template_name = 'club/imprimeur_add.html'
    form_class = ImprimeurAddForm

    def form_valid(self, form):
        ldap_user = conn_pool.get_conn(self.request.user)
        club = ldap_user.search(u'cid=%s' % self.cid, mode='w')[0]
        with club as club:
            new_imprimeur = form.cleaned_data['new_imprimeur']
            try:
                new_imprimeur_ldap = ldap_user.search(u'uid=%s' % new_imprimeur)[0]
            except IndexError:
                messages.error(self.request, "Ce login n'est pas valide")
                return redirect(self.request.path)
            try:
                club['imprimeurClub'].append(unicode(new_imprimeur_ldap['aid'][0]))
                club.history_gen()
                club.save()
            except ValueError as e:
                messages.error(self.request, e)
                return redirect(self.request.path)
        return super(ImprimeurAddView, self).form_valid(form)

class ImprimeurDelView(ImprimeurView):
    template_name = 'club/imprimeur_del.html'
    form_class = ImprimeurDelForm

    def get_form(self, form_class):
        ldap_user = conn_pool.get_conn(self.request.user)
        club = ldap_user.search(u'cid=%s' % self.cid, mode='w')[0]
        post = self.request.POST
        if post:
            return form_class(club, post)
        else:
            return form_class(club)

    def form_invalid(self, form):
        print self.request.POST
        print form.non_field_errors()
        print form.errors
        print form.__dict__
        return super(ImprimeurDelView, self).form_invalid(form)

    def form_valid(self, form):
        ldap_user = conn_pool.get_conn(self.request.user)
        club = ldap_user.search(u'cid=%s' % self.cid, mode='w')[0]
        with club as club:
            for old_imprimeur_aid in form.cleaned_data['old_imprimeurs']:
                club['imprimeurClub'].remove(unicode(old_imprimeur_aid))
                club.history_gen()
                club.save()
        return super(ImprimeurDelView, self).form_valid(form)

