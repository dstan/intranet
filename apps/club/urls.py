from django.conf.urls import url

import views

urlpatterns = [
    url('^$', views.IndexView.as_view(), name='index'),
    url('^(?P<cid>[0-9]+)/imprimeur/ajouter$', views.ImprimeurAddView.as_view(), name='imprimeur_add'),
    url('^(?P<cid>[0-9]+)/imprimeur/retirer$', views.ImprimeurDelView.as_view(), name='imprimeur_del'),
    ]
