# -*- coding: utf-8 -*-
# Récriture de l'app quota
# Enrichissement de celle-ci.
# Gabriel Détraz detraz@crans.org
"""Ce module permet d'afficher aux utilisateurs leur quota
sur l'espace de stockage du crans"""

import os
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

import psycopg2

import utils.quota as quota
from gestion.config.upload import soft, hard

from django.template import RequestContext
from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')

from passlib.apps import ldap_context
# On associe une couleur quand on est en dessous du pourcentage qui va bien
QUOTA_RATES = (30, 50, 75, 85)
QUOTA_COLORS_CLASSES = ("good", "ok", "warning", "alert", "overflow")
QUOTA_RATES_UPLOAD = (0, soft, 2*soft, hard/2)

@login_required
def affquotas(request):
    """Effectue le render après avoir récupéré les quotas"""
    # Quota d'espace disque
    quota_list = quota.getUserQuota(str(request.user).split('@')[0])
    quotas = []

    for index, current_quota in enumerate(quota_list):
        quotas.append({})

        pourcent = current_quota['%']

        quotas[index]['pourcent'] = pourcent
        quotas[index]['pourcentaff'] = min(int(pourcent), 100)
        quotas[index]['label'] = current_quota['label']
        quotas[index]['limite'] = current_quota['limite']
        quotas[index]['quota'] = current_quota['quota']
        quotas[index]['usage'] = current_quota['usage']
        quotas[index]['filesystem'] = current_quota['filesystem']

        # On peut dépasser temporairement les 100 %
        if current_quota['%'] < 100:
            quotas[index]['mess'] = u'Votre quota est actuellement à un niveau normal'
        else:
            quotas[index]['mess'] = u"Vous êtes actuellement en dépassement de quota, veuillez libérer de l'espace"

        # On affiche la couleur qui va bien
        for r_index, rate in enumerate(QUOTA_RATES):
            if current_quota['%'] < rate:
                quotas[index]['color_class'] = QUOTA_COLORS_CLASSES[r_index]
                break
        else:
            # Si pas de break, c'est que quota['%'] est >= à QUOTA_RATES[-1]
            quotas[index]['color_class'] = QUOTA_COLORS_CLASSES[-1]


    # Partie 2 : afficher le quota d'upload (approximatif volontairement)
    # Quota d'upload : connexion base pg d'odlyd + recherche ldap du gonz

    connect_dict = {
        'database': 'filtrage',
        'user': 'crans_ro',
    }
    host = os.getenv('DBG_UPLOAD', 'upload.v4.adm.crans.org')
    if ':' in host:
        host, port = host.split(':', 1)
        connect_dict['port'] = int(port)
    connect_dict['host'] = host

    con = psycopg2.connect(**connect_dict)
    cur = con.cursor()
    luser = conn_pool.get_user(request.user)
    u_type, u_id = luser.dn.split(',', 1)[0].split('=')
    u_type = {'aid': 'adherent', 'cid': 'club'}[u_type]
    u_id = int(u_id)
    cur.execute("SELECT up FROM accounting where id='%s' and type=%s",
        (u_id, u_type))
    try:
        up = (cur.fetchone() or [])[0]
    except IndexError:
        up = None

    # Si y a rien dans la table (EXT?), on n'affiche rien, ou presque (bl et
    # avertissement)
    quot_up = {}
    quot_up['upload'] = False
    quot_up['limite'] = hard
    if up:
        upmega = up/(1024*1024)

        # En fonction de l'interval d'upload, on affecte une couleur et une fourchette
        for r_index, rate in enumerate(QUOTA_RATES_UPLOAD):
            if upmega < rate:
                uploadtot = u"entre %d et %d" % (QUOTA_RATES_UPLOAD[r_index-1], rate)
                quot_up['color_class'] = QUOTA_COLORS_CLASSES[r_index-1]
                quot_up['pourcent'] = r_index*100/4
                break
        else:
            # Si pas de break, c'est que quota['%'] est >= à QUOTA_RATES[-1]
            uploadtot = u"plus de %d" % QUOTA_RATES_UPLOAD[-1]
            quot_up['color_class'] = QUOTA_COLORS_CLASSES[-1]
            quot_up['pourcent'] = 100

        # On renvoit les données au template dans le dico quot_up
        quot_up['label'] = u"Quotas d'upload"
        quot_up['usage'] = uploadtot
        quot_up['upload'] = True

        # Si sanction, on affiche
        for bl in luser.blacklist_actif():
            if bl['type'] == u'autodisc_upload':
                quot_up['blackliste'] = True

    for bl in luser.blacklist_actif():
            if bl['type'] == u'autodisc_upload':
                quot_up['blackliste'] = True
    return render(request, "quota/affichage.html", {'quot_up': quot_up, 'quotas': quotas})
