# -*- encoding: utf-8 -*-

import django.shortcuts
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.views.decorators.debug import sensitive_post_parameters

from models import Profile, Call
from forms import ProfileForm
from django.core.urlresolvers import reverse

from django.views.generic.edit import DeleteView
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q

import datetime

@login_required
def index(request):
    try:
        profile = Profile.objects.filter(user__exact=request.user)[0]
    except IndexError:
        return django.shortcuts.render(request, "voip/noaccount.html")
    
    return django.shortcuts.render(request, "voip/view.html",
        {'profile': profile,})

@login_required
@sensitive_post_parameters('voicemail_password', 'pass_change2', 'pass_change1')
def edit(request):
    try:
        profile = Profile.objects.filter(user__exact=request.user)[0]
    except IndexError:
        profile = None
    if request.method == 'POST':
        form = ProfileForm(request.POST, instance=profile, user=request.user)
        if form.is_valid():
            profile = form.save()
            return django.shortcuts.redirect('voip:index')
    else:
        form = ProfileForm(instance=profile)
    return django.shortcuts.render(request, "voip/edit.html", {'form': form})

@login_required
def delete(request):
    obj = get_object_or_404(Profile, user__exact=request.user)
    if request.method == 'POST':
        obj.delete()
        return django.shortcuts.redirect('apps.accueil.views.view')
    return django.shortcuts.render(request, "confirm.html", {
        'confirm_title': u'Suppression du compte SIP',
        'confirm_message': u'Êtes-vous sûr de vouloir effacer votre compte téléphonique ?',
        'confirm_cancel_url': reverse('voip:index'),
    })

@login_required
def book(request, download=False):
    data = {'profiles': Profile.objects.filter(published__exact=True).order_by('num')}
    if not download:
        return django.shortcuts.render(request, "voip/book.html", data)

    response = django.shortcuts.render(request, "voip/book.vcf", data,
        content_type='text/x-vcard')
    date = datetime.datetime.now()
    response['Content-Disposition'] = 'attachment; filename="CransBook_%d_%d_%d.vcf"' % (date.day, date.month, date.year)
    return response

@login_required
def history(request, page=None):
    """ Affiche un historique des appels, paginé """
    num = get_object_or_404(Profile, user__exact=request.user).num

    hist_list = Call.objects.filter(
        Q(dst__exact=num)|Q(src__exact=num)
        ).order_by('-date')

    paginator = Paginator(hist_list, 25)

    try:
        hist = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        hist = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        hist = paginator.page(paginator.num_pages)

    return django.shortcuts.render(request, 'voip/history.html', {
        "history": hist,
        })
