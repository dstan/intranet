# -*- encoding: utf-8 -*-

import sys

from django.core.management.base import BaseCommand, CommandError
from apps.voip.models import Profile

class Command(BaseCommand):
    args = ''
    help = u'Génère une conf pour la boîte vocal d\'asterisk'

    def handle(self, *args, **options):
        for profile in Profile.objects.order_by('user__username').all():
            self.stdout.write(profile.asterisk_alias_config() + u'\n')
