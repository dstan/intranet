# -*- coding: utf-8 -*-

from django.contrib import messages

import settings
class asterisk_reload_conf_debug:
    @classmethod
    def reload_config(self, fname):
        pass

# TODO weird stuff here
try:
    from sip import asterisk_reload_conf
except ImportError:
    if not settings.DEBUG:
        raise
    asterisk_reload_conf = asterisk_reload_conf_debug

from django.db import models
from django.contrib.auth.models import User
import hashlib
import os

from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')

# Description de la configuration générée pour asterisk
# TODO eurk
asterisk_conf = {
    'modules':{
        'sip':{
            'reload_fields':["caller_id","password"],
            'gen_conf':lambda _:Profile.asterisk_config, # pour pouvoir appeler Profile paresseusement
            'conf_file':'sip_accounts',
        },
        'voicemail':{
            'reload_fields':["voicemail_password"],
            'gen_conf':lambda _:Profile.asterisk_voicemail_config,
            'conf_file':'sip_accounts_voicemail',
        },
        'dialplan':{
            'reload_fields':["num"],
            'gen_conf':lambda _:Profile.asterisk_alias_config,
            'conf_file':'sip_accounts_alias',
        },
   },
   'conf_path':'/usr/scripts/var/sip/'
}


class Profile(models.Model):
    """ Profil d'utilisateur Voip """

    class Meta:
        verbose_name = u'Profil SIP'
        verbose_name_plural = u'Profils SIP'

    CALLER_IDS = [
        ('full_name', u'Nom complet'),
        ('number', u'Numéro de Téléphone'),
        ('both', u'Nom et numéro de téléphone'),
    ]

    """ Propriétaire du profil. Devrait être un compte Cr@ns """
    user = models.OneToOneField(User, primary_key=True)

    """ Mot de passe. Hashé en md5 avec un sel
        (md5($num_asterisk:asterisk:$pass)) """
    password = models.CharField(u"Mot de passe", max_length=32, null=False, blank=False)

    """ Autoriser ou non la publication dans l'annuaire """
    published = models.BooleanField(u"Publication dans l'annuaire", help_text=u"Affichage du numéro et du nom dans l'annuaire du Crans", default=False)

    """ Choix du format pour l'identification de l'appelant """
    caller_id = models.CharField(u"Affichage de l'appelant", max_length=9, choices=CALLER_IDS, help_text=u"Nom à afficher lors d'un appel")

    """ Numéro de téléphone, on utilise un varchar afin de pouvoir placer des 0
        initiaux et des éventuels #"""
    num = models.CharField(u"Numéro de téléphone", max_length=10, null=False, blank=False)
    voicemail_password = models.CharField(u"Mot de passe boîte vocale", max_length=6, null=False, blank=False)


    def what_to_reload(self):
        """ Defini les modules asterisk dont il faut régénérer la configuration """
        def has_changed(self, field):
            if not self.pk:
                return True
            try:
                old_value = self.__class__._default_manager.\
                         filter(pk=self.pk).values(field).get()[field]
            except self.DoesNotExist:
                return True
            return not getattr(self, field) == old_value

        modules={}
        for module in asterisk_conf['modules'].keys():
            modules[module]=reduce( # List.fold_left
                lambda x,y: x or has_changed(self,y),
                asterisk_conf['modules'][module]['reload_fields'],
                False
            )
        return modules

    def reload_what_it_is_to_reload(self,modules):
        """ Écrit la configuration des modules asterisk à recharger, puis les recharge """
        def writeconf(gen_conf_module,path):
            conf=""
            for profile in Profile.objects.order_by('user__username').all():
                conf+=gen_conf_module(profile).encode('utf-8') + '\n'
            tmppath = path + '.new'
            if settings.LOCATION == 'o2' or not settings.DEBUG:
                with open(tmppath,'w') as fout:
                    fout.write(conf)
                os.rename(tmppath, path)
        for module in asterisk_conf['modules'].keys():
            if modules[module]:
                writeconf(
                    asterisk_conf['modules'][module]['gen_conf'](''),
                    asterisk_conf['conf_path'] + asterisk_conf['modules'][module][ 'conf_file']
                )
        
        if reduce(lambda x,y: x or y, modules.values(), False):
            asterisk_reload_conf.reload_config('all')
        else:
            for module in asterisk_conf['modules'].keys(): 
                if modules[module]:
                    asterisk_reload_conf.reload_config(module)

    def delete(self):
        ret=super(Profile,self).delete()
        self.reload_what_it_is_to_reload(self.what_to_reload())
        return ret

    def save(self):
        to_reload=self.what_to_reload()
        ret=super(Profile,self).save()
        self.reload_what_it_is_to_reload(to_reload)
        return ret

    def set_user(self, new_user):
        """ Défini l'utilisateur propriétaire du profil. Cette fonction calcule
        également le numéro de téléphone correspondant, à partir de la base LDAP.
        IMPORTANT: changer d'utilisateur (donc de numéro de téléphone) casse
        le mot de passe, qui devra être redéfini."""
        self.user = new_user
        try:
            aid = conn_pool.get_user(new_user)['aid'][0].value
            sip_ext = '1%04d' % int(str(aid)[:4])
        except KeyError:
            if not settings.DEBUG:
                raise Exception("Not allowed") # FIXME ?
            sip_ext = '9%04d' % int(str(new_user.pk)[:4])
        self.num = sip_ext

    def set_password(self, new_pass):
        """ Définition du mot de passe.
        IMPORTANT: le mot de passe est stocké hashé dans la base de donnée.
        Il faut donc avoir préalablement défini le numéro de téléphone (et
        donc l'utilisateur) avant d'appeler cette fonction.
        """
        if self.num == '':
            raise Exception('Set user/num first')
        self.password = hashlib.md5("%s:asterisk:%s" % (self.num, new_pass)).hexdigest()

    def __unicode__(self):
        return u'%s (%s)' % (self.user.username, self.num)

    def asterisk_alias_config(self):
        """Génération de la ligne de conf asterisk mappant login à son numéro correspondant"""
        return "exten => %(login)s,1,Goto(%(num)s,1)" % {'num':self.num,'login':self.user.username.split('@')[0]}

    def asterisk_voicemail_config(self):
        """ Génération de la ligne de conf asterisk activant la boîte vocale"""
        return u"%(num)s => %(mail_pass)s,%(full_name)s,%(mail)s,,attach=%(attach)s|delete=%(delete)s" % {
       'num':self.num,'mail_pass':self.voicemail_password,'full_name':self.user.get_full_name(),
       'mail':self.user.username,'attach':'yes','delete':'no'}

    def asterisk_config(self):
        """ Génération du paragraphe de conf asterisk definissant l'utilisateur"""
        return u""";%(uid)s
[%(num)s](default-crans)
username=%(num)s
callerid="%(callerid)s"
md5secret=%(passwd)s
mailbox=%(num)s@666
""" % {
 'callerid': self.get_caller_id(),
 'num': self.num,
 'passwd': self.password,
 'uid': self.user.username,
}


    def get_category(self):
        """ Renvoie la catégorie du profil, dans l'annuaire """
        return "Cr@ns"

    def get_caller_id(self):
        """ Calcule le texte descriptif de l'appelant """
        if self.caller_id == 'full_name':
            return self.user.get_full_name()
        elif self.caller_id == 'number':
            return self.num
        else: # Both
            return self.user.get_full_name() + u' <' + self.num + u'>'

class Call(models.Model):
    """ Un appel archivé.
    Automatiquement rempli par asterisk (et les scripts maisons de Nit)
    """

    class Meta:
        """La table s'appelle history, on définit ici plutôt un nom pour un objet"""
        db_table = 'voip_history'
        verbose_name = u'Appel archivé'
        verbose_name_plural = u'Appels archivés'

    uniq_id = models.CharField(u"Identifiant de l'appel", max_length=80, primary_key=True)
    src = models.CharField(u"Numéro appelant", max_length=80)
    dst = models.CharField(u"Numéro appelant", max_length=80)

    """ Une durée égale à NULL correspond à un appel en cours (sauf si plantage) """
    duration = models.IntegerField(u"Durée de l'appel", null=True, blank=True)
    date = models.DateField(u"Date d'initiation de l'appel")

    def __unicode__(self):
        o = u"%s -> %s @ %s" % (self.src, self.dst, self.date)
        if self.duration:
            o += " (%ds)" % self.duration
        return o

