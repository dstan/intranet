# -*- coding: utf-8 -*-
from django.forms import ModelForm, PasswordInput, CharField, ValidationError
from models import Profile

class ProfileForm(ModelForm):
    """ Formulaire d'édition de profil Cr@ns, avec confirmation de changement de
    mot de passe. Basé sur le modèle, il l'étend légèrement. """
    class Meta:
        model = Profile
        """user et num ne doivent pas être modifiés à la main. (l'un dépend de 
           l'autre, et cela n'est pas au choix de l'utilisateur, anyway)
           password est gardé tel quel, on utilise nos validations et champs
           perso pour le modifier"""
        exclude = ('user', 'num', 'password')

    """ Premier champ de mdp, à laisser vide si inchangé"""
    pass_change1 = CharField(max_length=255,
                             widget=PasswordInput,
                             label="Mot de passe (laisser vide si inchangé)",
                             required=False)
    """ Champ de confirmation """
    pass_change2 = CharField(max_length=255,
                             widget=PasswordInput,
                             label="Mot de passe (répéter)",
                             required=False)
    
    def __init__(self, *args, **kwargs):
        """ Construit un formulaire. On rajoute un argument facultatif: l'user
            qui édite ce formulaire (pour la création de l'objet).
            On récupère aussi une instance de profil, éventuellement. Si elle
            n'existe pas, c'est que l'on doit créer le profil, le mdp est donc
            obligatoire (sinon non).
            """
        self.user = kwargs.get('user',None)
        if kwargs.has_key('user'):
            del kwargs['user']
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.need_pass = kwargs.get('instance',None) == None


    def clean(self):
        """ En plus des vérifications habituelles, on vérifie que les deux mdp
        sont identiques, si renseignés. Et renseignés si obligatoires."""
        cleaned_data = super(ProfileForm, self).clean()
        p1 = cleaned_data.get('pass_change1','')
        p2 = cleaned_data.get('pass_change2','')
        if self.need_pass and len(p1) == 0:
            raise ValidationError({'pass_change1':[u'Vous devez rentrer un mot de passe (il n\'a jamais été défini)',]})
        if p1 != p2:
            raise ValidationError({'pass_change1':[u'Les mots de passe ne correspondent pas',]})
        # assert( p1 == p2 )
        if 0 < len(p1) and len(p1) < 6:
            raise ValidationError({'pass_change1':[u'Le mot de passe doit posséder au moins 6 caractères',]})
        return cleaned_data

    def save(self, commit=True):
        """ On applique deux trois modifs avant de réellement sauver: on
        remplace l'user et on redéfinie le mdp si changé"""
        obj = super(ProfileForm, self).save(commit=False)
        obj.set_user(self.user) # important: on doit calculer l'user et le num
                                # AVANT de définir le pass
        if self.cleaned_data.get('pass_change1','') != '':
            obj.set_password(self.cleaned_data['pass_change1'])
        if commit:
            return obj.save()
        else:
            return obj
