from django.conf.urls import patterns as patterns
from django.conf.urls import url

import views

urlpatterns = patterns('',
    url('^$', views.afficher, name='afficher'),
    url('chgpass', views.chgpass, name='chgpass'),
    url('redirection', views.redirection, name='redirection'),
    url('alias', views.alias, name='alias')
)
