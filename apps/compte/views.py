# -*- coding: utf-8 -*-
# Récriture de l'app compte
# Enrichissement de celle-ci.
# Gabriel Détraz detraz@crans.org

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.debug import sensitive_post_parameters
import subprocess

from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.utils import timezone
from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')

from django.contrib import messages

from passlib.apps import ldap_context
from gestion.chgpass import check_password
import lc_ldap.crans_utils

from gestion import config

from forms import CompteForm
from forms import PassForm
from forms import MailForm
from forms import AliasForm
from django.forms.util import ErrorList

# Vue d'affichage seulement des infos pour l'adh
@login_required
def afficher(request):
    luser = conn_pool.get_user(request.user)

    # On transmet au template si les checkbox sont cochées ou pas à la base.
    gl = (luser.get('contourneGreylist', [u''])[0] == u"OK")

    obj_ty = luser.ldap_name


    # Données initiales pour le formulaire, permet de tester les
    # changements quand on valide le formulaire, et de préremplir
    # celui-ci.
    if unicode(obj_ty) == u'adherent':
        adher = True

        # Pour la redirection, on fait un appel externe non bloquant (cas du dev)
        redirection_mail = subprocess.Popen(["sudo", "-n", "/usr/scripts/utils/forward.py", "--read", "--name=%s" % luser['uid'][0]], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        mailredirect = redirection_mail.stdout.readlines()
        if mailredirect:
            mailredirect = mailredirect[0]
        else:
            mailredirect = "N/A"

        # On peuple le champ étude avec des valeurs par défaut
        _etudes = list(config.etudes_defaults)

        for i in xrange(len(luser['etudes'])):
            _etudes[i] = luser['etudes'][i]

        etabetudes = _etudes[0]
        annetudes = _etudes[1]
        dometudes = _etudes[2]

        initial_data = {
            'nom': luser['nom'][0],
            'prenom': luser['prenom'][0],
            'tel': luser['tel'][0],
            'contourneGreylist': gl,
            'annetudes': annetudes,
            'etabetudes': etabetudes,
            'dometudes': dometudes,
        }

    else:
        adher = False
        initial_data = {
            'nom': luser['nom'][0],
        }

    if request.method == "POST":
        form = CompteForm(request.POST, initial=initial_data)
        if form.is_valid():
            # On ouvre une connexion
            luserconn = conn_pool.get_user(request.user, mode='w')
            with luserconn as luser:
                # Pour les attributs que l'on ne peut mettre à jour de façon générique,
                # on fait les tests de modification explicite au début, puis on les
                # vire de changed_data.

                # Pour les études, l'établissement, l'année et le type d'études
                # sont trois champs dans le formulaire, c'est plus simple à gérer.
                if unicode(obj_ty) == u"adherent":
                    for i in xrange(3 - len(luser['etudes'])):
                        luser['etudes'].append(config.etudes_defaults[i])

                    if 'etabetudes' in form.changed_data:
                        luser['etudes'][0] = unicode(form.cleaned_data['etabetudes']) or config.etudes_defaults[0]
                        _ = form.changed_data.remove('etabetudes')

                    if 'annetudes' in form.changed_data:
                        luser['etudes'][1] = unicode(form.cleaned_data['annetudes']) or config.etudes_defaults[1]
                        _ = form.changed_data.remove('annetudes')

                    if 'dometudes' in form.changed_data:
                        luser['etudes'][2] = unicode(form.cleaned_data['dometudes']) or config.etudes_defaults[2]
                        _ = form.changed_data.remove('dometudes')

                # Le greylisting est un peu particulier.
                if 'contourneGreylist' in form.changed_data:
                    if form.cleaned_data['contourneGreylist']=="True":
                        luser["contourneGreylist"] = "OK"
                    else:
                        luser["contourneGreylist"] = []
                    _ = form.changed_data.remove('contourneGreylist')

                # On boucle sur les champs "simples"
                for field in form.changed_data:
                    try:
                        luser[field] = form.cleaned_data[field]
                    except ValueError as e:
                        elist = form._errors.setdefault(field, ErrorList())
                        elist.append(e)

                # Il faut créer l'historique avant de sauvegarder.
                luser.history_gen()
                luser.save()
                messages.success(request, u"""Les modifications ont été prises en compte""")
    else:
        form = CompteForm(initial=initial_data)
    context = {
        'form': form,
        'now': timezone.now(),
        'adher': adher,
        'luser': luser,
        'mailredirect': mailredirect,
        }
    return render(request, "compte/affichage.html", context)

@sensitive_post_parameters()
@login_required
def chgpass(request):
    if request.method == "POST":
        form = PassForm(request.POST)
        if form.is_valid():
            luserconn = conn_pool.get_user(request.user, mode='w')

            with luserconn as luser:
                # Changement de mot de passe : il faut satisfaire à : le mot de passe existant est le bon, les 2 champs nouveaux mot de passe sont égaux,
                # , et le nouveau mot de passe respecte la convention Cr@ns. Si non, on renvoie une erreur à chaque fois.
                if "passwdexists" in form.changed_data:
                    if ldap_context.verify(unicode(form.cleaned_data['passwdexists']), unicode(luser['userPassword'][0])):
                        new_field2 = form.cleaned_data['newpasswd1'].encode('utf-8')
                        new_field3 = form.cleaned_data['newpasswd2'].encode('utf-8')
                        if new_field2 == new_field3:
                            check, msg = check_password(new_field2)
                            if check:
                                hashedPassword = lc_ldap.crans_utils.hash_password(new_field2)
                                luser['userPassword'] = [hashedPassword.decode('ascii')]
                                luser.history_gen()
                                luser.save()
                                messages.success(request, u"""Votre mot de passe a bien été changé""")
                                return redirect("/compte/")
                            else:
                                errors = form._errors.setdefault("newpasswd2", ErrorList())
                                errors.extend(msg.split('\n'))
                        else:
                            errors = form._errors.setdefault("newpasswd2", ErrorList())
                            errors.append(u"Le nouveau mot de passe n'est pas le même sur les deux champs")
                    else:
                        errors = form._errors.setdefault("passwdexists", ErrorList())
                        errors.append(u"Ancien mot de passe erroné.")

    else:
        form = PassForm()
    return render(request, "compte/chgpass.html", {'form': form})

@login_required
def redirection(request):
    luser = conn_pool.get_user(request.user)
    redirection_mail = subprocess.Popen(["sudo", "-n", "/usr/scripts/utils/forward.py", "--read", "--name=%s" % luser['uid'][0]], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    mailredirect = redirection_mail.stdout.readlines()
    if mailredirect:
        mailredirect = mailredirect[0]
    else:
        mailredirect = "N/A"
    initial_data = {
    'mailredirect': mailredirect,
    }
    if request.method == "POST":
        form = MailForm(request.POST, initial=initial_data)
        if form.is_valid():
        # La redirection n'est pas un champ ldap
            if 'mailredirect' in form.changed_data:
                mailredirect = unicode(form.cleaned_data['mailredirect'])
                redirection_mail = subprocess.Popen(["sudo", "-n", "/usr/scripts/utils/forward.py", "--write", "--mail=%s" % mailredirect, "--name=%s" % luser['uid'][0]])
                messages.success(request, u"""Votre redirection a bien été changée""")
                return redirect("/compte/")
    else:
        form = MailForm(initial=initial_data)
    return render(request, "compte/redirection.html", {'form': form})

@login_required
def alias(request):
    luser = conn_pool.get_user(request.user, mode='w')
    if request.method == "POST":
        form = AliasForm(request.POST)
        if form.is_valid():
            # Pour les Alias mail, on ne permet que l'ajout, pas la suppression
            if 'mailAlias' in form.changed_data:
                try:
                    luser['mailAlias'].append(form.cleaned_data['mailAlias'])
                    luser.history_gen()
                    luser.save()
                    messages.success(request, u"""L'alias a été ajouté avec succès""")
                    return redirect("/compte/")
                except ValueError as e:
                     # Le message d'une ValueError est directement dans e
                     elist = form._errors.setdefault('mailAlias', ErrorList())
                     elist.append(e)
    else:
        form = AliasForm()
    return render(request, "compte/alias.html", {'form': form, 'luser': luser})
