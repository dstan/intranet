# -*- coding: utf-8 -*-


from django import forms
from django.forms import widgets
from django.core.validators import validate_email
from django.forms.util import ErrorList


class CompteForm(forms.Form):
    nom = forms.CharField(label=u'Nom', max_length=40)
    prenom = forms.CharField(label=u'Prénom', max_length=40, required=False)
    tel = forms.CharField(label=u'Téléphone', max_length=15, required=False)
    etabetudes = forms.CharField(label=u'Etablissement', max_length=100, required=False)
    annetudes = forms.CharField(label=u"Année d'étude", max_length=100, required=False)
    dometudes = forms.CharField(label=u'Domaine', max_length=100, required=False)
    contourneGreylist = forms.CharField(label=u'Contournement du greylisting', max_length=10, widget= widgets.CheckboxInput, required=False)

class MailForm(forms.Form):
    mailredirect = forms.CharField(label=u'Redirection des mails', max_length=40, required=False)

    def clean_mailredirect(self):
        """Nettoie le champ de redirection et vérifie qu'il correspond
        aux critères imposés"""
        mailredirect = self.cleaned_data['mailredirect']

        if not mailredirect.strip():
            return ""

        if "@crans.org" in mailredirect:
            raise forms.ValidationError("Impossible de rediriger vers une addresse mail crans")
        try:
            validate_email(mailredirect)
        except forms.ValidationError:
            if not mailredirect.strip('"').strip().startswith('|'):
                raise forms.ValidationError("Ce champ doit être une adresse mail ou l'execution d'un programme type procmail")
        return mailredirect

class AliasForm(forms.Form):
    mailAlias = forms.EmailField(label=u'Nouvel alias Mail', max_length=40, required=False)

class PassForm(forms.Form):
    passwdexists = forms.CharField(label=u'Ancien mot de passe', max_length=255, widget= widgets.PasswordInput, required=False)
    newpasswd1 = forms.CharField(label=u'Nouveau mot de passe', max_length=255, widget= widgets.PasswordInput, required=False)
    newpasswd2 = forms.CharField(label=u'Retaper le mot de passe', max_length=255, widget= widgets.PasswordInput, required=False)

