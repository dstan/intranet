# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class Local(models.Model):
    u"""Un local, que ce soit Crans ou Crous"""
    is_local = True
    class Meta:
        verbose_name_plural = "locaux"
        ordering = ['batiment', 'etage']
    batiment = models.CharField(max_length=1)
    etage = models.IntegerField()
    description = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        if self.description != '':
            comment = u" (%s)" % self.description
        else:
            comment = u""
        return u"%d%s%s" % (self.etage, self.batiment, comment)

    def get_clefs(self):
        return Clef.objects.filter(local__exact=self)

class Clef(models.Model):
    u"""Une clé, un proprio, pour un local"""
    local = models.ForeignKey(Local)
    proprietaire = models.ForeignKey(User)
    description = models.CharField(max_length=255, blank=True, null=True)

    def get_next_interventions(self):
        # La relation "exact" est en fait, une inclusion, comme on peut
        # s'y attendre (un vrai "exact" serait vraiment lourd)
        return Intervention.objects.filter(clefs__exact=self)

    def __unicode__(self):
        if self.description != '':
            comment = u" (%s)" % self.description
        else:
            comment = u""
        return u"%d%s%s - %s %s" % (self.local.etage, self.local.batiment,\
                                 comment, self.proprietaire.first_name, \
                                 self.proprietaire.last_name)


class Intervention(models.Model):
    u"""Une intervention, peut-être planifiée"""
    class Meta:
        permissions = (("can_view", u"Peut voir les interventions"),
                       ("can_create", u"Peut créer son intervention"))
        ordering = ['-debut', 'intervenant']
    ETAT_CHOICES = (
        ('prevu',u'Prévu'),
        ('en_cours',u'En cours'),
        ('termine',u'Terminé'),
        ('annule',u'Annulé'),
    )

    intervenant = models.ForeignKey(User)
    titre = models.CharField(max_length=255, blank=False, null=False)
    clefs = models.ManyToManyField(Clef)
    debut = models.DateTimeField()
    fin = models.DateTimeField()
    etat = models.CharField(max_length=8, choices=ETAT_CHOICES)
    description = models.TextField()
    
    def __unicode__(self):
        base = self.titre + " par " + self.intervenant.first_name + " " + \
               self.intervenant.last_name
        return base

    @models.permalink
    def get_absolute_url(self):
        return ('intervention', (), {'iid': self.id})

