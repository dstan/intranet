# -*- encoding: utf-8 -*-
# Create your views here.
import django.shortcuts
from django.http import Http404
from django.template import RequestContext

from django.contrib.auth.decorators import login_required, permission_required

from models import Clef, Local, Intervention

class Proprio:
    is_proprio = True
    def __init__(self, user, name=None):
        self.user = user
        if name == None:
            name = u"%s %s" % (user.first_name, user.last_name)
        self.name = name

    def get_clefs(self):
        return Clef.objects.filter(proprietaire__exact=self.user)

    def __unicode__(self):
        return self.name

from django.contrib.auth.decorators import login_required, permission_required

@permission_required('locaux.can_view')
def view(request):
    t = {'groupe_list': [Proprio(request.user, u"Mes clés")] +
                         list(Local.objects.all()) }
    return django.shortcuts.render_to_response("locaux/liste_clefs.html", t,\
            context_instance=RequestContext(request))

@permission_required('locaux.can_view')
def intervention(request, iid):
    try:
        [interv] = list(Intervention.objects.filter(id__exact=iid))
        return django.shortcuts.render_to_response("locaux/intervention.html",\
            {'intervention': interv},\
            context_instance=RequestContext(request))
    except ValueError:
        raise Http404
