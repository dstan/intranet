# -*- coding: utf-8 -*-

"""
Ce fichier est probablement déprécié.

Il donne les primitives de modification du compte WiKi
associé à un utilisateur. Ce fichier fait appel à une base de donnée sql
et n'est utilisé qu'en environnement de test (cf views.py) local.
En pratique, on utilise plutôt la version ldap de ce fichier, qui
fait appel au binding. """

from django.db import models
from django.contrib.auth.models import User


class WikiName(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    name = models.CharField(max_length=255, null=True)

    @classmethod
    def set_canonical_from_user(self, user):
        obj, created = self.objects.get_or_create(user=user)
        obj.name = user.first_name.capitalize() + user.last_name.capitalize()
        obj.save()

    @classmethod
    def get_value_from_user(self, user):
        obj, created = self.objects.get_or_create(user=user)
        return obj.name

    @classmethod
    def set_value_from_user(self, user, value):
        obj, created = self.objects.get_or_create(user=user)
        obj.name = value
        obj.save()

    @classmethod
    def create_account(cls, user, wiki_name, password):
        return "Impossible en test", True

    @classmethod
    def get_canonical_email_from_user(cls, user):
        return cls.get_canonical_from_user(user) + "@crans.org"

    @classmethod
    def get_canonical_from_user(cls, user):
        return user.first_name.capitalize() + user.last_name.capitalize()
