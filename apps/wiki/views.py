# -*- coding: utf-8 -*-

from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters, sensitive_variables
from django.contrib import messages
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.edit import FormView

from forms import LinkAccount, CreateAccount, Form
import settings
if settings.LOCATION != 'perso':
    from models_ldap import WikiName
else:
    from models_test import WikiName


class Create(FormView):
    template_name = 'wiki/create.html'
    form_class = CreateAccount
    success_url = reverse_lazy('wiki:index')

    @method_decorator(sensitive_variables('cleaned_data', 'password'))
    def form_valid(self, form):
        wiki_name = form.cleaned_data['wiki_name']
        password = form.cleaned_data['password1']
        reponse, erreur = WikiName.create_account(self.request.user, wiki_name, password)

        if WikiName.get_value_from_user(self.request.user) is not None:
        # L'utilisateur a déjà un compte et essaye de forger une nouvelle requête
            messages.warning(self.request, u'Vous avez déjà un compte associé !')
            return redirect('wiki:index')

        if erreur:
        # Un compte est déjà présent avec l'adresse mail canonique de l'utilisateur
            if "L'adresse %s est d" % WikiName.get_canonical_email_from_user(self.request.user) in reponse:
                messages.warning(
                    self.request,
                    (u'Vous avez probablement déjà créé un compte Wiki. '
                     u"Essayez plutôt de l'associer ou de "
                     u'<a href="https://wiki.crans.org/?action=recoverpass">récupérer</a> '
                     u'votre mot de passe.')
                )
                messages.warning(self.request, u'En cas de problème, contactez nounou@crans.org')
                return redirect('wiki:index')
            else:
                messages.warning(self.request, reponse)
                return self.form_invalid(form)
        else:
            WikiName.set_value_from_user(self.request.user, wiki_name)
            return super(Create, self).form_valid(form)

    def get_initial(self, *args, **kwargs):
        initial = super(Create, self).get_initial(*args, **kwargs)
        initial.update(
            {
                'wiki_name': WikiName.get_canonical_from_user(self.request.user)
            }
        )
        return initial

    def get_context_data(self, *args, **kwargs):
        context = super(Create, self).get_context_data(*args, **kwargs)
        context.update(
            {
                'canonical_email': WikiName.get_canonical_email_from_user(self.request.user)
            }
        )
        return context

    @method_decorator(sensitive_post_parameters('password1', 'password2'))
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Create, self).dispatch(*args, **kwargs)


class LinkView(FormView):
    template_name = 'wiki/link.html'
    form_class = LinkAccount
    success_url = reverse_lazy('wiki:index')

    @method_decorator(sensitive_variables('cleaned_data', 'password'))
    def form_valid(self, form):
        WikiName.set_value_from_user(self.request.user, form.cleaned_data['wiki_name'])
        messages.success(self.request, u'Votre compte Wiki a bien été lié à votre compte Cr@ns.')
        messages.warning(
            self.request,
            (u'Vous devrez vous <a href="%s">déconnecter</a> '
             u'pour que la nouvelle association soit prise en compte.') % reverse('logout')
        )
        return super(LinkView, self).form_valid(form)

    @method_decorator(sensitive_post_parameters('password'))
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LinkView, self).dispatch(*args, **kwargs)


class Index(TemplateView):
    template_name = 'wiki/index.html'

    def get_context_data(self, *args, **kwargs):
        context = super(Index, self).get_context_data(*args, **kwargs)
        context.update(
            {
                'wiki_name': WikiName.get_value_from_user(self.request.user),
                'wiki_url': 'https://wiki.crans.org',
            }
        )
        return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Index, self).dispatch(*args, **kwargs)


class Forget(FormView):
    template_name = 'confirm.html'
    form_class = Form
    success_url = reverse_lazy('wiki:index')

    def form_valid(self, form):
        WikiName.set_value_from_user(self.request.user, None)
        messages.success(self.request, u"Compte Wiki désassocié avec succès.")
        return super(Forget, self).form_valid(form)

    def get_context_data(self, *args, **kwargs):
        context = super(Forget, self).get_context_data(*args, **kwargs)
        context.update(
            {
                'confirm_title': u"Oublier l'association Wiki",
                'confirm_message': u'Êtes-vous sûr de vouloir désassocier vos comptes Wiki et Cr@ns ?',
                'confirm_cancel_url': reverse('wiki:index'),
            }
        )
        return context

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Forget, self).dispatch(*args, **kwargs)
