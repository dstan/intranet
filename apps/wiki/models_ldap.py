# -*- coding: utf-8 -*-

"""Ce fichier donne les primitives de modification du compte WiKi
associé à un utilisateur, via Binding. """

from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')

import wiki.creer_compte_wiki


class WikiName(object):
    @classmethod
    def _get_ldap_from_user(self, user, mode='ro', refresh=False):
        """ Get fresh ldap user object """
        return conn_pool.get_user(user, mode=mode, refresh=refresh)

    @classmethod
    def get_value_from_user(self, user):
        luser = self._get_ldap_from_user(user)
        try:
            return luser['compteWiki'][0].value
        except (KeyError, IndexError, ValueError):
            return None

    @classmethod
    def set_value_from_user(self, user, value):
        with self._get_ldap_from_user(user, mode='rw') as luser:
            if value:
                luser['compteWiki'] = unicode(value)
            else:
                luser['compteWiki'] = []
            luser.history_gen()
            luser.save()

    @classmethod
    def set_canonical_from_user(self, user):
        self.set_value_from_user(user.first_name.capitalize() + user.last_name.capitalize())

    @classmethod
    def create_account(cls, user, wiki_name, password):
        message, erreur = wiki.creer_compte_wiki.creer_compte(
            wiki_name,
            password,
            cls.get_canonical_email_from_user(user),
            str(user.username)
        )
        return message, erreur

    @classmethod
    def get_canonical_email_from_user(cls, user):
        return str(conn_pool.get_user(user)["canonicalAlias"][0])

    @classmethod
    def get_canonical_from_user(cls, user):
        return user.first_name.capitalize() + user.last_name.capitalize()
