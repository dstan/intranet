# -*- coding: utf-8 -*
import os
from subprocess import call
from tempfile import mkdtemp, mkstemp
from django.template.loader import render_to_string

from django.shortcuts import render,  redirect
from django.contrib.auth.decorators import login_required

from django.template import RequestContext

from django.http import HttpResponse

from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')

from tex import render_tex
import models
import hashlib
import time
import settings

@login_required
def index(request):
    luser = conn_pool.get_user(request.user)
    return render(request, 'factures/index.html', {'luser': luser})

@login_required
def facture(request, fid):
    luser = conn_pool.get_user(request.user)
    f = conn_pool.get_conn(request.user).search(u"fid=%s" % int(fid))
    if not f:
        return redirect('factures:index')
    else:
        f = f[0]

    if not f.dn in [fac.dn for fac in luser.factures()]:
        return redirect('factures:index')
    total = 0
    for i in range(0,len(f['article'])):
        f['article'][i].value['ptotal']=int(f['article'][i]['nombre']) * float(f['article'][i]['pu'])
        total += f['article'][i].value['ptotal']
    if f.get('recuPaiement', []):
        paid = total
    else:
        paid = 0
    (jour, heure) = f['historique'][0].value.split(',')[0].split(" ")
    date = u"Le %s à %s" % (jour, heure.replace(":", "h"))
    topay = total - paid
#    return render(request, 'factures/facture.tex', {'total':total, 'paid':paid, 'topay':total-paid, 'date':f['historique'][0].value.split(',')[0], 'f':f})
    return render_tex(request, 'factures/facture.tex', {'total':total, 'paid':paid, 'topay':topay, 'DATE':date, 'f':f, 'fid':fid})
