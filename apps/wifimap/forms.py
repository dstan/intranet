# -*- coding: utf-8 -*-


from django import forms
from django.forms import widgets

class Wifi_Action(forms.Form):
    actions_possibles = forms.ChoiceField(widget=forms.RadioSelect(), label=u'Actions possible sur les bornes ', required=False)

class Wifi_Info(forms.Form):
    vlan_select = forms.CharField(label=u'Vlan séléctionné', max_length=40, required=False)
    ssid_name = forms.CharField(label=u'Nom du ssid', max_length=40, required=False)
    encryption_mode = forms.CharField(label=u'Protocole de sécurité (choisir wpa2, psk ou open)', max_length=40, required=False)
    encryption_server = forms.CharField(label=u'Serveur Radius (ipv6)', max_length=60, required=False)
    encryption_key = forms.CharField(label=u'Clef de Sécurité Radius', max_length=40, required=False)

class Bornes_Cibles(forms.Form):
    batiment = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, label=u'Action à effectuer sur les batiments ', required=False)
    borne = forms.CharField(label=u'Action sur une borne en particulier ', max_length=40, required=False)
