# -*- coding: utf-8 -*-
from django.db import models

class VirtAP(models.Model):
    u"""Une borne virtuelle"""
    class Meta:
        verbose_name = "Borne WiFi virtuelle"
        verbose_name_plural = "Bornes WiFi virtuelles"
        #ordering = ['batiment', 'etage']
    host = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    lat = models.FloatField(null=True)
    lon = models.FloatField(null=True)
    comment = models.CharField(max_length=255)

    def xmlRepr(self, doc):
        borne = doc.createElement('borne')
        borne.setAttribute('virtual', 'virtual')
        def addTextNode(label, value):
            node = doc.createElement(label)
            node.appendChild(doc.createTextNode(unicode(value)))
            borne.appendChild(node)
        addTextNode('hostname', self.host)
        addTextNode('lon', self.lon)
        addTextNode('lat', self.lat)
        addTextNode('location', self.location)
        addTextNode('client_count', '0')
        addTextNode('editable', '')
        return borne


    def __unicode__(self):
        return "<%s, %s>" % (self.host, self.location)
