# -*- coding: utf-8 -*-
#
# Copyright (C) 2012 Daniel STAN
# Authors: Daniel STAN <daniel.stan@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import settings

from wifi_new import parse_xml

import django.shortcuts
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.csrf import csrf_exempt

from django.contrib import messages
from models import VirtAP
from forms import Wifi_Action
from forms import Bornes_Cibles
from forms import Wifi_Info

from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')
from lc_ldap.crans_utils import escape as ldap_escape
from gestion.config.config import liste_bats
from wifi_new.manage_borne_conf import exec_conn
from wifi_new import template

import xml.dom.minidom

#from django.contrib.auth.decorators import login_required, permission_required

def get_xml(request):
    public = not request.user.groups.filter(name='crans_nounou')
    doc = parse_xml.global_status(public)

    for ap in VirtAP.objects.all():
        doc.documentElement.appendChild(ap.xmlRepr(doc))

    # Only authorized users
    if not request.user.has_perm('wifimap.change_virtAP'):
        for editable in doc.getElementsByTagName('editable'):
            editable.parentNode.removeChild(editable)

    return HttpResponse(doc.toxml('utf-8'),content_type='text/xml; coding=utf-8')

def index(request):
    public = not request.user.groups.filter(name='crans_nounou')
    return django.shortcuts.render_to_response('wifimap/index.html', locals(), context_instance=RequestContext(request))

@csrf_exempt
@permission_required('wifimap.change_virtAP')
def update(request,hostname, lon, lat):
    doc = xml.dom.minidom.Document()

    ldap_c = conn_pool.get_conn(request.user)
    ldap_ap = ldap_c.search(u'(&(objectClass=borneWifi)(host=%s.*))' %
        ldap_escape(hostname), mode='w')
    if not ldap_ap:
        ap = get_object_or_404(VirtAP, host__exact=hostname)
        ap.lon = lon
        ap.lat = lat
        doc.appendChild(ap.xmlRepr(doc))
    else:
        ap = ldap_ap[0]
        ap['positionBorne'] = u'%s %s' % (lat, lon)
        ap.history_add(request.user.username, u'PositionBorne')
        borne = doc.createElement('borne')
        def addTextNode(label, value):
            node = doc.createElement(label)
            node.appendChild(doc.createTextNode(unicode(value)))
            borne.appendChild(node)
        addTextNode('hostname', hostname)
        addTextNode('lon', lon)
        addTextNode('lat', lat)
        addTextNode('editable', '')
        doc.appendChild(borne)
    ap.save()
    return HttpResponse(doc.toxml('utf-8'),content_type='text/xml; coding=utf-8')

@permission_required('wifimap.change_virtAP')
def change_conf(request):
    wifi_action = Wifi_Action()
    bornes_cibles = Bornes_Cibles()
    wifi_info = Wifi_Info()
    BATS = [(bat,bat) for bat in liste_bats]
    BATS.extend((('ens','ENS'),('all','Tous')))
    OPTIONS = [('ssid','Ajouter un SSID'), ('vlan','Ajouter une interface réseau'), ('ssid_enable','Activer un SSID (Deja présent !)'), ('ssid_disable','Désactiver un SSID'), ('hostapd','Redémarer Hostapd'), ('network','Redémarer network interfaces')]
    wifi_action.fields['actions_possibles'].choices = tuple(OPTIONS)
    bornes_cibles.fields['batiment'].choices = tuple(BATS)
    option=False
    if request.method == "POST":
        # On instancie
        wifi_action = Wifi_Action(request.POST)
        bornes_cibles = Bornes_Cibles(request.POST)
        wifi_info = Wifi_Info(request.POST)
        #On formatte
        bornes_cibles.fields['batiment'].choices = tuple(BATS)
        wifi_action.fields['actions_possibles'].choices = tuple(OPTIONS)

        # Etape 1 : selection de l'action
        if wifi_action.is_valid() and wifi_action.cleaned_data['actions_possibles']:
            #print wifi_action.cleaned_data['actions_possibles']
            option = str(wifi_action.cleaned_data['actions_possibles'])
            # On renvoie tout pour les menus secondaires.
            return django.shortcuts.render_to_response('wifimap/change_conf.html', locals(), context_instance=RequestContext(request))

        # Etape 2 : info supplémentaires et zone cible
        if bornes_cibles.is_valid() and 'option' in request.POST.keys():
            option = str(request.POST['option'])
            #print option

            # On récupère la cmd et on formatte
            cmd = getattr(template,option)

            #print bornes_cibles.cleaned_data['batiment']
            #print bornes_cibles.cleaned_data['borne']

            # Si c'est pas valide, excepté les cas triviaux, on envoie bouler pour eviter une commande foireuse sur les bornes
            if option!="hostapd" or option!="network":
                if not wifi_info.is_valid():
                    return django.shortcuts.render_to_response('wifimap/change_conf.html', locals(), context_instance=RequestContext(request))

            # On format pour les commandes complexes (vlan, add ssid)
            if option=="vlan":
                cmd = cmd.format(wifi_info.cleaned_data['vlan_select'])
            if option=="ssid_enable" or option == "ssid_disable":
                cmd = cmd.format(wifi_info.cleaned_data['ssid_name'])
            if option=="ssid":
                cmd = cmd.format(wifi_info.cleaned_data['vlan_select'],wifi_info.cleaned_data['encryption_mode'], wifi_info.cleaned_data['encryption_server'], wifi_info.cleaned_data['ssid_name'], wifi_info.cleaned_data['encryption_key'])

            #print cmd

            # On choisi la zone cible et on envoie la commande
            if "borne" in  bornes_cibles.changed_data:
                exec_conn(bats=all, ens=False, cmd="pwd", borne=bornes_cibles.cleaned_data['borne']+".wifi.crans.org")
            if "batiment" in  bornes_cibles.changed_data:
                if bornes_cibles.cleaned_data['batiment'] == u"ens":
                    exec_conn(bats=all, ens=True, cmd="pwd", borne=False)
                else:
                    exec_conn(bats=bornes_cibles.cleaned_data['batiment'], ens=False, cmd="pwd", borne=False)

            # On retourne à l'user
            messages.success(request, u"""La configuration a été prise en compte""")
            return django.shortcuts.redirect("/wifimap/")
        return django.shortcuts.render_to_response('wifimap/change_conf.html', locals(), context_instance=RequestContext(request))
    return django.shortcuts.render_to_response('wifimap/change_conf.html', locals(), context_instance=RequestContext(request))
