# -*- encoding: utf-8 -*-

# Donne l'etat des job pour l'intranet
# Vincent Legallic, Gabriel Détraz, Charlie Jacomme

import BeautifulSoup
import re, os
import time
import datetime
import urllib2
import settings
from django.db import models
from django import forms
from django.utils import timezone

URL_JOBLIST = 'https://imprimante.adm.crans.org/hp/device/JobLogReport/Index'
CA = '/etc/ssl/certs/cacert.org.pem'
STORE_FILE = "/intranet-dev/apps/impressions/ended_jobs.csv"
store_timeout = 30
store_cleanup = datetime.timedelta(days=60)


class JobsSync(models.Manager):
    def sync_jobs(self):
        now = time.time()
        # On supprime les jobs trop vieux
        self.filter(starttime__lte = timezone.now() - store_cleanup).delete()
        # On ne recharge que tous les store_timeout ou si la table est vide
        if self.filter(lastcheck__lte = now-store_timeout) or not self.all():
            RE_JOB_NAME = re.compile('JobLogName_')
            RE_JOB_STATUS = re.compile('JobLogStatus_')
            RE_JOB_DATE = re.compile('JobLogDate_')
            req = urllib2.urlopen(URL_JOBLIST, cafile=CA)
            doc = BeautifulSoup.BeautifulSoup(req.read())
            jobs = doc.findAll(attrs={'class': 'PrintJobTicket'})


            # Puis pour chaque job recupere de l'imprimante, on cree une entree
            for i in jobs:
                try:
                    jobname = i.find(attrs={'id': RE_JOB_NAME}).text
                    jid = jobname.split(":")[0]
                    result = i.find(attrs={'id': RE_JOB_STATUS}).text
                    endtime = i.find(attrs={'id': RE_JOB_DATE}).text
                    # On ne cree que les jobs qui n'existe pas encore
                    if not self.filter(jid=jid):
                        job = self.create()
                        job.format_from_name(jobname, 'username', result, endtime)
                        job.save()
                    else:
                        job = self.get(jid=jid)
                        job.valid_from_job(result, endtime)
                        job.save()
                except ValueError:
                    pass

# Pour les travaux finis
class Jobs(models.Model):
    jobname = models.CharField(max_length=255)
    login = models.CharField(max_length=50)
    file = models.FilePathField(max_length=255)
    endtime = models.CharField(max_length=255)
    result = models.CharField(max_length=50)
    lastcheck = models.CharField(max_length=50)
    club = models.CharField(max_length=50, blank=True)
    starttime = models.DateTimeField(auto_now_add=True)
    jid = models.IntegerField(default=0)
    prix = models.FloatField(default=0)

    objects = JobsSync()

    class Meta:
        verbose_name = 'Tache terminees'

    def format_from_name(self, jobname, username, result, endtime):
        self.jobname = jobname
        self.username = username
        # file fallback sur jobname par default
        self.file = "file not found, jobname = %s" % (repr(self.jobname),)
        # login fallback sur username par default
        self.login = "login not found, username = %s" % (repr(self.username),)
        self.result = result
        self.endtime = time.strftime("%F %T",
        time.strptime(endtime, "%Y/%m/%d %H:%M:%S"))
        self.lastcheck = time.time()

        try: # on essaie d'acceder a des donnes en plus si elles existent
            tab = jobname.split(':')
            self.login = tab[1].lower()
            self.file = ":".join(tab[2:])
            self.jid = tab[0]
        except:
            pass

    def valid_from_job(self, result, endtime):
        self.endtime = time.strftime("%F %T",
        time.strptime(endtime, "%Y/%m/%d %H:%M:%S"))
        self.lastcheck = time.time()
        self.result = result
