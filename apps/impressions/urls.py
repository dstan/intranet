from django.conf.urls import url

import views

urlpatterns = [
    url('^$', views.view, name="view"),
    url('^erreurs/$', views.erreur, name="erreurs"),
    url('^gestion/$', views.gestion, name="gestion"),
    url('^gestion/delete/(?P<jid>[^/]*)/$', views.delete_job, name="delete"),
    url('^gestion/lostjob/(?P<jid>[^/]*)/$', views.lost_job, name="lostjob"),
]
