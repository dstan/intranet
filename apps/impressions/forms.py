# -*- coding: utf-8 -*-

from django import forms
import settings
import models

class UploadForm(forms.Form):
    """Formulaire d'envoi d'un fichier à imprimer"""
    fichier = forms.FileField(label="Fichier à imprimer")
    def clean(self):
        """On vérifie qu'on nous a pas filé n'importe quoi."""
        out = forms.Form.clean(self)
        if out.has_key("fichier"):
            fichier = out["fichier"]
            if fichier != None and fichier.size > settings.MAX_PRINTFILE_SIZE:
                raise forms.ValidationError(u"Fichier trop volumineux (%s octets), maximum %s" % (fichier.size, settings.MAX_PRINTFILE_SIZE))

        return out

class PrintForm(forms.Form):
    """Formulaire pour spécifier les paramètres d'impression"""
    couleur = forms.ChoiceField(label="Couleur ou N&B",
                                choices=[('nb', 'Noir & Blanc'), ('couleur', 'Couleur')],
                                initial='nb')
    disposition = forms.ChoiceField(label="Disposition",
                                    choices=[('recto-verso', 'Recto/Verso'), ('recto', 'Recto'), ('livret', 'Livret')],
                                    initial='recto-verso')
    format = forms.ChoiceField(label="Format de papier",
                               choices=[('A4', 'A4'), ('A3', 'A3')],
                               initial='A4')
    agrafes = forms.ChoiceField(label="Agrafes",
                                 choices=[("none", "Aucune"),
                                        ("hg", "une en haut à gauche"),
                                        ("hd", "une en haut à droite"),
#                                        ("bg", "une en bas à gauche"),
#                                        ("bd", "une en bas à droite")],
                                        ("g", "deux sur le bord gauche"),
                                        ("d", "deux sur le bord droit")],
                                 initial="none")
    # Les options commentées n'existent pas
    perforation = forms.ChoiceField(label="Perforation",
                                 choices=[("None", "Aucune"),
                                        ("2HolePunchLeft", "2 trous à gauche"),
                                        ("2HolePunchRight", "2 trous à droite"),
                                        ("2HolePunchTop", "2 trous en haut"),
                                        ("2HolePunchBottom", "2 trous en bas"),
#                                        ("3HolePunchLeft", "3 trous à gauche"),
#                                        ("3HolePunchRight", "3 trous à droite"),
#                                        ("3HolePunchTop", "3 trous en haut"),
                                        ("4HolePunchLeft", "4 trous à gauche"),
                                        ("4HolePunchRight", "4 trous à droite")],
#                                        ("4HolePunchTop", "4 trous en haut")],
                                 initial="None")
    copies = forms.IntegerField(label="Nombre de copies", initial=1, min_value=1)
    filenom = forms.CharField(widget=forms.HiddenInput())
    # Pour mettre à jour le coût, on utilise javascript
    javascript_function = "javascript:update_prix();"
    for champ in [couleur, disposition, format, agrafes, copies]:
        champ.widget.attrs["onChange"] = javascript_function
    del champ # Gruiiik
    copies.widget.attrs["onKeyUp"] = javascript_function
    def clean(self):
        """Certains valeurs des paramètres sont incompatibles."""
        out = forms.Form.clean(self)
        return out


class ClubForm(forms.Form):
    club_list = forms.ChoiceField(label=u'Imprimer pour', required=False)
    def __init__(self, ldap_user, *args, **kwargs):
        super(ClubForm, self).__init__(*args, **kwargs)
        liste_compte = []
        for compte in [ldap_user] + ldap_user.clubs() + ldap_user.imprimeur_clubs():
            try:
                if compte['solde']:
                   solde = u" (" + unicode(compte['solde'][0]) + u"\u20AC)"
                else:
                   solde = u" (0\u20AC)"
                label = unicode(compte['nom'][0]) + solde
                uid = compte['uid'][0]
                if (uid,label) not in liste_compte:
                    liste_compte.append((uid,label))
            except KeyError:
                pass
        self.fields['club_list'].choices = tuple(liste_compte)
