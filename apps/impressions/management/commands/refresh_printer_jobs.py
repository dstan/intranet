from django.core.management.base import BaseCommand, CommandError
from apps.impressions.models import Jobs

class Command(BaseCommand):
    help = 'Rafrachit les jobs de l\' imprimante'

    def handle(self, *args, **options):
        try:
            Jobs.objects.sync_jobs()
        except URLError:
            pass
