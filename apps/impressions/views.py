# -*- encoding: utf-8 -*-

# Vincent Legallic, Gabriel Détraz, Charlie Jacomme

import settings

import time
import django.shortcuts
from django.http import HttpResponse, HttpResponseRedirect, Http404, HttpResponseBadRequest
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, render

from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')
# Pour stocker des messages dans la session courante
from django.contrib import messages

# On importe l'utilitaire d'impression
from impression.impression_hp import impression

# Formulaires
from forms import UploadForm, PrintForm, ClubForm
from models import Jobs
from apps.digicode.models import Code

# config_impression, pour les prix
import gestion.config.impression as config_impression

# Shortcuts pour recrediter, et la regexp des jobs effectués
from lc_ldap import shortcuts

# Quand on arrive pas à contacter l'imprimante
from urllib2 import URLError
# L'URL pour tester qu'on peut bien envoyer des impressions
import requests
from impression.impression_hp import URL_STATE, CA

config_prix = {"c_face_nb": config_impression.c_face_nb,
            "c_face_couleur": config_impression.c_face_couleur,
            "c_agrafe": config_impression.c_agrafe,
            "amm": config_impression.amm,
            "c_a4": config_impression.c_a4,
            "c_a3": config_impression.c_a3}

# Pour créer les dossier et faire des pdfinfo
import subprocess
import re
import os, os.path

def get_login(request):
    """Renvoie le login de l'utilisateur connecté"""
    # Attention, un intranet dirty hack fait que les users LDAP sont en login@crans.org
    login = request.user.username
    login = login.split("@")[0]
    return login

def is_imprimeur(request):
    """Renvoie True si l'utilisateur connecté à les droits imprimeurs"""
    luser = conn_pool.get_user(request.user)
    li = False
    if "Imprimeur" in luser['droits']:
        li = True
    return li

def get_solde(request):
    luser = conn_pool.get_user(request.user)
    try:
        solde = luser['solde'][0]
    except IndexError:
        solde = 0
    return solde

def get_solde_clubs(request):
    luser = conn_pool.get_user(request.user)
    class solde_clubs:
        def __init__(self):
            self.solde = 0
    clubs = []
    for cl in luser.clubs() + luser.imprimeur_clubs():
        try:
            club = solde_clubs()
            club.nom = cl['nom'][0]
            club.solde = cl['solde'][0]
        except KeyError:
            pass
        except IndexError:
            pass
        clubs.append(club)

    return clubs


def get_club(request):
    luser = conn_pool.get_user(request.user)
    club = []
    for cl in luser.clubs() + luser.imprimeur_clubs():
        try:
            nom = cl['nom'][0]
            uid = cl['uid'][0]
            if (uid,nom) not in club:
                club.append((uid,nom))
        except KeyError:
            pass
    club = tuple(club)
    return club

class NotPdfFile(Exception):
    """Exception levée si le fichier uploadé n'est pas un pdf"""
    pass

def pdfinfo(filepath):
    """Renvoi le résultat d'un pdfinfo sur un fichier"""
    filename = os.path.basename(filepath)
    # On commence par vérifier qu'on a bien affaire à un pdf
    fileinfo = subprocess.Popen(["file", "-b", filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    fileinfo = fileinfo.communicate()[0]
    if not "pdf document" in ''.join(fileinfo).lower():
        raise NotPdfFile
    infos = subprocess.Popen(["pdfinfo", filepath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    infos = infos.communicate()[0]
    infos = dict(re.findall('(.*?): *(.*)', infos))
    return infos

@login_required
def view(request):
    ldap_user = conn_pool.get_user(request.user)
    if request.method == "GET":
        uploadform = UploadForm(label_suffix=" :")
        clubform = ClubForm(ldap_user)
        return render(request, "impressions/impressions.html", {
            'uploadform': uploadform,
            'clubform' : clubform,
        })
    else:
        if "SubmitUploadForm" in request.POST.keys():
            uploadform = UploadForm(request.POST, request.FILES, label_suffix=" :")
            # On formate et on instancie pour les clubs
            clubform = ClubForm(ldap_user,request.POST)
            if uploadform.is_valid():
                fichier = request.FILES["fichier"]
                # Si impression en tant que club, on récupère le resultat
                club = False
                if clubform.is_valid() and not clubform.cleaned_data['club_list'] == ldap_user['uid'][0]:
                    club = clubform.cleaned_data['club_list']
                # On crée le dossier d'impression (si il existe déjà, ça ne fait rien de particulier)
                login = get_login(request)
                # Si on imprime en tant que club, on utilise le login club
                if club:
                    login_used = club
                else:
                    login_used = login
                createdir = subprocess.Popen(["sudo", "/usr/scripts/utils/chown_impressions.sh", login_used], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                createdir.communicate()
                # On y enregistre ensuite le fichier
                filename = fichier.name
                filepath = "/home/impressions/%s/%s" % (login_used, filename)
                with open(filepath, 'wb+') as destination:
                    for chunk in fichier.chunks():
                        destination.write(chunk)
                # On met tout ça dans une table
                job = Jobs.objects.create()
                job.login = login
                job.file = filename
                if club:
                    job.club = club
                job.save()
                # On cherche les infos sur le fichier
                try:
                    infos = pdfinfo(filepath)
                except NotPdfFile:
                    messages.error(request, u"""Votre fichier %s n'est pas un fichier PDF.""" % (filename))
                    clubform = ClubForm(ldap_user)
                    return render(request, "impressions/impressions.html", {
                        'uploadform': uploadform,
                        'clubform' : clubform,
                    })
                try:
                    pageno = int(infos["Pages"])
                except (KeyError, ValueError) as e:
                    messages.error(request, u"""Impossible de récupérer les informations sur le fichier PDF.""" )
                    clubform = ClubForm(ldap_user)
                    return render(request, "impressions/impressions.html", {
                        'uploadform': uploadform,
                        'clubform' : clubform,
                    })
                initial_data = {
                'filenom': filename,
                }
                printform = PrintForm(label_suffix=" :", initial=initial_data)
                return render_to_response("impressions/print.html",
                    {'printform': printform,
                     'pageno': pageno,
                     'une_agrafe': ["hg", "hd", "bg", "bd"],
                     'deux_agrafes': ["g", "d"],
                     'filename': filename,
                     'solde' : get_solde(request),
                     'solde_club' : get_solde_clubs(request),
                     'config_prix': config_prix},
                    context_instance=RequestContext(request))
            else:
                clubform = ClubForm(ldap_user)
                return render_to_response("impressions/impressions.html",\
                    {'uploadform': uploadform,
                     'clubform' : clubform},\
                    context_instance=RequestContext(request))
        elif "SubmitPrintForm" in request.POST.keys():
        # Avant toute chose, on vérifie l'état de l'imprimante, sinon on redirige
            try:
                req = requests.get(URL_STATE, verify=CA)
            except requests.exceptions.ConnectionError:
                return django.shortcuts.render_to_response("impressions/affichage.html")
            login = get_login(request)
            form  = PrintForm(request.POST)
            if form.is_valid():
                copies =  form.cleaned_data['copies']

                # On formate les options pour la couleur
                cl = form.cleaned_data['couleur']
                if cl=="couleur":
                    couleur = True
                else:
                    couleur = False

                format = form.cleaned_data['format']

                # On formate la disposition
                disp = form.cleaned_data['disposition']
                if disp=="recto-verso":
                    rv = True
                else:
                    rv = False
                if disp=="livret":
                    livr = True
                else:
                    livr = False

                # On formate les options pour les agrafes
                agra = form.cleaned_data['agrafes']
                if agra=="hg":
                    agrafe="TopLeft"
                elif agra=="hd":
                    agrafe="TopRight"
                elif agra=="g":
                    agrafe="Left"
                elif agra=="d":
                    agrafe="Right"
                else:
                    agrafe="None"

                perforation = form.cleaned_data['perforation']

                # On récupère le nom du fichier auprès de la form qui va bien
                filename = form.cleaned_data['filenom']
                # On prend dans la db, le plus recent, qui correspond au nom de fichier et à l'user
                jo = Jobs.objects.filter(file=filename,login=login).latest('id')
                if jo.club:
                    login = jo.club
                job = impression("/home/impressions/%s/%s" % (login, filename), adh=login)
                # On met à jour les options et on envoie
                job.changeSettings(papier=format,couleur=couleur,copies=copies,agrafage=agrafe,perforation=perforation,livret=livr,recto_verso=rv)
                try:
                    job.imprime()
                except ValueError:
                    messages.error(request, u"""Solde insuffisant pour effectuer l'impression""" )
                    jo.result = "Cancelled"
                    jo.save()
                    return django.shortcuts.redirect("/impressions/")
                # On récupère le job id
                jo.jid = job.get_jid()
                jo.result = "Running"
                jo.prix = float(job._calcule_prix())
                jo.save()
                unused_digits=Code.gen_random_code()
                code = Code.objects.create(digits=unused_digits, owner=request.user)
                messages.success(request, u"""Votre fichier a bien été envoyé à l'impression \n Le code pour entrer dans le batiment J est B7806 \n Un code pour entrer dans le local impression a été généré : """ + unicode(unused_digits) + u"""#""")
                return django.shortcuts.redirect("/impressions/")
            else:
                 return HttpResponseBadRequest(u"Formulaire non pris en charge.")
        else:
            return HttpResponseBadRequest(u"Formulaire non pris en charge.")

@login_required
def gestion(request):
    login = get_login(request)
    imprimante_jobs = []
    lpq_jobs = []

    if not is_imprimeur(request):
        # On peuple avec les logins clubs de l'adh
        logins = [ unicode(cl[0]) for cl in get_club(request)]
        logins.append(login)
        ended_jobs = Jobs.objects.filter(login__in = logins)
    else:
        ended_jobs = Jobs.objects.all().order_by("-starttime")[:50]

    #On récupère les jobs en attente
    class current_jobs:
        def __init__(self):
            self.login="None"
            self.file="Unknown"

    lpq_proc = subprocess.Popen(["lpq"], stdout=subprocess.PIPE)
    lpq_request = lpq_proc.stdout.readlines()[2:]

    # Si il y a des jobs en attente, on peuple la file lpq_jobs
    if lpq_request:
        for job in lpq_request:
            job = job.split()
            current = current_jobs()
            current.jid = job[2]
            # Important : sur l'intranet, le login est dans la tache (owner : www-data)
            try:
                current.file = job[3].split(":")[2]
                current.login = job[3].split(":")[1]
            # Si c'est pas une tache intranet, on traite classiquement
            except IndexError:
                current.file = job[3]
                current.login = job[1]
            current.ordre = job[0]


            # On affiche si imprimeur ou proprio de la tache, ou respo des clubs concernés
            if current.login == login or current.login in [ unicode(cl[0]) for cl in get_club(request)] or is_imprimeur(request):
                lpq_jobs.append(current)

    # On précise si la personne est imprimeur
    imprimeur = is_imprimeur(request)

    return render_to_response("impressions/liste.html",
                              {"ended_jobs" : ended_jobs,
                               "lpq_jobs" : lpq_jobs,
                               "imprimeur" : imprimeur,
                               "imprimante_jobs" : imprimante_jobs},
                              context_instance=RequestContext(request))


def make_recredit(request, crans_jid, imprimeur_not_required=False):
    # SI impression en tant que club, on change le login
    # On effectue le recredit également en récupérant le prix de la tache
    prix = False
    owner = False
    login = get_login(request)
    try:
        jo = Jobs.objects.get(jid=crans_jid)
        if jo.club:
            login = jo.club
            owner = jo.club
        else:
            owner = jo.login
        prix = jo.prix
        crans_jid = jo.jid
        task = jo.file

        # On vérifie qu'on a bien les droits
        if owner == login and imprimeur_not_required or is_imprimeur(request):
        # On est obligé de recréditer par l'intermediaire de www-data, l'user n'en a pas le droit
        # TODO : quand les impressions seront des factures, il suffira de detruire la facture correspondante au jid
            if crans_jid and prix:
                ldap = shortcuts.lc_ldap_admin()
                adherent = ldap.search(u'uid=%s' % owner, mode="w")[0]
                with adherent as adh:
                    adh.solde(prix, u"Impression ratee, jid=%s, tache=%s" % (crans_jid, task),login=login)
                    adh.save()
                jo.result = "Cancelled"
                jo.save()
                messages.success(request, u"La tâche %s a été annulée." % (task))
        else:
            return "Denied"
    except:
        return "Introuvable"

@login_required
def delete_job(request, jid=-1):
    # Supprime les taches de la queue et recrédite
    crans_jid = None
    # On récupère le job
    try:
        jid = int(jid)
    except:
        pass

    # On récupère la file d'attente actualisée
    lpq_proc = subprocess.Popen(["lpq"], stdout=subprocess.PIPE)
    lpq_jobs = lpq_proc.stdout.readlines()[2:]

    # On garde selon le jid et on supprime
    lpq_jobs = [job.split() for job in lpq_jobs if int(job.split()[2]) == jid]
    if lpq_jobs == []:
        messages.error(request, u"Cette tâche n'est plus dans la file d'attente du serveur.")
    else:
        job = lpq_jobs[0]
        try:
            crans_jid = job[3].split(":")[0]
            result = make_recredit(request, crans_jid, imprimeur_not_required=True)
            if result == "Denied":
                messages.error(request, u"Vous n'êtes pas le propriétaire de cette tâche, vous ne pouvez pas l'annuler.")
                return HttpResponseRedirect("/impressions/gestion/")
            elif result == "Introuvable":
                messages.error(request, u"Tache introuvable, impossible de recréditer")
            subprocess.Popen(["cancel",str(jid)], stdout=subprocess.PIPE)
        except IndexError:
            messages.error(request, u"Impossible de recréditer la tache")

    return HttpResponseRedirect("/impressions/gestion/")

@login_required
def lost_job(request, jid=-1):
    # Permet aux imprimeurs de recréditer les taches paumées entre cups et l'imprimante
    # On récupère le job
    try:
        jid = int(jid)
    except:
        messages.error(request, u"Action impossible, jid introuvable")
        return HttpResponseRedirect("/impressions/gestion/")

    result = make_recredit(request, jid, imprimeur_not_required=False)
    if result == "Denied":
        messages.error(request, u"Droits imprimeurs requis")
        return HttpResponseRedirect("/impressions/gestion/")
    elif result == "Introuvable":
        messages.error(request, u"Tache introuvable, impossible de recréditer")
    return HttpResponseRedirect("/impressions/gestion/")


def erreur(request):
    return django.shortcuts.render_to_response("impressions/affichage.html")
