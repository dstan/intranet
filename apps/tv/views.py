# Create your views here.
# -*- coding: utf-8 -*-

import imp
import pickle
import ipaddr
import urllib
import hashlib
import datetime

import gestion.secrets_new as secrets
import tv.dns as tv_dns
import tv.radio.config as tv_config
#from gestion import config.dns

from django.conf import settings
import django.shortcuts
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound
#from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import User
from django.template import RequestContext
from django.core.urlresolvers import reverse

from decorators import request_passes_test

import gestion.config
import gestion.config.tv

net_tv = [ipaddr.IPNetwork(net) for net in gestion.config.NETs['fil'] + gestion.config.prefix['fil']]
net_crans = [ipaddr.IPNetwork(net) for net in gestion.config.NETs['all'] + gestion.config.prefix['subnet']]

SAP_FILE_URL = gestion.config.tv.SAP_FILE_PIC
BASE_IMAGE_URL = gestion.config.tv.BASE_IMAGE_URL
IMAGE_SUFFIX = gestion.config.tv.IMAGE_SUFFIX
SMALL_IMAGE_SUFFIX = gestion.config.tv.SMALL_IMAGE_SUFFIX

def radio_password(user, radio=""):
    """Génère (de façon déterministe) un mot de passe pour un (utilisateur, station radio"""
    if radio.startswith("/"):
        radio = radio[1:]
    hash = hashlib.md5("%s~%s~%s~%s" % (user.username, datetime.datetime.now().date(), settings.SECRET_KEY, radio))
    return hash.hexdigest()

def get_client_ip(request):
    """Retourne l'ip source d'une requête"""
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def image_url_for_channel(channel_ip, small=0 ):
   if small:
      return BASE_IMAGE_URL + str(channel_ip) + IMAGE_SUFFIX
   else:
      return BASE_IMAGE_URL + str(channel_ip) + SMALL_IMAGE_SUFFIX

def chaines():
    """Retourne le dico des chaines disponibles en multicast"""
    try:
        multicast=pickle.load(open(SAP_FILE_URL))
        for group in multicast.keys():
            multicast[group]=dict([(unicode(key, 'utf-8'), value) for (key, value) in multicast[group].items()])
        return multicast
    except:
        return {}

def check_local(request):
    """Vérifie si une ip est locale au réseaux crans"""
    ip=ipaddr.IPAddress(get_client_ip(request))
    return True in [ip in net for net in net_crans]

def check_local_or_logged(request):
    """ip locale ou user authentifié"""
    return check_local(request) or request.user.is_authenticated()

@request_passes_test(check_local_or_logged)
def tv(request, autoplay_type=None, autoplay_url=None):
    """Page d'index du module TV : affiches les chaines et stations radios disponibles"""
    # Si rediriger avec de l'autoplay, on mémorise et cleanup l'url
    if request.GET.get('autoplay_type', None) and request.GET.get('autoplay_url', None):
        request.session['autoplay_type'] = request.GET['autoplay_type']
        request.session['autoplay_url'] = request.GET['autoplay_url']
        request.session['autoplay_volume'] = request.GET.get('autoplay_volume', '100')
        return redirect("tv:index")

    # TV uniquement en filaire
    ip=ipaddr.IPAddress(get_client_ip(request))
    if not True in [ip in net for net in net_tv]:
        messages.warning(request, "Vous devez être connecté en filaire au réseau du Cr@ns pour pouvoir regarder la télévision.")
        tnt=[]
        sat=[]
    else:
        tnt=[
         (name,
          "udp://@%s:1234" % tv_dns.idn(name, fqdn=True, charset=None),
          image_url_for_channel(ip),
          reverse("tv:tv_playlist", kwargs={'tv': name})
         ) for (name, ip) in chaines().get('TNT', {}).items()]
        sat=[
         (name,
          "udp://@%s:1234" % tv_dns.idn(name, fqdn=True, charset=None),
          image_url_for_channel(ip),
          reverse("tv:tv_playlist", kwargs={'tv': name})
         ) for (name, ip) in chaines().get('SAT', {}).items()]

    # Radio accessible de partout : ip crans en non connecté seulement
    radio = []
    user_auth = request.user.is_authenticated()
    for (name, params) in tv_config.multicast['Radio'].items():
        if user_auth:
            userpass = "%s@" % (request.user.username,)
        else:
            userpass = ""
        radio.append(
               (name,
               "http://%stv.crans.org/%s" % (userpass, params[0]),
               image_url_for_channel(params[1]),
               reverse("tv:radio_playlist", kwargs={'radio': params[0]})),
        )

    # Tri des chaines case insensitif
    key = lambda s: s[0].lower()
    tnt.sort(key=key)
    sat.sort(key=key)
    radio.sort(key=key)

    # dico de params pour générer la page a partir du template
    params={ 'tnt' : tnt, 'sat': sat, 'radio': radio, 'user': request.user,
             'autoplay_type':request.session.get('autoplay_type', None),
             'autoplay_url':request.session.get('autoplay_url', None),
             'autoplay_volume':request.session.get('autoplay_volume', None),
    }

    # On oublie les paramètre autoplay
    request.session['autoplay_type'] = None
    request.session['autoplay_url'] = None
    request.session['autoplay_volume'] = None

    # Render de la page
    return django.shortcuts.render_to_response("tv/index.html", params, context_instance=RequestContext(request))

@request_passes_test(check_local_or_logged)
def radio_playlist(request, radio):
    """Génère un fichier de playlist pour les stations radios"""
    if radio in tv_config.multicast_tag["Radio"]:
        if request.user.is_authenticated():
            userpass = "%s:%s@" % (request.user.username, radio_password(request.user, radio))
        else:
            userpass = ""
        return HttpResponse("http://%stv.crans.org/%s\n" % (userpass, radio), content_type="audio/x-mpegurl")
    else:
        return redirect("tv:index")

def tv_playlist(request, tv):
    """Génère un fichier de playlist avec l'adresse multicast de la chaine"""
    if check_local(request):
        return HttpResponse("udp://@%s:1234\n" % tv_dns.idn(tv, fqdn=True, charset=None), content_type="audio/x-mpegurl")
    else:
        return redirect("tv:index")

def get_radio_password(request, radio=""):
    """Transmet son mot de passe radio à l'utilisateur (en ajax)"""
    if not request.user.is_authenticated():
        return HttpResponseForbidden()
    elif radio in tv_config.multicast_tag["Radio"]:
        return HttpResponse(radio_password(request.user, radio))
    else:
        return HttpResponseNotFound("Not Found")

@csrf_exempt
def icecast_auth(request):
    """API appelé par icecast pour vérifier les mots de passes"""
    actions = ["mount_add", "mount_remove", "listener_add", "listener_remove"]
    response = HttpResponse()
    response["icecast-auth-user"]=1
    response["icecast-auth-timelimit"]=86400
    if request.method == 'POST':
        POST = request.POST
        if not POST["action"] in actions:
            return HttpResponseForbidden()

        # Un client tente d'accéder a un flux radio
        if POST["action"] == "listener_add":
            ip=ipaddr.IPAddress(POST["ip"])
            # Si 127.0.0.1, l'ip viens du proxy local
            if str(ip) == "127.0.0.1":
                # On authentifie le proxy
                if POST.get('ClientHeader.x-auth', '') != secrets.get("icecast-token"):
                    # untrust proxy
                    return HttpResponseForbidden()
                else:
                    # Le proxy est authentifié, on lui fait confiance pour l'ip du client
                    ip = ipaddr.IPAddress(POST['ClientHeader.x-real-ip'])
            # Si ip crans on accepte
            if True in [ip in net for net in net_crans]:
                return response
            # Sinon, il faut s'authentifier
            elif POST["user"] and POST["pass"]:
                try:
                    user = User.objects.get(username=POST["user"])
                    # Si user/pass sont ok
                    if POST["pass"] == radio_password(user, POST['mount']):
                        return response
                except User.DoesNotExist:
                    return HttpResponseForbidden()
        # Pour les autres actions, toujours ok
        else:
            return response
    return HttpResponseForbidden()
