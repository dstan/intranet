from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    url('^$', views.tv, name="index"),
    url('^auth$', views.icecast_auth, name="icecast_auth"),
    url('^radio/(?P<radio>.+).m3u$', views.radio_playlist, name="radio_playlist"),
    url('^radio/password/(?P<radio>.+)$', views.get_radio_password, name="get_radio_password"),
    url('^(?P<tv>.+).m3u$', views.tv_playlist, name="tv_playlist"),
)
