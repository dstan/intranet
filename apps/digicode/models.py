# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
from django.utils import timezone
import datetime
import time
import random

class Code(models.Model):
    
    #On fixe la durée de validité d'un code
    TIME_TO_LIVE = datetime.timedelta(days = 3)
    DIGITS_MAXLENGTH = 6
    
    class Meta:
        ordering = ['-date'] #On trie par date décroissante par défaut
    
    digits = models.PositiveIntegerField(validators = [MaxValueValidator(999999)], unique = True)
    date = models.DateTimeField(default = timezone.now)
    owner = models.ForeignKey(User, null = True, blank = True)
    
    def __unicode__(self):
        try:
            return u'%s - #%s'%(self.owner.username, self.digits)
        except:
            return u'No user - #%s'%(self.digits)

    def remaining_time(self):
        '''
        Retourne le temps restant sous la forme d'une chaîne unicode bien formatée.
        Si le temps est négatif affiche 'Code expiré'.
        '''
        remain = self.TIME_TO_LIVE + self.date - timezone.now()
        seconds = int(remain.total_seconds())
        if seconds <= 0:
            return u'Code expiré'
        else:
            days = seconds / (3600*24)
            hours = (seconds - days*24*3600) / 3600
            minutes = (seconds - days*24*3600 - hours*3600) / 60
            
            if days > 0:
                return u'%d jour(s) %d heure(s) et %d minute(s)' % (days, hours, minutes)
            elif hours > 0:
                return u'%d heure(s) et %d minute(s)' % (hours, minutes)
            else:
                return u'%d minute(s)' % (minutes)
    
    @classmethod
    def exists(cls, digits):
        return int(digits) in [obj.digits for obj in cls.objects.all()]

    @classmethod
    def gen_random_code(cls):
        '''
        Établi la liste des codes exitants et retourne un code aléatoire non utilisé.
        '''
        existing_codes = [obj.digits for obj in cls.objects.all()]
        remaining_codes = set(range(100000, 1000000)).difference(existing_codes)
        return random.choice(list(remaining_codes))

    @classmethod
    def clear_old(cls):
        """
        Efface les codes expirés.
        """
        for code in cls.objects.all():
            remain = code.TIME_TO_LIVE + code.date - timezone.now()
            if int(remain.total_seconds()) <= 0:
                code.delete()
