# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.debug import sensitive_post_parameters
from models import Code
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.utils.importlib import import_module
import datetime

from django.utils import timezone
import os
if os.getenv('DBG_INTRANET', None):
    digicode_pass = 'secret:' + os.getenv('DBG_INTRANET')
else:
    from gestion import secrets_new as secrets
    digicode_pass = secrets.get('digicode_pass')

conn_pool = import_module('conn_pool', 'intranet')

@login_required
def view_code(request):
    '''
    Choix de la méthode d'affichages des codes.
    Si l'on est nounou ou imprimeur on voit les codes de tout le monde, sinon que les siens.
    '''
    if request.user.groups.filter(name = 'crans_nounou') or request.user.groups.filter(name = 'crans_imprimeur'):
        liste_codes = Code.objects.all()
        return render(request, 'digicode/digicode.html', {'liste_codes' : liste_codes, 'CAN_GENERATE' : True})
    else:
        liste_codes = Code.objects.filter(owner = request.user)
        return render(request, 'digicode/digicode.html', {'liste_codes' : liste_codes, 'CAN_GENERATE' : False})

@csrf_exempt #On autorise l'envoie de POST provenant d'un autre serveur
@sensitive_post_parameters('password') #On évite d'envoyer le mdp par mail
def delete_code(request):
    '''
    Méthode de suppression de code par l'envoi d'un POST.
    Le POST doit contenir un mot de passe partagé et le code à supprimer.
    '''
    response = HttpResponse()
    if request.method == 'POST':
        post_dict = request.POST
        if post_dict['password'] == digicode_pass: #On compare au mot de passe de secrets
            to_delete = Code.objects.filter(digits = post_dict['code'])
            if to_delete:
                to_delete.delete()
                response.content = u'Code Successfully Deleted'
            else:
                response.content = u'Code Does Not Exist'
        else:
            response.content = u'Bad Password'
    else:
        response.content = u'Bad Method'
    return response

@csrf_exempt
@sensitive_post_parameters('password')
def create_code(request, digits=None):
    '''
    Méthode de création de code par l'envoi d'un POST.
    Le POST doit contenir le mot de passe partagé.
    '''
    response = HttpResponse()
    if request.method == 'POST':
        post_dict = request.POST
        if post_dict['password'] == digicode_pass: #On compare au mot de passe de secrets
            if post_dict['user']:
                if digits and Code.exists(digits):
                        response.content = "Code already used"
                else:
                    if digits:
                        digits = digits
                    else:
                        digits = Code.gen_random_code()
                    code_obj = Code(digits=digits)
                    try:
                        code_obj.owner = User.objects.get(username=post_dict['user'])
                        response.content = unicode(digits)
                        code_obj.save()
                    except User.DoesNotExist:
                        # user will have an "unusable" password
                        luser=conn_pool.get_conn(request.user).search(u"uid=%s" % post_dict['user'])
                        if luser:
                            user = User.objects.create_user(unicode(post_dict['user']), '')
                            user.save()
                            code_obj.owner = user
                            response.content = unicode(digits)
                            code_obj.save()
                        else:
                            response.content = "User %s does not exists" % post_dict['user']
            else:
                response.content = u"No user specified"
        else:
            response.content = u"Bad Password"
    else:
        response.content = u"Bad Method"
    return response

@csrf_exempt
@sensitive_post_parameters('password')
def list_code(request, owner=None):
    '''
    Méthode pour lister les code par l'envoi d'un POST.
    Le POST doit contenir le mot de passe partagé.
    '''
    response = HttpResponse()
    if request.method == 'POST':
        post_dict = request.POST
        if post_dict.get('password', '') == digicode_pass: #On compare au mot de passe de secrets
                if owner:
                    code_objs = Code.objects.filter(owner__username=owner, date__gte=(timezone.now() - Code.TIME_TO_LIVE))
                else:
                    code_objs = Code.objects.filter(date__gte=(timezone.now() - Code.TIME_TO_LIVE))
                response.content = u""
                for code_obj in code_objs:
                    age = timezone.now() - code_obj.date
                    response.content+="%s,%s,%s\n" % (code_obj.digits, int(age.total_seconds()), code_obj.owner)
        else:
            response.content = u"Bad Password"
    else:
        response.content = u"Bad Method"
    response['Content-Length'] = len(response.content)
    return response

