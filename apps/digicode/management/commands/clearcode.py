# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from apps.digicode.models import Code

class Command(BaseCommand):
    help = u"Efface les codes périmés"

    def handle(self, *args, **options):
        Code.clear_old()
        self.stdout.write(u"Tous les vieux codes ont bien été effacés")
