# -*- coding: utf-8 -*-

from django import forms
import models

class DigicodeForm(forms.ModelForm):
    fields = ('digits', 'date', 'owner', )
    class Meta:
        model = models.Code
        fields = ('digits', 'date', 'owner', )
        widgets = {
            # On surcharge le TextInput pour limiter à 6 caractères sur les navigateurs récents
            # À changer lors de la sortie de django 1.6 pour une méthode plus propre !
            'digits' : forms.TextInput(attrs = {'maxlength' : model.DIGITS_MAXLENGTH, 'size' : model.DIGITS_MAXLENGTH})
        }
