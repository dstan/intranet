#~*~ coding: utf-8 ~*~
from django.contrib import admin
import models
import forms


class DigicodeAdmin(admin.ModelAdmin):
    form = forms.DigicodeForm
    exclude = ('date',)
    
    def get_form(self, request, obj=None, **kwargs):
        '''
        Récupère l'utilisateur courant et le met par défaut owner.
        '''
        form = super(DigicodeAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['owner'].initial = request.user
        form.base_fields['digits'].initial = models.Code.gen_random_code()
        return form


admin.site.register(models.Code, DigicodeAdmin)
