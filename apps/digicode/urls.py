# -*- coding: utf-8 -*-

from django.conf.urls import url
from models import Code
import views

urlpatterns = [
    url(r'^$', views.view_code),
    url(r'^delete/$', views.delete_code),
    url(r'^create/(?P<digits>[0-9]{%s})?$' % Code.DIGITS_MAXLENGTH, views.create_code),
    url(r'^list/(?P<owner>[^/]*)$', views.list_code),
]
