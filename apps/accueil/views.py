# -*- coding: utf-8 -*-
#
# ACCUEIL.PY
#
# Copyright (C) 2010 Antoine Durand-Gasselin
# Author: Antoine Durand-Gasselin <adg@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

from django.shortcuts import render
from django.template import RequestContext

import settings
from django.contrib.auth.decorators import login_required
from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')

@login_required
def view(request):
    '''Affiche toutes les applications disponibles en fonction des
    droits de la personne connectée.'''
    return render(request, "accueil.html")
