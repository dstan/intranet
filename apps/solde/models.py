# -*- coding: utf-8 -*-

from django.db import models
from lc_ldap import shortcuts
from django.core.urlresolvers import reverse

# Create your models here.

class Transaction(models.Model):
    """
        Modèle permettant de stocker une transaction de façon temporaire dans une base
        de données, en attendant sont traitement.
        L'objet créé a vocation à être détruit à la fin de la procédure.
    """
    modePaiement = models.CharField(max_length=256,
        blank=False, null= False, default='invalid',
        help_text='Mode de paiement')
    idTransaction = models.CharField(max_length=256,
        blank=False, null=True, help_text='Identifiant de la transaction')
    uid = models.CharField(max_length=256, blank=False, null=False, default=-1,
        help_text='UID ayant effectué la transaction')
    # TODO montant = models.DecimalField(max_digits=8, decimal_places=2)
    montant = models.FloatField(blank=False, null=False, default=0.00,
        help_text='Montant du rechargement')
    # TODO oldSolde = models.DecimalField(max_digits=8, decimal_places=2)
    oldSolde = models.FloatField(blank=False, null=False, default=0.00,
        help_text="Solde de l'adhérent avant le rechargement")
    valid = models.NullBooleanField(
        help_text="""Indique l'état de la transaction (NULL=en cours;"""
        """True=effectuée;False=non effectuée)""")
    created = models.DateTimeField(auto_now_add=True,
        help_text="Date de création")
    class Meta:
        unique_together = ('modePaiement', 'idTransaction', )

    def get_absolute_url(self):
        return reverse("solde:credite", args=[self.pk])

    def credite(self):
        """Modifie le solde de l'adhérent en créant la facture"""
        ldap = shortcuts.lc_ldap_admin()
        fldif = {
            'article': [u'SOLDE~~Rechargement ComNPay~~1~~%.2f' % self.montant],
            'modePaiement': [u'comnpay'],
        }

        with ldap.search(u'uid=' + unicode(self.uid), mode='rw')[0] as adherent:
            with ldap.newFacture(adherent.dn, fldif) as new_facture:
                new_facture.crediter()

        # On met à jour la transaction dans la base de données (On indique qu'elle a réussi)
        self.valid = True
        self.save()
