# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('modePaiement', models.CharField(default=b'invalid', help_text=b'Mode de paiement', max_length=256)),
                ('idTransaction', models.CharField(help_text=b'Identifiant de la transaction', max_length=256, null=True)),
                ('uid', models.CharField(default=-1, help_text=b'UID ayant effectu\xc3\xa9 la transaction', max_length=256)),
                ('montant', models.FloatField(default=0.0, help_text=b'Montant du rechargement')),
                ('oldSolde', models.FloatField(default=0.0, help_text=b"Solde de l'adh\xc3\xa9rent avant le rechargement")),
                ('valid', models.NullBooleanField(help_text=b"Indique l'\xc3\xa9tat de la transaction (NULL=en cours;True=effectu\xc3\xa9e;False=non effectu\xc3\xa9e)")),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='transaction',
            unique_together=set([('modePaiement', 'idTransaction')]),
        ),
    ]
