#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

# Application solde, utilise comnpay, importe le module 
# python comnpay situé à la racine
# Donne la liste des remboursements passé (fid), et la liste 
# complète des opérations
# Codé par Hamza

import django.shortcuts
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.debug import sensitive_variables
from django.views.decorators.http import require_POST
from django.utils.importlib import import_module
from django.utils.datastructures import MultiValueDictKeyError

import gestion.secrets_new as secrets

from settings import ROOT_URL
from models import Transaction

import re
import os
from decimal import Decimal
from collections import OrderedDict

from lc_ldap import shortcuts
from comnpay.payment import Payment

if os.getenv('DBG_COMNPAY'):
    comnpay_tpe, comnpay_secret = os.getenv('DBG_COMNPAY').split('@', 1)
    comnpay_url = 'https://secure-homologation.comnpay.com'
else:
    comnpay_secret = secrets.get('comnpay_secret')
    comnpay_tpe = secrets.get('comnpay_tpe')
    comnpay_url = 'https://secure.comnpay.com'

conn_pool = import_module('conn_pool','intranet')

@login_required
def accueil(request):
    """
        Affiche la page principale pour la consultation de son solde.
    """
    # Récupération de l'utilisateur LDAP
    ldap_user = conn_pool.get_user(request.user)

    # On vérifie si l'utilisateur a un solde sinon on met 0.00€
    solde = _decimal(ldap_user.get("solde", [0])[0])

    # On récupère l'historique des transactions
    full_history = ldap_user["historique"]

    # On prépare les regex pour parser l'historique
    lign_parser = re.compile(r'^(?P<date>[:/0-9 ]*), .* : (?P<op>(debit|credit)) (?P<montant>[.0-9]*) Euros( \[(?P<what>.*)\])?')
    impression_re = re.compile(r'^impression\((?P<number>\d+)\): (?P<filename>.+)(?: par \w+)?$')

    # On construit la liste de tuples qu'on utilisera dans le template (date,intitulé,montant,credit|debit)
    operations = list()
    for lign in full_history:
        m = lign_parser.match(lign.value)
        if m:
            what = m.group('what')
            if what:
                impression_match = impression_re.match(what)
                if impression_match:
                    what = 'Impression %s : %s' % (
                        impression_match.group('number'),
                        impression_match.group('filename').split('/')[-1][:30],
                    )
            operations.append((
                lign.get_datetime(),
                what,
                _decimal(m.group('montant')),
                m.group('op'),
            ))

    # On y retrie par date décroissante
    operations.reverse()

    # On utilise un paginateur pour que l'utilisateur s'y retrouve (20 éléments par page par défaut)
    p = Paginator(operations,20)

    if 'page' in request.GET.keys():
        try:
            page = p.page(request.GET['page'])
        except (EmptyPage, PageNotAnInteger) as e:
            page = p.page(1)
    else:
        page = p.page(1)

    return django.shortcuts.render(request, "solde/accueil.html", {
        "session": request.session,
        "solde": solde,
        "operations": page.object_list,
        "num_pages": p.num_pages,
        "current_page": page,
    })

def _decimal(montant):
    """Renvoie le decimal avec deux chiffres"""
    return Decimal('%.2f' % float(montant))

def _clean_montant(montant):
    """
        Traite une chaine de caractères représentant une somme d'argent pour la mettre au format
        u'x.y' où x est un entier naturel et y un entier naturel avec 00 <= y <= 99.
        Renvoie None si montant est incorrect ou u'x.y' avec float(u'x.y') >= 0.
    """
    montant_final = None

    money_pattern = re.compile(r'(?P<entier>[0-9]+)[.,]?(?P<cents>[0-9]*).*?')
    m = money_pattern.match(montant)
    if m:
        entier,cents = m.group('entier'),m.group('cents')
        if len(cents) < 2:
            cents += u'00'

        montant_final = entier + u"." + cents[0:2]

    return montant_final

def _clean_moyen(moyen,pretty=False):
    """
        Vérifie que la chaine de caractères moyen est bien un des moyens de paiement accepté par
        le Cr@ns et renvoie la chaine si c'est le cas, 'invalid' sinon.
        L'argument optionnel pretty est un booléen indiquant que l'on veut récupérer la chaine sous
        une forme "présentable". (Par exemple, 'paypal' -> 'PayPal', 'note' -> 'Note Kfet 2015' ou autre ...)
    """
    # available_moyen est un dictionnaire dont les éléments sont de la forme 'moyen' : 'prettyMoyen'
    available_moyen = {
        'comnpay': 'ComNPay',
        'paypal': 'PayPal',
        'note': 'Note Kfet 2015',
    }

    if moyen in available_moyen.keys():
        return available_moyen[moyen] if pretty else moyen

    return 'Invalide' if pretty else 'invalid'


@csrf_exempt
@sensitive_variables('idTpe', 'idTransaction', 'comnpay_secret', 'comnpay_tpe','p')
def rechargerIPN(request):
    """
        Traite l'étape 4 (côté Crans) du rechargement d'un compte.
        Etape 4 (Cr@ns) : Le serveur ComNPay renvoie les informations de paiement sur lesquelles
                          on se base pour créer la facture et recharger le compte de l'adhérent.
        Cette view est chargée par ComNPay directement (callback) et il n'y
        donc pas de csrf_token fourni. Pas grave car on fait une vérification
        à partir de la transaction.
    """

    # Vérification des informations envoyées par l'appelant de l'URL
    p = Payment()

    # Calcule un dictionnaire ordonné car validSec() ci-dessous réalise un hash
    # dépendant de l'ordre (sic)
    order = ('idTpe', 'idTransaction', 'montant', 'result', 'sec', )
    try:
        data = OrderedDict([(f, request.POST[f]) for f in order])
    except MultiValueDictKeyError:
        return HttpResponseBadRequest("HTTP/1.1 400 Bad Request")

    if not p.validSec(data, comnpay_secret):
        return HttpResponseBadRequest("HTTP/1.1 400 Bad Request")

    # A partir d'ici, on fait confiance au serveur qui a envoyé les données ComNpay
    result = True if (request.POST['result'] == 'OK') else False
    idTpe = request.POST['idTpe']
    idTransaction = request.POST['idTransaction']

    # On vérifie que le paiement nous est destiné
    if not idTpe == comnpay_tpe:
        return HttpResponseBadRequest("HTTP/1.1 400 Bad Request")

    transaction = django.shortcuts.get_object_or_404(Transaction,
        idTransaction=idTransaction, modePaiement='comnpay')

    # On vérifie que le paiement est valide
    if not result:
        # Le paiement a échoué : on effectue les actions nécessaires (On indique qu'elle a échoué)
        transaction.valid = False
        transaction.save()

        # On notifie au serveur ComNPay qu'on a reçu les données pour traitement
        return HttpResponse("HTTP/1.1 200 OK")

    transaction.credite()

    # A nouveau, on notifie au serveur qu'on a bien traité les données
    return HttpResponse("HTTP/1.0 200 OK")


@csrf_exempt     # Le serveur qui redirige l'utilisateur ne peut pas mettre son csrf_token --> csrf_exempt pour eviter une 403 inutile
@login_required
def rechargerResponse(request):
    """
        Traite l'étape 4 (coté client) du rechargement d'un compte.
        Etape 4 (client) : Affiche un message de retour (Succès ou Echec) au client
                           et indique selon les cas le nouveau solde, le message d'erreur ...
    """

    try:
        transaction = django.shortcuts.get_object_or_404(Transaction,
            idTransaction=request.POST['transactionId'],
            modePaiement=request.GET['moyen'],
        )
    except MultiValueDictKeyError:
        return django.shortcuts.redirect('solde:transactions')

    # Données de la transaction
    result = True if (request.POST['result'] == 'OK') else False
    moyen = _clean_moyen(transaction.modePaiement,pretty=True)
    err_msg = request.POST['reason']
    old_solde = _decimal(transaction.oldSolde)
    montant = _decimal(transaction.montant)

    new_solde = old_solde
    if result:
        new_solde += montant

    return django.shortcuts.render(request, "solde/recharger_response.html", {
        'session': request.session,
        'result': result,
        'moyen': moyen,
        'montant': montant,
        'new_solde': new_solde,
        'err_msg': err_msg,
    })

@login_required
@sensitive_variables('form','transaction','comnpay_secret','comnpay_tpe','p')
def recharger(request):
    """
        Affiche la page permettant de recharger son compte Crans, quelque soit le mode de paiement.
        Traite les étapes 1 --> 3 des demandes de rechargement de compte Cr@ns.
        Etape 1 : Saisie du montant.
        Etape 2 : Choix du moyen de paiement.
        Etape 3 : Redirection vers le moyen de paiement choisi
    """
    ldap_user = conn_pool.get_user(request.user)

    step = 1
    # Si l'utilisateur demande un rechargement
    if 'step' in request.GET.keys():
        step = int(request.GET['step'])

        # Etape 1 : On récupère le montant entré par l'utilisateur
        if step == 1:
            montant = _clean_montant(request.GET['montant'])
            if montant is None:
                return django.shortcuts.redirect('solde:recharger')
            montant = _decimal(montant)
            # Une fois le montant récupéré et traité, on lui demande son mode de rechargement
            return django.shortcuts.render(request, "solde/recharger.html", {
                "session" : request.session,
                "montant" : montant,
                "step" : 2,
            })
        # Etape 2 : On récupère le mode de rechargement de l'utilisateur
        elif step == 2:
            moyen = _clean_moyen(request.GET['moyen'])
            montant = _clean_montant(request.GET['montant'])
            if montant is None:
                # Si le montant est incorrect, on renvoie au début de la procédure
                return django.shortcuts.redirect('solde:recharger')
            montant = _decimal(montant)
            if moyen == 'comnpay':
                # Rechargement via Com'N'Pay
                p = Payment(comnpay_tpe,
                    comnpay_secret,
                    ROOT_URL + reverse('solde:rechargerResponse')[1:] + "?moyen=comnpay&result=succes&montant=%.2f" % montant,
                    ROOT_URL + reverse('solde:rechargerResponse')[1:] + "?moyen=comnpay&result=echec&montant=%.2f" % montant,
                    ROOT_URL + reverse('solde:rechargerIPN')[1:],
                    "",
                    "D")
                form = p.buildSecretHTML("Rechargement du compte Crans", float(montant))

                transaction = Transaction(
                    modePaiement='comnpay',
                    idTransaction=p.idTransaction,
                    uid=ldap_user["uid"][0].value,
                    montant=float(montant),
                    oldSolde=float(ldap_user.get("solde", [0.])[0])
                )
                transaction.save()

                # On passe à la dernière étape du rechargement (étape 3)
                return django.shortcuts.render(request, "solde/recharger.html",{
                    "montant": montant,
                    "moyen": _clean_moyen(moyen, True),
                    "step": 3,
                    "comnpay_url": comnpay_url,
                    "form": form,
                    "session": request.session,
                })
            else:
                # Le moyen de rechargement choisi n'est pas supporté
                return HttpResponseBadRequest("HTTP/1.0 400 Bad Request (mode de rechargement non supporté)")

        else:
            # step a une valeur non attendue
            return HttpResponseBadRequest("HTTP/1.0 400 Bad Request (Valeur inattendue de step)")

    # Si l'utilisateur vient d'arriver sur la page, on commence à l'étape 1
    return django.shortcuts.render(request, "solde/recharger.html", {
        "step": 1,
        "session": request.session,
    })

@login_required
def factures(request):
    """
        Affiche la liste des factures PayPay ou ComNPay de l'utilisateur.
    """
    # Récupération de l'utilisateur LDAP
    ldap_user = conn_pool.get_user(request.user)

    # On récupère les factures de l'utilisateur et garde celle qui parlent de PayPal ou Com'N'Pay
    ldap_factures = ldap_user.factures()

    factures = list()
    for f in ldap_factures:
        if f['modePaiement'][0].value not in [u'paypal', u'comnpay']:
            del f
        else:
            # Si c'est une facture paypal ou comnpay, on la met dans un tuple (fid,date,modePaiement,montant)
            factures.append((f['fid'][0].value,f['recuPaiement'][0].value,f['modePaiement'][0].value,f['article'][0].value['pu'],))

    factures.reverse()

    return django.shortcuts.render(request, "solde/factures.html", {
        "factures": factures,
        "session": request.session,
    })

@permission_required('solde.change_transaction')
def credite(request, tid):
    transaction = django.shortcuts.get_object_or_404(Transaction, pk=int(tid))
    back = reverse('admin:solde_transaction_change', args=[tid])
    if transaction.valid is not None:
        return django.shortcuts.render(request, "solde/credited.html", {
            'tr': transaction,
            'back': back,
        })
    if request.method == 'POST':
        transaction.credite()
        return django.shortcuts.redirect(back)
    return django.shortcuts.render(request, "solde/confirm_credite.html", {
        'tr': transaction,
        'confirm_cancel_url': back,
    })
