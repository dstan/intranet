from django.conf.urls import patterns as patterns
from django.conf.urls import url

import views

urlpatterns = patterns('',
    url('^$', views.accueil, name="transactions"),
    url('^recharger$', views.recharger, name="recharger"),
    url('^recharger/response$', views.rechargerResponse, name="rechargerResponse"),
    url('^recharger/ipn$', views.rechargerIPN, name="rechargerIPN"),
    url('^factures$', views.factures, name="factures"),
    url('^credite/(?P<tid>[0-9]+)/', views.credite, name="credite"),
)
