# Create your views here.

import django.shortcuts
from django.contrib.auth.decorators import login_required

from django.template import RequestContext

@login_required
def bonjour(request):
    return django.shortcuts.render_to_response("dummy/bonjour.html", {"session" : request.session }, context_instance=RequestContext(request))
