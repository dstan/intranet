from django.conf.urls import patterns as patterns

import views

urlpatterns = patterns('',
    ('^mozilla.xml$', views.mozilla)
)
