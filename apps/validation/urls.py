# -*- encoding: utf-8 -*-

from django.conf.urls import include, patterns, url

import views

urlpatterns = patterns('',
    # Ceci est la vue appelée par les scripts désireux d'enregister une
    # nouvelle action
    url('^register/(.*)/$', views.register, name='register'),

    # Fonctions de callback (le name doit correspondre à celui fourni
    # dans ConfirmAction.view_name
    url('^$', views.afficher, name='afficher'),
    url('^upload/(\d+)/(.*)/$', views.upload, name='upload'),
    url('^demenagement/(\d+)/(.*)/$',views.demenagement, name='demenagement'),
    # TODO
    # faire de même pour:
    # * chbre invalide
    # * compte inactif
    # * machine inutilisée depuis n jours (cf vue "keep" de l'app "machines")
)

