# -*- coding: utf-8 -*-
from django import forms

from django.core.validators import RegexValidator

class DemenagementForm(forms.Form):
    """
        Formulaire à remplir par l'utilisateur pour les changements de chambre.
    """
    keep_connection = forms.BooleanField(required=False,
        initial=True,
        label=u'Je souhaite conserver ma connexion Cr@ns')
    status = forms.ChoiceField(required=True,
        initial='is_ext',
        choices=(
                    ('is_ext', u"J'habite à l'extérieur du campus de Cachan"),
                    ('is_crous_cachan', u"J'habite sur le campus de Cachan"),
                ),
        label=u'Nouveau statut',
    )
    new_chbre = forms.CharField(
            max_length=255,
            initial='',
            label=u'Nouvelle chambre (Si tu es sur le campus de Cachan)',
            required=False)
    adresse_rue = forms.CharField(max_length=510, label=u'Rue', required=False)
    adresse_code = forms.IntegerField(
            min_value=1000,
            max_value=99999,
            label=u'Code postal',
            required=False)
    adresse_ville = forms.CharField(max_length=255, label=u'Ville', required=False)

    def clean(self):
        """
            Effectue la vérification des informations fournies par l'adhérent, à savoir:
            - Chambre manquante alors que l'adh déclare habiter sur le campus.
        """
        # On effectue un premier nettoyage des informations
        cleaned_data = super(forms.Form,self).clean()
        keep_connection = cleaned_data.get('keep_connection')
        status = cleaned_data.get('status')
        new_chbre = cleaned_data.get('new_chbre').upper()
        adresse_rue = cleaned_data.get('adresse_rue')
        adresse_code = cleaned_data.get('adresse_code')
        adresse_ville = cleaned_data.get('adresse_ville')

        if status == 'is_crous_cachan':
            # L'information new_room est-elle manquante ?
            if new_chbre == '' or new_chbre == None:
                raise forms.ValidationError(u"Tu n'as pas indiqué ta nouvelle chambre.")

            cleaned_data['new_chbre'] = unicode(new_chbre)
        else:
            # L'adhérent est hors campus
            cleaned_data['new_chbre'] =  u'EXT'

        if keep_connection:
            if not adresse_rue:
                raise forms.ValidationError(u"Tu n'as pas spécifié de rue pour ta nouvelle adresse.")
            if not adresse_code:
                raise forms.ValidationError(u"Tu n'as pas spécifié de code postal pour ta nouvelle adresse.")
            if not adresse_ville:
                raise forms.ValidationError(u"Tu n'as pas spécifié de ville pour ta nouvelle adresse.")
        else:
            # Il ne veut plus de connexion, il n'a pas besoin d'adresse
            cleaned_data.update({
                'adresse_rue': u'',
                'adresse_code': 0,
                'adresse_ville': u'',
            })

        return cleaned_data

class SearchForm(forms.Form):
    """ Formulaire de recherche pour le cableur """
    alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')
    login = forms.CharField(label=u'Login', max_length=40, required=False, validators=[alphanumeric])
    chambre = forms.CharField(label=u'Chambre', max_length=5, required=False, validators=[alphanumeric])
    aid = forms.IntegerField(label=u'Aid', required=False)

