# -*- coding: utf-8 -*-

import random
import string
import datetime

from django.db import models
from django.core.urlresolvers import reverse

def _generate_secret():
    """Génère un mot de passe aléatoire"""
    random.seed(datetime.datetime.now().microsecond)
    chars = string.letters + string.digits
    length = 64
    return u''.join([random.choice(chars) for _ in xrange(length)])

class ConfirmAction(models.Model):
    """Une action envoyée par mail à un utilisateur, et qui demande
    confirmation"""
    view_name = models.CharField(max_length=255)

    secret = models.CharField(max_length=64, default=_generate_secret)

    created = models.DateTimeField(auto_now_add=True)

    triggered = models.DateTimeField(null=True, blank=True)

    data = models.TextField(blank=True)

    def get_absolute_url(self):
        """Il s'agit de l'url que doit charger l'utilisateur pour voir la page"""
        return reverse('validation:' + self.view_name, args=[str(self.pk), str(self.secret)])

    def __unicode__(self):
        data = self.data
        if len(data) > 64:
            data = data[0:63] + u"…"
        return u"%s (%s)" % (self.view_name, data)
