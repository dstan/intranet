# -*- coding: utf-8 -*-

import datetime
import json

from django.utils import timezone
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.template.loader import render_to_string
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required

from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')

from settings import BASE_LDAP_TEST

from django.template import RequestContext
from django.contrib import messages
from models import ConfirmAction
from forms import DemenagementForm
from forms import SearchForm
import lc_ldap.shortcuts
from lc_ldap.attributs import UniquenessError
from gestion import secrets_new as secrets
import gestion.mail as mail_module

# On regarde si on doit utiliser la base LDAP de test, parce que c'est toujours
# marrant de tester des trucs en dev, et de se faire expulser de sa chambre sur
# la base de prod.
if BASE_LDAP_TEST:
    use_ldap_admin = lc_ldap.shortcuts.with_ldap_conn(retries=2, delay=5,
        constructor=lc_ldap.shortcuts.lc_ldap_test)
else:
    use_ldap_admin = lc_ldap.shortcuts.with_ldap_conn(retries=2, delay=5,
        constructor=lc_ldap.shortcuts.lc_ldap_admin)

RESPBATS = 'respbats@crans.org'

@use_ldap_admin
def upload(request, pk, secret, ldap):
    """
    Validation d'une déconnexion. Reprogramme la fin d'une déconnexion
    pour dans 24h.
    """
    task = get_object_or_404(ConfirmAction, pk=pk, view_name='upload',
        secret=secret)
    data = json.loads(task.data)

    with ldap.search(data['dn'], mode='rw')[0] as adh:
        bl = adh['blacklist'][data['blid']].value
        if bl['fin'] == '-' and 'confirm' in request.POST:
            bl['fin'] = bl['debut'] + 24*60*60
            adh.history_add(u'validate', u'blacklist_upload (fin)')
            adh.save()
            task.triggered = timezone.now()
            task.save()
        if bl['fin'] != '-':
            fin = datetime.datetime.fromtimestamp(bl['fin'])
            return render(request, 'validation/upload_done.html',
                {'fin': fin})
        else:
            return render(request, 'validation/upload.html')

@use_ldap_admin
@csrf_exempt
def demenagement(request, pk, secret, ldap):
    """
    Validation d'un déménagement. Propose les alternatives suivantes:
    - Conservation de l'accès Internet (avec nouvelle chambre ou EXT)
    - Suppression de l'accès Internet
    """

    task = get_object_or_404(ConfirmAction, pk=pk, view_name='demenagement',
        secret=secret)
    data = json.loads(task.data)

    f = DemenagementForm(label_suffix="")
    chbre = data['chbre']

    if isinstance(task.triggered, datetime.datetime) \
    and task.triggered < timezone.now():
        # L'url a déjà été triggered, on empêche un nouvel accès, mais on
        # remercie d'être passé
        return render(request, 'validation/demenagement_succes.html')

    if request.method == 'POST':
        # On traite un formulaire de déménagement
        f = DemenagementForm(request.POST, label_suffix="")

        if f.is_valid():
            keep_connection = f.cleaned_data['keep_connection']
            status = f.cleaned_data['status']
            new_chbre = f.cleaned_data['new_chbre']
            adresse_rue = f.cleaned_data['adresse_rue']
            adresse_code = f.cleaned_data['adresse_code']
            adresse_code = unicode('%05d' % adresse_code)
            adresse_ville = f.cleaned_data['adresse_ville']

            if keep_connection and status != 'is_crous_cachan':
                adresse_complete = [adresse_rue, adresse_code, adresse_ville, ]
            else:
                adresse_complete = []

            with ldap.search(data['dn'], mode='rw')[0] as adh:
                if not keep_connection:
                    # L'adhérent ne souhaite plus garder sa connexion
                    # On supprime ses machines

                    for machine in adh.machines():
                        machine.delete()

                try:
                    adh['postalAddress'] = adresse_complete
                except ValueError:
                    messages.error(request, u"""Cette adresse est invalide""" )
                    return render(request, 'validation/demenagement.html', {
                      'chbre' : chbre,
                      'form' : f,
                    })
                try:
                    # On modifie la chambre de l'adhérent
                    adh['chbre'] = new_chbre

                except UniquenessError as e:
                    # 1er cas : La chambre est occupée

                    with mail_module.ServerConnection() as smtp:
                        smtp.send_template('demenagement_erreur', {
                            'from': RESPBATS,
                            'to': RESPBATS,
                            'adh': adh,
                            'aid': adh['aid'][0].value,
                            'chbre': chbre,
                            'new_chbre': new_chbre,
                            'type_erreur' : 'UniquenessError',
                            'erreur' : e,
                        })

                    return render(request, 'validation/demenagement_succes.html', {
                        'keep' : keep_connection,
                        'new_chbre' : new_chbre,
                        'ask_respbats' : True,
                        'adresse' : adresse_complete,
                        'why' : 'occupee',
                    })

                except ValueError as e:
                    # 2ème cas : La chambre est invalide

                    # On ajoute une erreur au formulaire et on le renvoie
                    f.add_error('new_chbre', "La chambre %s est invalide." % new_chbre)

                    return render(request, 'validation/demenagement.html', {
                        'chbre' : chbre,
                        'form' : f,
                    })

                except Exception as e:
                    # 3ème cas : Erreur inconnue

                    with mail_module.ServerConnection() as smtp:
                        smtp.send_template('demenagement_erreur', {
                            'from': RESPBATS,
                            'to': RESPBATS,
                            'adh': adh,
                            'aid': adh['aid'][0].value,
                            'chbre': chbre,
                            'new_chbre': new_chbre,
                            'type_erreur': 'Exception',
                            'erreur': e,
                        })

                    return render(request, 'validation/demenagement_succes.html', {
                        'keep' : keep_connection,
                        'new_chbre' : new_chbre,
                        'ask_respbats' : True,
                        'adresse' : adresse_complete,
                        'why' : 'inconnu',
                    })

                # On oublie pas de sauvegarder les modifications
                adh.history_gen(login=u'validate')
                adh.save()

            task.triggered = timezone.now()
            task.save()
            return render(request,'validation/demenagement_succes.html', {
                'keep' : keep_connection,
                'ask_respbats' : False,
                'new_chbre' : new_chbre,
                'adresse' : adresse_complete,
            })

        else:
            return render(request, 'validation/demenagement.html', {
                 'form' : f,
                 'chbre' : chbre,
            })

    else:
        # On envoie un formulaire de déménagement
        return render(request, 'validation/demenagement.html', {
              'form' : f,
              'chbre' : chbre,
        })

@csrf_exempt
def register(request, view_name):
    """Enregistre une action à effectuer. Les données d'exécution sont dans
    request.POST['data'].
    Pour être sûr que l'appelant avait le droit de faire cet appel, on vérifie
    le secret partagé dans request.POST['shared_secret'].
    Affiche en réponse une url de callback"""
    if request.POST.get('shared_secret', '') != secrets.get('validation'):
        return HttpResponseForbidden()
    task = ConfirmAction()
    task.data = request.POST.get('data', '')
    task.view_name = view_name
    task.save()
    return HttpResponse(task.get_absolute_url(), content_type="text/plain")

def is_cableur(request):
    """Renvoie True si l'utilisateur connecté a les droits"""
    luser = conn_pool.get_user(request.user)
    li = False
    if "Cableur" in luser['droits'] or "Nounou" in luser['droits']:
        li = True
    return li

@login_required
@use_ldap_admin
def afficher(request, ldap):
    """ Affiche les liens upload pour les cableurs  """
    if not is_cableur(request):
        messages.error(request, u""" Accès interdit, droits insuffisants """ )
        return HttpResponseRedirect("/")
    if request.method == "POST":
        form = SearchForm(request.POST)
        if form.is_valid():
            links = []
            adh = None
            if 'login' in form.changed_data:
                adh = ldap.search(u'uid=%s' % form.cleaned_data['login'])
            if 'chambre' in form.changed_data:
                adh = ldap.search(u'chbre=%s' % form.cleaned_data['chambre'])
            if adh:
                adh = adh[0]
                aid = adh['aid'][0]
                links = ConfirmAction.objects.filter(data__contains='"aid=%s"' % aid)
            elif 'aid' in form.changed_data:
                aid = form.cleaned_data['aid']
                links = ConfirmAction.objects.filter(data__contains='"aid=%s"' % aid)
            return render(request, "validation/affichage.html", {
                'form': form,
                'links': links,
            })
        else:
           return render(request, "validation/affichage.html", {
                'form': form,
            })
    else:
        form = SearchForm()
        context = {'form': form}
        return render(request, "validation/affichage.html", context)

