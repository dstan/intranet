# -*- coding: utf-8 -*-
# Application pour permettre aux adhérents de lister leur page perso
# Gabriel Détraz <detraz@crans.org>, Hamza Dely <dely@crans.org>

import django.shortcuts
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from models import PagePerso
from django.template import RequestContext
from django.utils.importlib import import_module

from forms import PagePersoForm

# Vue d'affichage du formulaire d'indexation de la pageperso
@login_required
def pageperso(request):
    result = PagePerso.objects.filter(login=str(request.user))
    if request.method == "POST":
        if result.exists():
            form = PagePersoForm(request.POST, instance=result[0])
        else:
            form = PagePersoForm(request.POST)
        if form.is_valid():
            if result.exists():
                form.save()
            else:
                new_page = form.save(commit=False)
                new_page.login = str(request.user)
                new_page.save()
            labelperso = u"Modifier l'affichage de ma page perso"
            deref = True
        else:
            labelperso = u"Référencer ma page perso"
            deref = False

    # Si c'est pas un post, on renvoie le formulaire prérempli (si deja une page)
    else:
        if result.exists():
            res = result[0]
            initial_data = {
                'nom_site': res.nom_site,
                'slogan': res.slogan,
            }
            form = PagePersoForm(instance=result[0])
            labelperso = u"Modifier l'affichage de ma page perso"
            deref = True
        # Si la page existe pas, le champ est vierge
        else:
            form = PagePersoForm()
            labelperso = u"Référencer ma page perso"
            deref = False
    return django.shortcuts.render_to_response("pageperso/affichage.html", locals(), context_instance=RequestContext(request))

@login_required
def deref(request):
    result = PagePerso.objects.filter(login=str(request.user))
    result.delete()
    return django.shortcuts.redirect("/pageperso/")
