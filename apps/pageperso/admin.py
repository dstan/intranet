#~*~ coding: utf-8 ~*~
"""Module d'admin basique pour les pages persos.
"""
from django.contrib import admin
from django import forms
import models

class PagePersoAdmin(admin.ModelAdmin):
    """On a juste besoin de voir les données"""
    list_display = ('login', 'nom_site', 'slogan', 'logo')

admin.site.register(models.PagePerso, PagePersoAdmin)

