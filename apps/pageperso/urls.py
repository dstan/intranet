# -*- coding: utf-8 -*-
from django.conf.urls import patterns as patterns
from django.conf.urls import url

import views

urlpatterns = patterns('',
    url('^$', views.pageperso, name='pageperso'),
    url('deref', views.deref, name='deref')
)

