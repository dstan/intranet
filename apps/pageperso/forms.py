#  -*- coding: utf-8 -*-
"""Formulaire pour les adhérents, permettant d'ajouter
des infos sur sa page perso."""

from django import forms
from models import PagePerso

class PagePersoForm(forms.ModelForm):
    """ModelForm proposant un formulaire basé sur la classe
    PagePerso. C'est du vieux django, faudra mettre à jour."""
    class Meta:
        model = PagePerso
        fields = ['nom_site', 'slogan', 'logo']

