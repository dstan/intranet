from django import template
from django.core.urlresolvers import reverse
import settings

register = template.Library()

@register.tag
def get_page_url(parser, token):
    myvars = token.split_contents()

    if len(myvars) not in [2, 3]:
        raise template.TemplateSyntaxError, "%r tag requires one or two arguments" % token.contents.split()[0]

    return FormatPageUrl(myvars)

class FormatPageUrl(template.Node):
    def __init__(self, myvars):
        self.argument = None
        if len(myvars) == 3:
            self.argument = template.Variable(myvars[2])
        self.root = template.Variable(myvars[1])

    def render(self, context):
        try:
            if self.argument:
                argument = self.argument.resolve(context)
            else:
                argument = None
            # root = nom de la vue
            root = self.root.resolve(context)
            # on met devant le nom de l'appli pour que les URL reverse marchent
            root = settings.APP_PRISES_NAME + ":" + root
            if argument:
                return reverse(root, args = [argument])
            else:
                return reverse(root)
        except template.VariableDoesNotExist:
            return ''

        
