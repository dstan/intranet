from django.conf.urls import patterns, url

import views

urlpatterns = patterns('',
    #url('^$', views.redirect_to_view, name="root"),
    url('^$', views.prise_autorise, name="prise_autorise"),
    url('^(?P<pk>[0-9]+)/del$', views.prise_autorise_del, name="prise_autorise_del"),
    url('^view/(.+)?$', views.view, name="view"),
    url('^validate/(.+)?$', views.validate, name="validate"),
)
