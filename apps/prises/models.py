# -*- coding: utf-8 -*-
#
# MODELS.PY -- Modèles de base de données Django pour les prises
#
# Copyright (C) 2010 Nicolas Dandrimont
# Authors: Nicolas Dandrimont <olasd@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
import settings
import reversion

MAIL_SRC = settings.CABLAGE_MAIL_FROM
MAIL_DEST = settings.CABLAGE_MAIL_DEST

NEW_CABLAGE_TEMPLATE = u"""\
%(user)s vient d'enregistrer %(num_new)s nouveau%(x_new)s cablage%(s_new)s
%(new_str)s
"""

VAL_CABLAGE_TEMPLATE = u"""\
%(user)s vient de valider %(num_val)s nouveau%(x_val)s cablage%(s_val)s
%(val_str)s
"""

DONE_CABLAGE_TEMPLATE = u"""\
%(user)s vient de désenregistrer %(num_done)s câblage%(s_done)s.
"""

TODO_CABLAGE_TEMPLATE = u"""\
---
Câblage%(s_todo)s toujours à effectuer :
%(todo_str)s
"""

SIGNATURE_TEMPLATE = """\

--
L'intranet du Cr@ns
%(site)s%(url)s
"""

CUSTOM_HEADERS = {
    "X-Crans-Intranet": "cablage",
    }


class Prise(models.Model):

    class Meta:
        ordering = ["batiment", "chambre"]
        permissions = (
            ("can_view", "Peut visualiser les prises"),
            ("can_change", "Peut modifier les prises"),
            ("can_validate", "Peut valider les câblages"),
            )

    batiment = models.CharField(max_length=1)
    chambre = models.CharField(max_length=4)
    prise_crans = models.IntegerField(blank=True, null=True)
    prise_crous = models.IntegerField(blank=True, null=True)
    crans = models.BooleanField(default=True)
    crous = models.BooleanField(default=False)
    commentaire = models.CharField(max_length=1024, blank=True, null=True)
    cablage_effectue = models.BooleanField(default=True)

    def toggle(self):
        """Passe une prise du Cr@ns au CROUS ou vice-versa..."""
        self.crans = not self.crans
        self.crous = not self.crous
        self.cablage_effectue = not self.cablage_effectue

    def chambre_str(self):
        """Renvoie la chambre correspondant à la prise comme une
        chaîne prête à l'emploi"""
        return u"%s%s" % (self.batiment.upper(), self.chambre)

    @classmethod
    def send_new_cablage_email(cls, instances, user):
        """Envoie un e-mail concernant les câblages effectués à
        l'email de notification"""

        if not instances:
            return

        batiment = instances[0].batiment.upper()
        userstr = u"%s %s <%s>" % (user.first_name, user.last_name, user.email)
        todo = [instance for instance in cls.objects.filter(cablage_effectue = False).all() if instance not in instances]

        num_new = len([instance for instance in instances if not instance.cablage_effectue])
        num_done = len([instance for instance in instances if instance.cablage_effectue])
        num_todo = len(todo)

        new_str = u", ".join(sorted(instance.chambre_str()
                                    for instance in instances
                                    if not instance.cablage_effectue))
        todo_str = u", ".join(sorted(instance.chambre_str()
                                     for instance in todo))

        dic = {
            "x_new": u"x" if num_new > 1 else u"",
            "s_new": u"s" if num_new > 1 else u"",
            "x_done": u"x" if num_done > 1 else u"",
            "s_done": u"s" if num_done > 1 else u"",
            "s_todo": u"s" if num_todo > 1 else u"",
            "batiment": batiment,
            "num_new": num_new,
            "num_done": num_done,
            "num_todo": num_todo,
            "user": userstr,
            "new_str": new_str,
            "todo_str": todo_str,
            "url": reverse(settings.APP_PRISES_NAME + ":validate", args=[batiment])[1:],
            "site": settings.ROOT_URL,
            }

        TEMPLATE = []
        if num_new:
            TEMPLATE.append(NEW_CABLAGE_TEMPLATE)
        if num_done:
            TEMPLATE.append(DONE_CABLAGE_TEMPLATE)
        if num_todo:
            TEMPLATE.append(TODO_CABLAGE_TEMPLATE)

        if TEMPLATE:
            TEMPLATE.append(SIGNATURE_TEMPLATE)
            message = EmailMessage(u"[Câblage] Bâtiment %(batiment)s" % dic,
                                   u"".join(TEMPLATE) % dic,
                                   MAIL_SRC,
                                   MAIL_DEST,
                                   headers=CUSTOM_HEADERS)
            message.send()


    @classmethod
    def send_new_validate_email(cls, instances, user):
        """Envoie un email concernant la validation des câblages à
        l'adresse de contact"""
        if not instances:
            return

        batiment = instances[0].batiment.upper()
        userstr = u"%s %s <%s>" % (user.first_name, user.last_name, user.email)
        todo = cls.objects.filter(cablage_effectue = False).all()

        num_todo = len(todo)
        num_val = len(instances)

        val_str = u", ".join(sorted(instance.chambre_str()
                                    for instance in instances))
        todo_str = u", ".join(sorted(instance.chambre_str()
                                     for instance in todo))

        dic = {
            "x_val": u"x" if num_val > 1 else u"",
            "s_val": u"s" if num_val > 1 else u"",
            "s_todo": u"s" if num_todo > 1 else u"",
            "batiment": batiment,
            "num_val": num_val,
            "num_todo": num_todo,
            "user": userstr,
            "val_str": val_str,
            "todo_str": todo_str,
            "url": reverse(settings.APP_PRISES_NAME + ":validate", args=[batiment])[1:],
            "site": settings.ROOT_URL,
            }

        TEMPLATE = []
        if num_val:
            TEMPLATE.append(VAL_CABLAGE_TEMPLATE)
        if num_todo:
            TEMPLATE.append(TODO_CABLAGE_TEMPLATE)

        if TEMPLATE:
            TEMPLATE.append(SIGNATURE_TEMPLATE)
            message = EmailMessage(u"[Câblage] Bâtiment %(batiment)s" % dic,
                                   u"".join(TEMPLATE) % dic,
                                   MAIL_SRC,
                                   MAIL_DEST,
                                   headers=CUSTOM_HEADERS)
            message.send()



    def __unicode__(self):
        return u"<Prise chambre=%s%s>" % (
            self.batiment.upper(),
            self.chambre,
            )


reversion.register(Prise)

class PriseAutorise(models.Model):
    class Meta:
        unique_together = (('prise', 'aid'),)

    #FIXME
    Prise.__unicode__ = Prise.chambre_str

    prise = models.ForeignKey(Prise)
    aid = models.IntegerField(blank=False, null=False)
    commentaire = models.CharField(max_length=1024, blank=True, null=True)

    def __unicode__(self):
        return self.prise.chambre_str()
