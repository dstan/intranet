from django.contrib import admin
from apps.prises.models import Prise
from reversion.admin import VersionAdmin

class PriseAdmin(VersionAdmin):
    list_display = ('__unicode__', 'crans', 'prise_crans', 'crous', 'prise_crous', 'commentaire')
    list_display_links = ('__unicode__',)
    list_editable = ('commentaire',)
    list_filter = ('batiment', 'cablage_effectue', 'crans', 'crous')

admin.site.register(Prise, PriseAdmin)
