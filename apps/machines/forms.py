# -*- coding: utf8 -*-
from django import forms
from django.core.validators import RegexValidator

machine_name_validator = RegexValidator(
    regex='^([a-zA-Z0-9\-])+$',
    message="Votre nom de machine n'est pas valide",
    )

mac_validator = RegexValidator(
    regex='^(<automatique>|([0-9A-Fa-f]{2}[:-]?){5}([0-9A-Fa-f]{2}))$',
    message="L'adresse mac n'est pas valide",
)

class MachineForm(forms.Form):
    hostname = forms.CharField(label='Nom', max_length=100, validators=[machine_name_validator])
    mac = forms.CharField(label='Adresse mac', max_length=17, validators=[mac_validator])
    dns_ipv6 = forms.BooleanField(label='DNS IPv6', required=False)
    ssh_fpr = forms.CharField(label='Clé publique SSH', required=False, widget=forms.Textarea)

