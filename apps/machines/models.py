# -*- coding: utf-8 -*

from django.db import models
import lc_ldap.crans_utils

from django.utils.importlib import import_module
from django.db.models import Q
conn_pool = import_module('conn_pool', 'intranet')

import settings

import datetime
import netaddr

import sys

# Create your models here.
get_user = conn_pool.get_user

get_machine = conn_pool.get_machine
# odlyd
def trigger_generate(server):
    # TODO: intégrer ceci dans generate
    if not settings.BASE_LDAP_TEST:
        import gestion.gen_confs.generate as generate
        generate.trigger(server, background=True)

def create_machine(user, type, mac, host, club=None):
    """Crée une machine pour l'utilisateur ldap `user`, où `type` est
    "wifi-adh", "wifi-adh-v6", "fil" etc.
    Et `club` est un éventuel cid (int).
    """
    ldif = {
        'macAddress': [u'%s' % mac.encode('utf-8')],
        'host': [u'%s' % host.encode('utf-8')]
    }
    if club:
        luser = conn_pool.get_conn(user).search(u'cid=%s' % club)[0]
    else:
        luser = get_user(user)
    with conn_pool.get_conn(user).newMachine(luser.dn, type, ldif, user.username) as machine:
        if type in ["wifi-adh", "wifi-adh-v6"]:
            machine['ipsec']=u'auto'
        machine.create(login=user.username)
    conn_pool.get_user(user, refresh=True)
    return machine

def delete_machine(user, mid):
    machine = get_machine(user, mid, mode='rw')
    if machine:
        with machine:
            if user.username:
                machine.delete(login=user.username)
                conn_pool.get_user(user, refresh=True)
            else:
                machine.delete(login=u'Anonymous')


def exists_host(user, host):
	machine = conn_pool.get_conn(user).search(u'host=%s' % host)
	if machine:
		return True
	else:
		return False

def exists_mac(user, mac):
    if mac == u'<automatique>':
        return False
    machine = conn_pool.get_conn(user).search(u'macAddress=%s' % mac)
    if machine:
        return True
    else:
        return False

def can_create_machine(user):
    return user.groups.filter(name='crans_paiement_ok')

def can_create_machine_fil(user):
    if user.groups.filter(
     Q(name='crans_multimachines')
     | Q(name='crans_apprenti')
     | Q(name='crans_bureau')
     | Q(name='crans_cableur')
     | Q(name='crans_imprimeur')
     | Q(name='crans_nounou')
    ): return can_create_machine(user)
    else: return can_create_machine(user) and len([ m for m in get_user(user).machines() if str(m['objectClass'][0]) == 'machineFixe' ]) < 1

class Machine(dict):

    def __init__(self, user, mid=None, machine=None, mode='ro'):
        self.user = user
        self.luser = get_user(user)
        self.mode = mode
        if not machine and not mid:
            self.machine = None
        elif not machine and mid:
            self.machine = get_machine(user, mid, mode=mode)
        else:
            self.machine = machine
        if self.machine:
            self.mode = self.machine.mode
            if self.machine.proprio().dn != self.luser.dn and not self.machine.proprio().dn in [ cl.dn for cl in self.luser.clubs()]:
                raise ValueError("Aucun droit sur la machine %s" % self.machine['mid'][0])
            self.update(self._get_machine_info())

    def delete(self, attr, index):
        with self.machine:
            if index is None:
                raise ValueError("Index is None !!!")
                self.machine[attr] = []
                self[attr]=[]
            else:
                del(self.machine[attr][index])
                del(self[attr][index])
            self.machine.validate_changes()
            self.machine.save()
        conn_pool.get_user(self.user, refresh=True)
        self.machine = get_machine(self.user, self.machine['mid'][0], mode=self.mode)

    def _get_machine_info(self):
        machine = self.machine
        luser = self.luser

        def bl_timestamp(bl):
            return {
                'debut': '-' if bl['debut'] == '-' else datetime.datetime.fromtimestamp(bl['debut']),
                'fin': '-' if bl['fin'] == '-' else datetime.datetime.fromtimestamp(bl['fin']),
                'type':bl['type']
            }
        def historique(h):
            time, h = str(h).split(',',1)
            login, h = h.split(':',1)
            return (time.strip(), login.strip(), h.strip())

        machine = {
            'mid' : str(machine['mid'][0]),
            'type': str(machine['objectClass'][0]),
            'mac': str(machine['macAddress'][0]),
            'ip': [ str(ip) for ip in machine.get('ipHostNumber', []) ],
            'ipv6': [ str(ip) for ip in machine['ip6HostNumber'] ],
            'blacklist':  [ bl_timestamp(bl.value) for bl in luser.get('blacklist', []) + machine.get('blacklist', []) ],
            'alias': [ str(alias) for alias in machine.get('hostAlias', []) ],
            'host': str(machine['host'][0]).split('.',1)[0],
            'ipsec': machine.has_key('ipsec') and str(machine['ipsec'][0]) or None,
            'wifi_user': machine.has_key('ipsec') and str(machine['host'][0]).split('.',1)[0] or None,
            'dnsipv6' : False if len(machine.get('dnsIpv6', []))>0 and not machine['dnsIpv6'][0].value else True,
            'historique' : [ historique(h) for h in machine['historique'] ],
            'sshFingerprint' : [ key for key in machine.get('sshFingerprint', [])]
        }

        return machine

    def set(self, **kw):
        try:
            if 'mac' in kw.keys():
                try:
                    kw['mac'] = lc_ldap.crans_utils.format_mac(kw['mac']).lower()
                except netaddr.AddrFormatError:
                    raise ValueError('Adresse MAC invalide.\nelle doit être de la forme XX:XX:XX:XX:XX:XX')
                if self.get('mac', '') != kw['mac'] and exists_mac(self.user, kw['mac']):
                    raise ValueError('Mac déjà utilisée sur le réseau.')
                else:
                    self['mac'] = kw['mac']
            if 'host' in kw.keys():
                if '.' in kw['host']:
                    raise ValueError('Nom d\'hote invalide %s' % self._format_host(kw['host']))
                if self.get('host', '') != kw['host'].split('.',1)[0] and exists_host(self.user, kw['host']):
                   raise ValueError('Nom de machine : nom déjà pris.')
                else:
                    self['host'] = kw['host'].split('.',1)[0]
            if 'dnsipv6' in kw.keys():
                self['dnsipv6'] = kw['dnsipv6']
        except ValueError:
            self.update(kw)
            raise

    def _format_host(self, host):
        host = host.lower()
        if not host.endswith('crans.org'):
            if self['type']=='machineFixe':
                host = "%s.crans.org" % host
            elif self['type']=='machineWifi':
                host = "%s.wifi.crans.org" % host
        return host

    def save(self):
        with self.machine:
            mac_changed = False
            if str(self.machine['host'][0]).split('.',1)[0] != self['host']:
                host = self._format_host(self['host'])
                self.machine.history_add(self.user.username, u"host (%s -> %s)" % (self.machine['host'][0], host))
                self.machine['host'] = host
                mac_changed = True
            if str(self.machine['macAddress'][0]) != self['mac']:
                self.machine.history_add(self.user.username, u"macAddress (%s -> %s)" % (self.machine['macAddress'][0], self['mac']))
                self.machine['macAddress'] = self['mac']
            if (len(self.machine.get('dnsIpv6', []))>0 and self.machine['dnsIpv6'][0].value != self['dnsipv6']) or (len(self.machine.get('dnsIpv6', [])) == 0 and not self['dnsipv6']):
                self.machine.history_add(self.user.username, u"dnsIpv6 (%s -> %s)" % (not self['dnsipv6'], self['dnsipv6']))
                self.machine['dnsIpv6'] = unicode(self['dnsipv6'])
            self.machine.validate_changes()
            self.machine.save()
            if mac_changed:
                trigger_generate('odlyd')
        conn_pool.get_user(self.user, refresh=True)
        self.machine = get_machine(self.user, self.machine['mid'][0], mode=self.mode)

    def create_init(self, type):
        if type == 'fil':
            self['type']='machineFixe'
            if self.luser['etudes'][0] == u'Personnel ENS':
                self['realm']='personnel-ens'
            else:
                self['realm']='adherents'
        elif type == 'wifi':
            self['type']='machineWifi'
            self['realm']='wifi-adh'
        else:
            raise ValueError('type %s inconnu' % type)

    def create(self, cid=None):
        if self.machine:
            raise ValueError("La machine existe déjà")
        self.machine = create_machine(self.user, self['realm'], self['mac'], self._format_host(self['host']), club=cid if cid else None)
        self.update(self._get_machine_info())
        trigger_generate('odlyd')
