# -*- coding: utf-8 -*

from django.shortcuts import render,  redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from django.template import RequestContext

from django.contrib import messages

from lc_ldap.ldap_locks import LockError
from django.contrib import messages
from django.utils.importlib import import_module
conn_pool = import_module('conn_pool', 'intranet')

import models
import hashlib
import time
import settings

from forms import MachineForm

def unicode_of_Error(x):
    """Formatte l'exception de type ValueError"""
    # TODO templatetag ?
    return u"\n".join(unicode(i, 'utf-8') if type(i) == str
            else repr(i) for i in x.args)

def keep(request, mid, hash, action, valid_until):
    """Confirmer la suppression ou création de machine"""
    if hashlib.sha256(settings.SECRET_KEY + mid + valid_until).hexdigest() != hash or int(valid_until, 16) < time.time():
        return render(request, 'machines/confirm_keep.html', {'message':'Lien invalide.'})
    if action == 'drop':
        models.delete_machine(request.user, mid)
    with open('/var/www/to_keep', 'a') as f:
        f.write('%s: %s\n' % (mid, action))
    return render(request, 'machines/confirm_keep.html', {'message':'Votre demande a bien été prise en compte, merci.'})

@login_required
def index(request):
    clubs = []
    for cl in models.get_user(request.user).clubs():
        machines = []
        for machine in cl.machines():
            # TODO template tags !
            machines.append({
                'type' : str(machine['objectClass'][0]),
                'host' : str(machine['host'][0]).split('.',1)[0],
                'mid' : str(machine['mid'][0]),
            })

        clubs.append({'nom' : str(cl['nom'][0]), 'machines' : machines, 'cid' : str(cl['cid'][0]) })

    machines = []
    for machine in models.get_user(request.user).machines():
        # TODO templatetags. Typiquement le type de machine est mauche
        machines.append({
            'type' : str(machine['objectClass'][0]),
            'host' : str(machine['host'][0]).split('.',1)[0],
            'mid' : str(machine['mid'][0]),
        })

    return render(request, 'machines/index.html', {'clubs' : clubs, 'machines' : machines, 'can_create_machine_fil' : models.can_create_machine_fil(request.user)})


@login_required
def detail(request, mid=None):
    if request.method == "GET":
        try:
            machine = models.Machine(user=request.user, mid=mid)
            if not machine:
                return redirect('machines:index')
            return render(request, 'machines/detail.html', {'machine' : machine})
        except (ValueError, EnvironmentError, LockError) as error:
            messages.error(request, 'Erreur : %s' % unicode_of_Error(error))
            return redirect("machines:index")
    else:
        print request.POST

@login_required
def advanced(request, mid=None):
    if request.method == "GET":
        machine = models.Machine(user=request.user, mid=mid)
        if not machine:
            return redirect('machines:index')

        return render(request, 'machines/advanced.html', {'machine' : machine})
    else:
        print request.POST

@login_required
def ssh_keys(request, mid=None, delete=None):
    machine = models.Machine(user=request.user, mid=mid, mode= 'rw' if delete or request.method == "POST" else 'ro')
    if not machine:
        return redirect('machines:index')
    try:
        if request.method == "GET":
            if delete:
                machine.delete('sshFingerprint', int(delete))
                return redirect('machines:ssh_fprs', mid=mid)

            return render(request, 'machines/ssh_fprs.html', {'machine' : machine})
        else:
            machine.machine['sshFingerprint'].append(request.POST['ssh_fpr'])
            machine.machine.validate_changes()
            machine.machine.save()
            conn_pool.get_user(request.user, refresh=True)
            return redirect('machines:ssh_fprs', mid=mid)
    except (ValueError, EnvironmentError, LockError) as error:
        return render(request, 'machines/ssh_fprs.html', {
            'machine' : machine,
            'ssh_fpr' : request.POST['ssh_fpr'],
            'error' : 'Erreur : %s' % unicode_of_Error(error)})

        print request.POST

@login_required
def delete(request, mid):
    try:
        machine = models.Machine(user=request.user, mid=mid)
    except (ValueError, EnvironmentError, LockError) as error:
        messages.error(request, 'Erreur : %s' % unicode_of_Error(error))
        return redirect("machines:index")
    if not machine:
        messages.error(request, u"Cette machine n'existe pas")
        return redirect("machines:index")

    if request.method == 'POST':
        try:
            models.delete_machine(request.user, mid)
            messages.success(request, u'La machine a bien été supprimée')
            return redirect("machines:index")
        except (ValueError, EnvironmentError, LockError) as error:
            return render(request, 'machines/detail.html', {
                'machine' : machine,
                'error' : 'Erreur : %s' % unicode_of_Error(error)})

    return render(request, "confirm.html", {
        'confirm_title': u'Suppression de %s' % machine.machine['host'][0],
        'confirm_message': u'Êtes-vous sûr de vouloir effacer cette machine ?',
        'confirm_cancel_url': reverse('machines:detail', args=[mid]),
    })

@login_required
def add(request, mtype=None, cid=None, mac=None):
    # TODO factoriser !
    if not models.can_create_machine(request.user):
        messages.error(request, 'Vous ne pouvez pas ajouter de machine !')
        return redirect('machines:index')
    if cid and not cid in [str(cl['cid'][0]) for cl in models.get_user(request.user).clubs() ]:
        messages.error(request, 'Vous ne pouvez pas ajouter de machine pour ce club !')
        return redirect('machines:index')
    if mtype == 'fil' and not cid and not models.can_create_machine_fil(request.user):
        messages.error(request, 'Vous avez atteint votre nombre limite de machines filaires !')
        return redirect('machines:index')

    if request.POST:
        form = MachineForm(request.POST)
        machine = models.Machine(request.user)
        machine.create_init(mtype)

        if form.is_valid():
            try:
                machine.set(host=form.cleaned_data['hostname'], mac=form.cleaned_data['mac'])
                machine.create(cid=cid)
                messages.success(request, u'Machine correctement ajoutée.')
                return redirect('machines:detail', mid=str(machine['mid']))
            except (ValueError, EnvironmentError, LockError) as error:
                messages.error(request, unicode_of_Error(error))
    else:
        if mac is not None:
            mac_initial = mac
        else:
            mac_initial = u'<automatique>'
        initial = {'mac': mac_initial}
        form = MachineForm(initial=initial)
    return render(request, 'machines/add_edit.html', {'form' : form})

@login_required
def edit(request, mid=None):
    # TODO factoriser !
    try:
        machine = models.Machine(user=request.user, mid=mid, mode='rw')
    except ValueError:
        machine = None
    if not machine:
        messages.error(request, 'Cette machine ne vous appartient pas !')
        return redirect('machines:index')

    if request.POST:
        form = MachineForm(request.POST)
        if form.is_valid():
            try:
                machine.set(
                    host=form.cleaned_data['hostname'],
                    mac=form.cleaned_data['mac'],
                    dnsipv6=form.cleaned_data['dns_ipv6']
                )
                ssh_fpr = form.cleaned_data['ssh_fpr']
                if ssh_fpr:
                    machine['sshFingerprint'].append(ssh_fpr)
                    machine.machine.validate_changes()
                    machine.machine.save()
                machine.save()
                messages.success(request, u'Les changements ont bien été enregistrés.')
                return redirect('machines:detail', mid=str(machine['mid']))
            except (ValueError, EnvironmentError, LockError) as error:
                messages.error(request, unicode_of_Error(error))
    else:
        initial = {
            'hostname': machine['host'],
            'mac': machine['mac'],
            'dns_ipv6': machine['dnsipv6'],
        }
        form = MachineForm(initial=initial)
    return render(request, 'machines/add_edit.html', {'form' : form, 'edit': True})

