# -*- coding: utf-8 -*-
from django.conf.urls import url

import views

urlpatterns = [
    url('^$', views.index, name='index'),
    url('^(?P<mid>[0-9]+)/(?P<action>keep|drop)/(?P<hash>.+)/(?P<valid_until>[0-9A-Fa-f]+)/$', views.keep, name='keep'),
    url('^(?P<mid>[0-9]+)/$', views.detail, name='detail'),
    url('^(?P<mid>[0-9]+)/edit/$', views.edit, name='edit'),
    url('^(?P<mid>[0-9]+)/advanced/$', views.advanced, name='advanced'),
    url('^(?P<mid>[0-9]+)/advanced/ssh_keys/$', views.ssh_keys, name='ssh_keys'),
    url('^(?P<mid>[0-9]+)/advanced/ssh_keys/delete/(?P<delete>[0-9]+)$', views.ssh_keys, name='ssh_keys_delete'),
    url('^(?P<mid>[0-9]+)/delete/$', views.delete, name='delete'),
    url('^add/(?P<mtype>fil|wifi)/$', views.add, name='add'),
    url('^add/(?P<mtype>fil|wifi)/(?P<mac>([0-9a-f]{2}:){5}[0-9a-f]{2})/$', views.add, name='add'),
    url('^add/(?P<mtype>fil|wifi)/(?P<cid>[0-9]+)/$', views.add, name='add'),
]
