/* WifiMap Crans !
 *  Copyright (C) 2012 Daniel STAN
 *  Authors: Daniel STAN <daniel.stan@crans.org>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//var debug = document.location.hostname != 'intranet2.crans.org';
var debug = false;

var WifiMap;

//Initialiser la map à la fin du chargement (pour trouver <div id="map">)
window.addEventListener('load',function () { WifiMap.init(); });

//Wifi map !
WifiMap = {
    'moved': null,
    'init': function() {
        var that = this;
        this.map = new OpenLayers.Map({'div': 'map','controls':[]});
        this.moved = null;
        // Note: Min lon: 2.325175
        //     : Min lat: 48.786424
        //     : Min lon: 2.33118
        //     : Max lat: 48.791113
// La suite est à décommenter si pas d'accès au xml static.
//        if( debug ) {
            // La carte est à ajuster avec ses réglages (mais j'ai pas envie de
            // le faire proprement, là
//            var dy = 0.00055; //Petit ajustement pour mes yeux
//            var dx = -0.0006;
//            this.layer = new OpenLayers.Layer.Image("Campus de Cachan",
                '/static/test/campusmap.png',
//                new OpenLayers.Bounds(-180, -88.759, 180, 88.759),
//                new OpenLayers.Bounds( 2.325175+dx, 48.786424+dy, 2.33118+dx, 48.791113+dy),
//                new OpenLayers.Size(905, 804),
//                {transitionEffect: 'resize', numZoomLevels: 9});
//        }
//        else
            this.layer = new OpenLayers.Layer.OSM("Campus de Cachan",
              // Official OSM tileset as protocol-independent URLs
              [
                  '//a.tile.openstreetmap.org/${z}/${x}/${y}.png',
                  '//b.tile.openstreetmap.org/${z}/${x}/${y}.png',
                  '//c.tile.openstreetmap.org/${z}/${x}/${y}.png'
              ],
                {transitionEffect: 'resize'});
        //connexions = new OpenLayers.Layer.Vector("Interconnexions");

        //on/off: comme on peut s'y attendre
        //autres: pour afficher des bornes pas encore existantes (todo)
        //prev: prévisualisation des nouvelles bornes pas installées
        //hl: pour les bornes en cours de sélection
        this.bornesLayers = {//'ens': new OpenLayers.Layer.Markers("Mixte Ens-Cr@ns"),
          'off': new OpenLayers.Layer.Markers("En panne"),
          'on': new OpenLayers.Layer.Markers("Fonctionnelle"),
          'prev': new OpenLayers.Layer.Markers("Nouvelle couverture"),
          'pont wifi': new OpenLayers.Layer.Markers("Pont Wifi"),
          'hl': new OpenLayers.Layer.Markers("Remarquables")
          };

        //Noms des fichiers images suivant le mode de la borne
        var files = {'on': 'marker-green', 'off': 'marker',
            'prev': 'marker-blue', 'pont wifi': 'marker-gold'};
        for( var type in files)
            this.bornesLayers[type].markerUrl = '/static/OpenLayers/img/' +
                                                (files[type]) + '.png';

        //Layer de base (OSM actuellement)
        this.map.addLayers([this.layer]);
        //Controles divers (ceux ci-dessous sont visibles)
        this.map.addControl(new WifiMap.ModeSwitcher({'wifiMap': this}));
        this.map.addControl(new OpenLayers.Control.Navigation());
        this.map.addControl(new OpenLayers.Control.PanZoom());

        for( layer in this.bornesLayers ) this.map.addLayer(this.bornesLayers[layer]);

        //Projections utilisées, pour changement de coordonnées
        //Placer ces lignes après l'ajout du layer principal à la map (sic)
        this.projections = {
            'gps': new OpenLayers.Projection("EPSG:4326"),
            'map': this.map.getProjectionObject()
        };

        //Initilisation des vues
        this.recenter();
        this.get_all();

        //Le bouton "globe" (et assimilé) recentre sur le campus, et non sur
        //la Terre entière
        this.map.zoomToMaxExtent = this.recenter.bind(this);
        //En fin de zoom, on redimensionne les markers des bornes
        //(un marker a une taille fixe indépendante du zoom, par défaut)
        this.map.events.register('zoomend',this, function () {
            var up = [];
            for( host in that.listeBornes) {
                up.push(host);
                that.listeBornes[host].update_marker_size();
            }
        });

        WifiMap.map.events.register("click",this ,function (e) {
            var pos = this.map.getLonLatFromViewPortPx(this.map.events.getMousePosition(e));
            if( this.moved )
                this.moved.moveTo(pos);
            this.moved = null;
        });

        //Event select: tout déselectionner
        this.map.events.register('click', undefined, this.highlight.bind(this,undefined));
    },

    //Recentre sur le campus avec un zoom judicieux
    'recenter': function() {
        this.map.setCenter(new OpenLayers.LonLat(2.3281524662736,48.789449383327).
            transform(this.projections.gps,this.projections.map),17);
    },

    //Envoie une requête de récupération de toutes les bornes
    'get_all': function () {
        var that = this;
        var req = new XMLHttpRequest();
        if( debug )
            var url = "/static/test/status.xml";
        else
            var url = "/wifimap/get_xml";
        req.open("GET", url, true);
        req.onreadystatechange = function () {
            if( this.readyState != 4) return;
            if( this.status != 200 ) {
                if( this.status == 0)
                    return; //0 signifie que l'on quitte la page en cours
                alert("Impossible de charger la carte ! Erreur: "+this.status);
                return;
            }
            that.show_bornes(this.responseXML);
        }
        req.send(null);
    },

    //Crée un nouvel objet borne (factory), en prenant en entrée son hostname
    'make_borne': function (hostname) {
        var borne = {'hostname':hostname};
        var that = this;

        borne.wifiMap = this;

        borne.update_info = function(xml) {
            //Clean up last marker
            this.xml = xml;
            var old = this.marker;
            this.xml = xml;
            var highlighted = this.highlighted;

            var type = "on";
            if( xml.hasAttribute('offline'))
                type = "off";
            if( xml.hasAttribute('virtual'))
                type = "prev";
            for( var child=xml.firstChild; child; child=child.nextSibling) {
                if( child.tagName == 'pontwifi')
                    if( getInnerText(child) == 'pontwifi')
                        type = "pont wifi";
            }
            this.layer = that.bornesLayers[type];   //On définit layer même
            //S'il n'est pas sûr qu'on placera la borne sur la carte
            if(old) {
                this.highlight(false);
                this.layer.removeMarker(old);
                old.destroy(); this.marker = null;
            }
            //New coords ?
            if( !this.mapPosition || !this.mapPosition.modified) {
                try {
                    var x = parseFloat(xml.getElementsByTagName('lon')[0].firstChild.nodeValue);
                    var y = parseFloat(xml.getElementsByTagName('lat')[0].firstChild.nodeValue);
                }
                catch(e) {
                    this.mapPosition = undefined;
                    //Mettre à jour les infos tabulaires de la borne avant de quitter
                    this.update_borne_infos(xml);
                    return;
                }

                this.mapPosition = new OpenLayers.LonLat(x,y);
                this.mapPosition.transform(that.projections.gps,that.projections.map);
            }

            //On va compter le nombre de client et en déduire la taille
            var counts = xml.getElementsByTagName('client_count');
            var ratio=0;   //Taille d'une borne éteinte
            for(var i=0; i < counts.length; i++) {
                ratio += parseFloat(counts[i].firstChild.nodeValue)/50;
                if(counts[i] == xml) break;
            }
            ratio = (1+ratio)*5/8;
            this.ratio = ratio;

            var size = new OpenLayers.Size(21*ratio,25*ratio); //Size here does not matter
            var offset = new OpenLayers.Pixel(-size.w/2,-size.h); //see update_marker_size
            var url = this.layer.markerUrl;
            var icon = new OpenLayers.Icon(url,size,offset);

            this.marker = new OpenLayers.Marker(this.mapPosition, icon);

            this.layer.addMarker(this.marker);
            this.update_marker_size();

            //Event select en cas de click
            this.marker.events.register("click",this,function (e) {
                that.highlight(this.hostname);
            });

            //Mettre à jour les données tabulaires
            this.update_borne_infos(xml);

            //Remise en place de certains trucs oubliés
            if( highlighted)
                borne.highlight(true);
        };

        //Est-ce que cette borne est sélectionnée:
        borne.highlighted = false;

        //Sélectionner une borne (ou pas)
        borne.highlight = function(on) {
            var ratio = 2;
            if( this.highlighted == on) return; //Nothing to do
            this.highlighted = on;
            if( !borne.marker ) return; //Nothing to do
            if( on ) {
                this.layer.removeMarker(this.marker);
                that.bornesLayers['hl'].addMarker(this.marker);
            }
            else {
                that.bornesLayers['hl'].removeMarker(this.marker);
                this.layer.addMarker(this.marker);
            }
            this.update_marker_size();
        };

        borne.moveTo = function (mapPos) {
            this.mapPosition = mapPos.clone();
            var gps = mapPos.clone();
            gps.transform(that.projections.map, that.projections.gps);
            console.debug("Move to " + gps.lon + "; " + gps.lat );
            this.mapPosition.modified = true;
            this.update_info(this.xml);
        };

        //Met à jour la taille du markeur (notamment après un zoom)
        borne.update_marker_size = function() {
            if( ! this.marker ) return; //Nothing to do
            if( debug )
                var ratio = 0.5;
            else
                var ratio = this.ratio * Math.pow(2,that.map.getZoom()-18);
            if( this.highlighted ) ratio *= 2;
            this.marker.icon.size.w = 21*ratio;
            this.marker.icon.size.h = 25*ratio;
            this.marker.icon.offset.x = -this.marker.icon.size.w/2;
            this.marker.icon.offset.y = -this.marker.icon.size.h;

            //Si selectionnée, petit effet de transparence
            this.marker.icon.setOpacity(this.highlighted ? 0.7 : 1)
            //Et permet de cliquer "au travers" de la borne en cours
            this.marker.icon.imageDiv.style.pointerEvents = this.highlighted ? 'none': '';
            this.marker.icon.draw();
        };

        //Met à jour les données tabulaires (bref, tout ce qui n'est pas sur la carte,
        //mais à côté
        borne.update_borne_infos = function (xml) {
            var template = $('borne_template');
            var html = template.cloneNode(true);

            //Toujours utile de pouvoir se référer à la borne
            html.borne = this;
            html.removeAttribute('id'); //On évite de duppliquer un id
            html.className = html.className.replace(/ *template ?/,'');

            html.className += ' ' + (xml.hasAttribute('offline')?'non-':'') + 'connected';
            this.offline = xml.hasAttribute('offline');

            var titleTag = getElementsByClassName(html,'hostname')[0];
            setInnerText(titleTag,hostname);
            titleTag.name = hostname;

            //Lieu de la borne tel que décrit dans la base
            this.location = xml.getElementsByTagName('location');
            this.location = this.location.length > 0?getInnerText(this.location[0]):undefined;

            //Et si lieu, autant l'afficher
            if( this.location )
                templates.fillSpanClass(html,'location',this.location);

            //Ajout d'un commentaire standard (info mais peut-être aussi d'autre choses)
            function addComment(txt) {
                templates.fillSpan(templates.getInstance(html,'comment'), txt);
            }

            //Parcourir, l'ensemble des nœuds, et voir au cas par cas
            for( var child=xml.firstChild; child; child=child.nextSibling) {
                //Ignorer le texte pourri
                if( child.nodeType != child.ELEMENT_NODE) continue;
                if( child.tagName == 'comment')
                    addComment(getInnerText(child));
                else if( child.tagName == 'uptime')
                    templates.fillSpanClass(html,'uptime',
                        format_uptime(parseInt(getInnerText(child))));
                else if( child.tagName == 'client_count') {
                    this.count = parseInt(getInnerText(child));
                    var c = parseInt(getInnerText(child));
                    //templates.fillSpanClass(html,'count', "(" + c +" client" + (c>1?'s)':')'));
                    templates.fillSpanClass(html,'count', "(" + c + ')');
                }
                else if( child.tagName == 'prise') {
                    templates.fillSpanClass(html,'prise',getInnerText(child));
                }
                else if( child.tagName == 'network') {
                    network = templates.getInstance(html,'network');

                    templates.fillSpanClass(network,'ssid',
                        getInnerText(child.getElementsByTagName('ssid')[0]));
                    templates.fillSpanClass(network,'bssid',
                        getInnerText(child.getElementsByTagName('bssid')[0]));
                    templates.fillSpanClass(network,'channel',
                        getInnerText(child.getElementsByTagName('channel')[0]));

                    var clients = child.getElementsByTagName('client');
                    for( var i = 0; i < clients.length; i++) {
                        templates.fillSpan(templates.getInstance(network,'client'),
                            getInnerText(clients[i].getElementsByTagName('mac')[0]));
                    }
                }
            }

            //Si position sur la carte, on active le bouton pour zoomer dessus
            if(this.mapPosition) {
                var panTo = getElementsByClassName(html,'pan_to')[0];
                panTo.addEventListener('click',
                    (function (event) {
                        if(this.mapPosition) { 
                            this.wifiMap.map.panTo(this.mapPosition);
                            this.wifiMap.highlight(this.hostname);
                        }
                        event.preventDefault();
                    }).bind(this));
                templates.enable(panTo);
            }

            // Lien vers munin ou autre page de monitoring
            var mon_url = xml.getElementsByTagName('monitor');
            if( mon_url.length > 0) {
                var mon_a = getElementsByClassName(html, 'monitor')[0];
                mon_a.href = mon_url[0].getAttribute('href');
                templates.enable(mon_a);
            }

            if( xml.getElementsByTagName('editable').length > 0) {
                setCoord = getElementsByClassName(html,'setCoord')[0];
                setCoord.addEventListener('click',
                    (function (event) {
                        this.wifiMap.moved = this;
                        event.preventDefault();
                    }).bind(this));
                templates.enable(setCoord);
            }

            if( this.mapPosition) {
                var gps = this.mapPosition.clone();
                gps.transform(that.projections.map, that.projections.gps);
                templates.fillSpanClass(html, 'coords', gps.lon + ' ' + gps.lat)
                this.mapPosition.modified && getElementsByClassName(html, 'coords')[0].setAttribute('class','coords modified')
            }
            else
                templates.fillSpanClass(html, 'coords', 'N/A');

            getElementsByClassName(html, 'recordCoord')[0].addEventListener('click',
                (function (event) {
                    event.preventDefault();
                    var gps = this.mapPosition.clone();
                    gps.transform(that.projections.map, that.projections.gps);
                    //console.debug(gps.lon + ' ' + gps.lat + ' ' + this.hostname);
                    var req = new XMLHttpRequest();
                    req.open('GET', '/wifimap/update(' + this.hostname + ',' + gps.lon + ',' + gps.lat + ')', true);
                    var borne = this;
                    req.onreadystatechange = function() {
                        if( this.readyState != 4) return;
                        if( this.status != 200 ) {
                            if( this.status == 0)
                                return; //0 signifie que l'on quitte la page en cours
                            console.debug("La mise à jour a échouée");
                            return;
                        }
                        //Else: status == 200
                        borne.mapPosition=null;
                        borne.update_info(this.responseXML.documentElement);
                    }

                    req.send(null);
                    //that.show_bornes(xhr.responseXML);
                    //console.debug(xhr.responseText);
                    
/*
        this.update_date(doc);
        var bornes = doc.getElementsByTagName('borne')
        for( var i=0; i<bornes.length; i++) {
            var xml = bornes[i];
            var hostname = getInnerText(xml.getElementsByTagName('hostname')[0]);

            if( !this.listeBornes[hostname])
                this.listeBornes[hostname] = this.make_borne(hostname);
            this.listeBornes[hostname].update_info(xml);
        }
        this.sort();*/
                }).bind(this));

            //On fixe la nouvelle bar d'info
            if(this.html_list)
                this.html_list.parentNode.replaceChild(html,this.html_list);
            else //Tri par insertion
                this.place(template.parentNode, html);
            this.html_list = html;  //Référence borne -> objet html
            this.html_cont = template.parentNode;
        };

        borne.place = function(cont, html) {
            var insert = cont.firstChild;
            while( insert && !(insert.borne && this.wifiMap.cmp(insert.borne,this)))
                {
                insert = insert.nextSibling; }
            cont.insertBefore(html,insert);
        },

        //Renvoie un token afin d'aider au tri
        borne.token_location = function() {
            if( !this.location) return undefined;
            //On trie par bâtiment, puis étage. Les trucs qui ne matchent pas
            //doivent être triés à la fin
            var res = this.location.match(/^Au ([0-9])(.*)$/);
            return res == null?['z','z',this.location]:res.reverse();
        };

        borne.get_uptime = function() {
            if( !this.xml ) return 0;
            var tags = this.xml.getElementsByTagName('uptime');
            if( !tags.length ) return 0;
            else return parseInt(getInnerText(tags[0]));
        };

        return borne;
    },

    setOrder: function (main, way) {
        var i = 0;
        for( ; i < this.orders.length; i++)
            if( this.orders[i].name == main) break;
        if( way != 'down')
            this.cmp = function(a,b) { return this.orders[i].fct(b,a); }
        else
            this.cmp = this.orders[i].fct;
        this.sort();
    },

    sort: function() {
        for( var b in this.listeBornes )
            this.listeBornes[b].html_cont.removeChild(this.listeBornes[b].html_list);
        for( var b in this.listeBornes ) 
            this.listeBornes[b].place(this.listeBornes[b].html_cont,
                                      this.listeBornes[b].html_list);
    },
    
    'cmp': function (a,b) {
        return this.orders[0].fct(a,b);
    },

    'orders': [
            { label: 'Lieux', name: 'location', def: 'down',
              fct: function (a,b) { return a.token_location() > b.token_location(); }
            },
            {
              label: 'Nombre de clients', name: 'clients',
              fct: function (a,b) {
                if( a.count == undefined || a.offline) return true;
                if( b.count == undefined || b.offline) return false;
                return a.count < b.count;
              }
            },
            {
              label: 'Alphabétique', name: 'alphabeat',
              fct: function (a,b) { return a.hostname > b.hostname; }
            },
            {
              label: 'Uptime', name: 'uptime',
              fct: function (a, b) {
                return a.get_uptime() < b.get_uptime();
              }
            }
        ],
    
    //Liste de bornes (en fait, un dictionnaire)
    'listeBornes': {},

    //Affiche le xml de bornes passé en argument
    'show_bornes': function (doc) {
        this.update_date(doc);
        var bornes = doc.getElementsByTagName('borne')
        for( var i=0; i<bornes.length; i++) {
            var xml = bornes[i];
            var hostname = getInnerText(xml.getElementsByTagName('hostname')[0]);

            if( !this.listeBornes[hostname])
                this.listeBornes[hostname] = this.make_borne(hostname);
            this.listeBornes[hostname].update_info(xml);
        }
        this.sort();
    },

    //Met à jour la date de dernière màj
    'update_date': function (doc) {
        //On prend la màj la plus ancienne du tas
        var dates = doc.getElementsByTagName('currdate');
        var min = Infinity;
        for(var i=0; i < dates.length; i++)
            min = Math.min(min,parseInt(dates[i].firstChild.nodeValue))

        //Prochain chargement dans 5min + delta sec
        //5min étant l'intervalle du cron de màj
        var delta = 10; // ~= le temps que le script interroge toutes les bornes
        var wait = (5*60+delta+min)*1000 - (new Date()).getTime();
        if( wait > 0) //No refresh is already outdated (possible server problem)
            true || setTimeout(WifiMap.get_all.bind(WifiMap), wait);

        //Mise à jour régulière
        this.update_date.timer && clearInterval(this.update_date.timer);
        var last_update_field = $('last_update');
        this.update_date.timer = setInterval(function () {
            setInnerText(last_update_field, format_uptime(Math.floor((new Date()).getTime()/1000-min)));
        },1000);
    },

    //Borne selectionnée
    'highlighted': undefined,

    //Selectionner une borne
    'highlight': function (hostname) {
        if( this.highlighted ) {
            this.highlighted.highlight(false);
        }
        if( !hostname ) {
            this.highlighted = undefined; return;
        }
        this.highlighted = this.listeBornes[hostname];
        this.highlighted.highlight(true);
        
        this.slideTo(this.highlighted.html_list);
    },

    'slideTo': function(elem) {
        //Animation de slideover
        var scroll = elem.parentNode;
        var from = scroll.scrollTop;
        var to = elem.offsetTop - scroll.offsetTop;

        var i = 0, count = 10;
        function iter() {
            i++;
            scroll.scrollTop = (i*to + (count - i)*from)/count;
            if( i < count) setTimeout(iter,20);
        }
        setTimeout(iter,20);
    }
}


WifiMap.ModeSwitcher = OpenLayers.Class(OpenLayers.Control.LayerSwitcher,
{
    parentClass: OpenLayers.Control.LayerSwitcher.prototype,

    orderChoiceLbl: null,

    wifiMap: null,
    
    initialize: function (options) {
        this.parentClass.initialize.apply(this, options);
        this.wifiMap = options['wifiMap'];
    },

    loadContents: function() {
        this.parentClass.loadContents.apply(this);
        this.orderChoice = document.createElement('div');
        this.orderChoice.innerHTML = "Ordonner par<br>";

        this.layersDiv.appendChild(this.orderChoice);
        var orders = this.wifiMap.orders;
        for( var i =0; i < 2*orders.length; i++) {
            var j = Math.floor(i/2);
            var choice = document.createElement('input');
            var id = "choice" + i;
            choice.type = 'radio';
            var way = i % 2 ?'up':'down';
            var label = document.createElement('label');
            var img = document.createElement('img');
            img.src = '/static/img/sort-' + way + '.png';
            label.appendChild(img);
            label.htmlFor = id;
            choice.className = 'order ' + way;
            choice.name = 'order';
            choice.id = id;
            choice.checked = way == orders[j].def;
            choice.value = orders[j].name + '-' + way;
            choice.onchange = this.updateOrder.bind(this);
            this.orderChoice.appendChild(choice);
            //this.orderChoice.appendChild(document.createElement('span'));  //Trick css
            this.orderChoice.appendChild(label);
            if( !(i % 2) )
                this.orderChoice.appendChild(document.createTextNode(orders[j].label));
            else
                this.orderChoice.appendChild(document.createElement('br'));
        }
    },

    updateMap: function() {
        this.parentClass.updateMap.apply(this);
        for( b in this.wifiMap.listeBornes ) {
            var ap = this.wifiMap.listeBornes[b];
            var layer = ap.layer || this.wifiMap.bornesLayers.autres;
            ap.html_list.style.display = layer.visibility?'':'none';
        }
    },

    redraw: function () {
        this.parentClass.redraw.apply(this);
        for( var i = 0; i < this.dataLayers.length; i++) {
            if( !this.dataLayers[i].layer.markerUrl)
                continue;
            var img = document.createElement('img');
            img.src = this.dataLayers[i].layer.markerUrl;
            img.style.width = '15.75px';
            img.style.height = '18.75px';
            var txt = document.createTextNode('image');
            var label = this.dataLayers[i].labelSpan;
            label.parentNode.insertBefore(img, label.nextSibling);
        }
    },

    updateOrder: function () {
        var radios = document.getElementsByName('order');
        var res = [this.wifiMap.orders[0].name,'down'];
        for( var i = 0; i < radios.length; i++) {
            if( radios[i].checked ) {
                res = radios[i].value.split('-');
                break;
            }
        }
        this.wifiMap.setOrder(res[0],res[1]);
    }
    //CLASS_NAME: 'WifiMap.ModeSwitcher'
});

////
