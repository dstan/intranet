#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
WSGI config for Django_Client project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os

# ligne qui était là au départ, je l'ai virée parce que ça avait pas l'air
# d'importer ce qu'il fallait
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

# à la place, on va récupérer le path du dossier du fichier courant
here=os.path.dirname( os.path.realpath( __file__ ) )
import sys
sys.path.append(here)
# et a priori settings.py est dans le même dossier
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

# Bon, il semblerait qu'on a aussi besoin du répertoire parent
uphere="/".join(here.split("/")[:-1])
sys.path.append(uphere)

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
#from helloworld.wsgi import HelloWorldApplication
#application = HelloWorldApplication(application)
