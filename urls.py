# -*- encoding: utf-8 -*-

from django.conf.urls import include, patterns, url
import settings
import django.contrib.auth.views
from bw_comp.protectpost import protect

from django.contrib import admin
admin.autodiscover()

if settings.CAS_ENABLED:
    from django_cas_ng.signals import cas_user_authenticated
    from login import post_cas_login
    cas_user_authenticated.connect(post_cas_login)
    login_url = url(r'^login$', 'django_cas_ng.views.login', name="login")
    logout_url = url(r'^logout$', 'django_cas_ng.views.logout', name="logout")
else:
    login_url = url('^login', protect(django.contrib.auth.views.login), {'template_name': 'login.html'}, name="login")
    logout_url = url('^logout', 'django.contrib.auth.views.logout_then_login', name ="logout")

urlpatterns = patterns('',
    # Les pages existantes
    url('^$', 'apps.accueil.views.view', name='index'),
    # Pages de login
    login_url,
    logout_url,
    (r'^admin/', include(admin.site.urls)),
    (r'^heartbeat$', 'bw_comp.heartbeat.view'),
    )

if settings.DEV:
    # Quand on utilise le serveur de DEV, on veut que Django serve les static files
    urlpatterns += patterns('', url('^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root':'static/'}),
                           )

for app in settings.INTRANET_APPS:
    app = app["name"]
    urlpatterns += patterns('', ('^%s/' % app, include('apps.%s.urls' % app,
                                                       namespace = app,
                                                       app_name = app)))

# Pour une raison inconnue, ces valeurs par défaut ne sont pas définies ...
handler404='django.views.defaults.page_not_found'
handler403='django.views.defaults.permission_denied'
handler500='django.views.defaults.server_error'
