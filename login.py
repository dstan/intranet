# -*- coding: utf-8 -*-
#
# LOGIN.PY -- Gère l'interface d'authentification à l'aide des modèles Django.
#
# Copyright (C) 2009-2010 Nicolas Dandrimont
# Authors: Nicolas Dandrimont <olasd@crans.org>
# Censor: Antoine Durand-Gasselin <adg@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import settings, ldap
from django.contrib.auth.models import Group, User
from django.contrib.auth.backends import ModelBackend
from django.utils.importlib import import_module


# Pour se connecter à la base ldap
import lc_ldap.shortcuts

conn_pool = import_module('conn_pool', 'intranet')

def refresh_droits(user, cl_user):
    """Rafraîchit les droits de l'utilisateur django `user' depuis
    l'utilisateur LDAP `cl_user'"""

    cl_droits = [x.value for x in cl_user.get('droits', [])]
    if u"Nounou" in cl_droits:
        user.is_staff = True
        user.is_superuser = True
    else:
        user.is_staff = False
        user.is_superuser = False
    if u"Apprenti" in cl_droits:
            user.is_staff = True
    if u"Imprimeur" in cl_droits:
            user.is_staff = True

    groups = []
    for cl_droit in cl_droits:
        group, created = Group.objects.get_or_create(name="crans_%s" % cl_droit.lower())
        group.save()
        groups.append(group)
    if cl_user.paiement_ok():
        group, created = Group.objects.get_or_create(name="crans_paiement_ok")
        group.save()
        groups.append(group)
    if cl_user.imprimeur_clubs():
        group, created = Group.objects.get_or_create(name="crans_imprimeur_club")
        group.save()
        groups.append(group)
    if cl_user.clubs():
        group, created = Group.objects.get_or_create(name="crans_respo_club")
        group.save()
        groups.append(group)
    if 'aid' in cl_user:
        group, created = Group.objects.get_or_create(name="crans_adherent")
        group.save()
        groups.append(group)
    if 'aid' in cl_user:
        group, created = Group.objects.get_or_create(name="crans_club")
        group.save()
        groups.append(group)

    user.groups = [ group for group in user.groups.all() if not group.name.startswith('crans_') ]
    user.groups.add(*groups)
    user.save()

def refresh_fields(user, cl_user):
    """Rafraîchit les champs correspondants à l'utilisateur (nom,
    prénom, email)"""

    user.first_name = unicode(cl_user.get('prenom', [u"club"])[0])
    user.last_name = unicode(cl_user['nom'][0])
    mail = unicode(cl_user.get('mail', cl_user['uid'])[0])
    if '@' not in mail:  # Ne devrait pas arriver (pour migration)
        mail += u'@crans.org'
    user.email = mail

    user.save()

def post_cas_login(sender, user, created, attributes, ticket, service, **kwargs):
    ldap_user = conn_pool.get_user(user)
    refresh_droits(user, ldap_user)
    refresh_fields(user, ldap_user)

class LDAPUserBackend(ModelBackend):
    """Authentifie un utilisateur à l'aide de la base LDAP"""

    supports_anonymous_user = False

    def authenticate(self, username=None, password=None):
        """Authentifie l'utilisateur sur la base LDAP. Crée un
        utilisateur django s'il n'existe pas encore."""

        if not username or not password:
            return None
        shortcut = lc_ldap.shortcuts.lc_ldap_test if settings.BASE_LDAP_TEST else lc_ldap.shortcuts.lc_ldap
        try:
            conn = shortcut(user = username, cred = password)
            myself = conn.search(dn = conn.dn, scope = ldap.SCOPE_BASE)
            ldap_user = myself[0]
        except IndexError:
            return None
        except ldap.INVALID_CREDENTIALS:
            return None

        # On stocke les utilisateurs dans la base django comme "uid@crans.org"
        django_username = '%s@crans.org' % username
        try:
            user = User.objects.get(username=django_username)
        except User.DoesNotExist:
            user = User(username=django_username, password="LDAP Backend User!")
        user.save()
        conn_pool.CONNS[django_username] = conn
        refresh_droits(user, ldap_user)
        refresh_fields(user, ldap_user)
        return user

    def get_user(self, uid):
        """Récupère l'objet django correspondant à l'uid"""
        try:
            return User.objects.get(pk=uid)
        except User.DoesNotExist:
            return None

